
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm
local net = DLib.net

net.pool('tf2_pm_apply_hat')
net.pool('tf2_pm_remove_hat')
net.pool('tf2_pm_set_team')
net.pool('tf2_pm_unset_team')
net.pool('tf2_pm_set_hat_style')
net.pool('tf2_pm_unset_hat_style')
net.pool('tf2_pm_set_hat_color')
net.pool('tf2_pm_unset_hat_color')
net.pool('tf2_pm_set_bodygroup')

net.pool('tf2_pm_request_reset')
net.pool('tf2_pm_request_hat')
net.pool('tf2_pm_request_unhat')
net.pool('tf2_pm_request_color')
net.pool('tf2_pm_request_uncolor')
net.pool('tf2_pm_request_style')
net.pool('tf2_pm_request_unstyle')
net.pool('tf2_pm_request_team')
net.pool('tf2_pm_request_unteam')
net.pool('tf2_pm_player_death')
net.pool('tf2_pm_request_bodygroup')
net.pool('tf2_pm_request_unbodygroup')

net.pool('tf2_pm_player_model_update')
net.pool('tf2_pm_request_all')
net.receiveAntispam('tf2_pm_request_all')

net.receive('tf2_pm_request_unbodygroup', function(_, ply)
	if not IsValid(ply) then return end

	ply:GetTF2PM():UnsetCustomBodygroupAll()
end)

net.receive('tf2_pm_request_bodygroup', function(_, ply)
	if not IsValid(ply) then return end

	if net.ReadBool() then
		ply:GetTF2PM():SetCustomBodygroup(net.ReadString(), net.ReadUInt8())
	else
		ply:GetTF2PM():UnsetCustomBodygroup(net.ReadString())
	end
end)

net.receive('tf2_pm_request_reset', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():RemoveAllHats()
end)

net.receive('tf2_pm_request_team', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():SetTeam(net.ReadUInt16())
end)

net.receive('tf2_pm_request_unteam', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():UnsetTeam()
end)

net.receive('tf2_pm_request_hat', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():WearHat(net.ReadString())
end)

net.receive('tf2_pm_request_unhat', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():RemoveHat(net.ReadString())
end)

net.receive('tf2_pm_request_color', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():SetHatColor(net.ReadString(), net.ReadColor())
end)

net.receive('tf2_pm_request_uncolor', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():UnsetHatColor(net.ReadString())
end)

net.receive('tf2_pm_request_style', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():SetHatStyle(net.ReadString(), net.ReadUInt16())
end)

net.receive('tf2_pm_request_unstyle', function(_, ply)
	if not IsValid(ply) then return end
	ply:GetTF2PM():UnsetHatStyle(net.ReadString())
end)

net.receive('tf2_pm_request_all', function(_, ply)
	if not IsValid(ply) then return end

	for i, ply2 in ipairs(player.GetAll()) do
		if ply2 ~= ply and ply2.tf2_pm then
			ply2.tf2_pm:Send(ply)
		end
	end
end)
