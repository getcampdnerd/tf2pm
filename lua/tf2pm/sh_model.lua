
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm

tf2_pm.model_registry = tf2_pm.model_registry or {}
tf2_pm._model_registry = tf2_pm._model_registry or {}
tf2_pm.hat_registry = tf2_pm.hat_registry or {}

tf2_pm.hat_registry_sorted = nil

function tf2_pm.LookupModel(model)
	return tf2_pm._model_registry[model] or tf2_pm.model_registry[model] or false
end

function tf2_pm.LookupHat(hat)
	return tf2_pm.hat_registry[hat] or false
end

function tf2_pm.RegisterModel(classname, model, definition)
	assert(isstring(classname), 'Invalid classname')
	classname = classname:trim():lower()
	assert(classname ~= '', 'Empty classname')

	assert(isstring(model), 'Invalid model')
	model = model:trim():lower()
	assert(model ~= '', 'Empty model')

	assert(istable(definition), 'Invalid definition')

	definition.model = model

	--[[
		definition.targets = {
			-- example of scout model:
			["head"] = { -- hat affects "head" target
				bodygroup_overrides = {hat = 1, headphones = 1}
			},
		}
	]]
	definition.targets = definition.targets or {}
	definition.classname = classname

	-- example: {"scout"} or {"generic", "has_head", "has_hands"}
	-- determines applicable hats
	if isstring(definition.category) then
		definition.category = {definition.category}
	elseif definition.category == nil then
		definition.category = {classname}
	end

	if istable(definition.category) and not table.qhasValue(definition.category, classname) then
		table.insert(definition.category, classname)
	end

	-- {1, 2, 4, ...}
	if definition.model_skins == nil then
		definition.model_skins = false
	end

	if definition.player_color_skin == nil then
		definition.player_color_skin = false
	end

	if definition.hands_bodygroups == nil then
		definition.hands_bodygroups = false
	end

	if definition.hands_skin == nil then
		definition.hands_skin = false
	end

	-- Defines which hats to blacklist from utilizing with this model, even if hat is compatible
	-- by category or the has has `applicable` set to true
	-- example: `blacklist = {'pyro_fireman_helmet'}`
	if istable(definition.blacklist) and table.Count(definition.blacklist) == 0 then
		definition.blacklist = nil
	elseif definition.blacklist ~= nil and not istable(definition.blacklist) then
		error('Model.blacklist is invalid typeof ' .. type(definition.blacklist) .. '!')
	end

	-- Same as blacklist, but for targets
	if istable(definition.targets_blacklist) and table.Count(definition.targets_blacklist) == 0 then
		definition.targets_blacklist = nil
	elseif definition.targets_blacklist ~= nil and not istable(definition.targets_blacklist) then
		error('Model.targets_blacklist is invalid typeof ' .. type(definition.targets_blacklist) .. '!')
	end

	-- Allows to "fake" model's Skin ID when skin is being selected by a hat.
	-- This directly change how values are picked from `responsive_skin` of each cosmetic
	-- e.g. if remap_responsive_skins is {1, 1, 1, 2, 2, 4, 3, ...n} then with skins 1, 2, 3 cosmetics will get
	-- responsive_skin value from [1] index, and so on
	-- if mapping is missing from this table (or the table itself), original index is picked
	-- example: `remap_responsive_skins = {2, 1}`
	-- example above added to default tf2 models will cause all cosmetics have color of opposite team
	if istable(definition.remap_responsive_skins) and table.Count(definition.remap_responsive_skins) == 0 then
		definition.remap_responsive_skins = nil
	end

	-- By default hands skin is determined by model's skin
	-- You can forcefully redefine it using this table
	if definition.hands_skin_map == nil then
		definition.hands_skin_map = false
	end

	-- You probably want to leave this empty
	-- Otherwise those bodygroups would be forced on this model
	-- unless overriden by `targets` or hat's `bodygroup_overrides`
	if definition.bodygroup_overrides == nil then
		definition.bodygroup_overrides = {}
	end

	tf2_pm.model_registry[classname] = definition
	tf2_pm._model_registry[model] = definition

	return definition
end

function tf2_pm.RegisterHat(classname, model, definition)
	assert(isstring(classname), 'Invalid classname')
	classname = classname:trim():lower()
	assert(classname ~= '', 'Empty classname')

	if istable(model) then
		definition = model
		model = model.model
	end

	assert(isstring(model), 'Invalid model')
	model = model:trim():lower()
	assert(model ~= '', 'Empty model')

	assert(istable(definition), 'Invalid definition')

	definition.model = model

	if isstring(definition.targets) then
		definition.targets = {definition.targets}
	end

	-- example: {"eyes", "head"} or "chest"
	-- this define what bodygroups and everything else to hide
	-- and also to determine conflicting hats
	-- if at least one target collide with other hat's targets
	-- hats are considered to be incompatible
	definition.targets = definition.targets or {}

	if isstring(definition.target) then
		if istable(definition.targets) and not table.qhasValue(definition.targets, definition.target) then
			table.insert(definition.targets, definition.target)
		end

		definition.target = nil
	end

	-- example: "scout" or {"pyro", "demoman", "soldier"}
	-- or from example above in model definition of "category" property: {"has_hands"}
	-- pass nil or true for it to be applicable to *everything*
	-- hat is considered to be applicable when at least one of these collide with model category
	if isstring(definition.applicable) then
		definition.applicable = {definition.applicable}
	elseif definition.applicable == nil then
		definition.applicable = true
	end

	--[[
		overrides = {
			["scout"] = { -- we want to push model's "target" overrides when this hat is being worn by "scout" model
				model_skins = {5, 6},
			}
		}
	]]
	definition.overrides = definition.overrides or {}

	--[[
		global_overrides = {
			model_skins = {5, 6},
		}
	]]
	definition.global_overrides = definition.global_overrides or {}

	definition.bodygroup_overrides = definition.bodygroup_overrides or {}

	--[[
		-- keep in mind you CAN define a style with zero index!
		-- styles with zero index will always be applied if nothing else is selected
		styles = {
			{
				name = 'Fallback name', -- name to be utilized as fallback
				localized_name = 'title.tf2pm.item.myitem.name', -- optional, but recommended
				localized_description = 'title.tf2pm.item.myitem.desc', -- optional
				responsive_skin = {2, 3, ...}, -- optional, overrides hat's responsive_skin property
				model = 'newmodel.mdl', -- overrides model
				bodygroup_overrides = {["hat" = 2]}, -- overrides bodygroups of parent model
				bodygroups_self = {["antenna" = 1]}, -- overrides bodygroups of this model
				skin = 1, -- when this is present, responsive_skin is ignored

				-- keep in mind that those overrides are applied *after* root overrides
			}
		}
	]]
	if definition.styles == nil or not istable(definition.styles) or table.Count(definition.styles) == 0 then
		definition.styles = false
	end

	-- {skin 0, skin 1, skin 2, ...} skin indexes, as number
	-- each entry maps to wearer's (player's) skin
	-- In case of TF2 Cosmetics, this is most of time player's team (RED, BLU).
	-- If player's skin is unset (set to player_color_skin or 0) fallback_skin is utilized instead (which defaults to 0)
	if definition.responsive_skin == nil or not istable(definition.responsive_skin) or table.Count(definition.responsive_skin) == 0 then
		definition.responsive_skin = false
	end

	if definition.fallback_skin == nil or not isnumber(definition.fallback_skin) then
		definition.fallback_skin = 0
	end

	if definition.paintable == nil then
		definition.paintable = false
	end

	if definition.icon == nil then
		definition.icon = false
	end

	if definition.gui_category == nil then
		definition.gui_category = 'hats'
	end

	-- {["localized name (DLib.i18n)"] = Color()}
	if definition.predefined_colors == nil then
		definition.predefined_colors = false
	end

	if definition.name == nil then
		definition.name = definition.localized_name and DLib.i18n.localize(definition.localized_name) or 'Unnamed hat'
	end

	if definition.description == nil then
		definition.description = definition.localized_description and DLib.i18n.localize(definition.localized_description) or ''
	end

	if definition.localized_name == nil then
		definition.localized_name = definition.name
	end

	if definition.localized_description == nil then
		definition.localized_description = definition.description
	end

	definition.classname = classname
	tf2_pm.hat_registry[classname] = definition
	tf2_pm.hat_registry_sorted = nil

	return definition
end

hook.Add('DLib.LanguageChanged', 'tf2pm', function()
	tf2_pm.hat_registry_sorted = nil
end)

local i18n = DLib.i18n

local function hatsorter(a, b)
	return i18n.localize(a.localized_name) < i18n.localize(b.localized_name)
end

function tf2_pm.GetSortedHats()
	if tf2_pm.hat_registry_sorted then return tf2_pm.hat_registry_sorted end

	tf2_pm.hat_registry_sorted = {}

	for _, hatdef in pairs(tf2_pm.hat_registry) do
		table.insert(tf2_pm.hat_registry_sorted, hatdef)
	end

	table.sort(tf2_pm.hat_registry_sorted, hatsorter)

	return tf2_pm.hat_registry_sorted
end
