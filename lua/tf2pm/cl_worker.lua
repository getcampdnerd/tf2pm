
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm
tf2_pm.known_models = tf2_pm.known_models or {}

function tf2_pm.GC()
	local garbage = 0

	for i, model in ipairs(tf2_pm.known_models) do
		if IsValid(model) then
			if not IsValid(model.tf2_owner) then
				model.garbage = true
				model:Remove()
				garbage = garbage + 1
			elseif IsValid(model) and IsValid(model.tf2_owner) then
				model:SetParent(model.tf2_owner)
				model:AddEffects(EF_BONEMERGE)
			end
		else
			garbage = garbage + 1
		end
	end

	if garbage > 100 and garbage >= #tf2_pm.known_models / 2 or garbage > 1000 then
		local old = tf2_pm.known_models
		local i = 1
		tf2_pm.known_models = {}

		for i, model in ipairs(old) do
			if IsValid(model) and not model.garbage then
				table.insert(tf2_pm.known_models, model)
			end
		end
	end
end

timer.Create('tf2_pm.GC', 60, 0, tf2_pm.GC)

local vector_origin = Vector()

local function patch()
	matproxy.Add({
		name = 'ItemTintColor',

		init = function(self, mat, values)
			self.ResultTo = values.resultvar
			--self.OriginalColor = mat:GetVector(self.ResultTo) or vector_origin
		end,

		bind = function(self, mat, ent)
			if not IsValid(ent) then return end

			if ent.tf2_pm_paint then
				mat:SetVector(self.ResultTo, ent.tf2_pm_paint)
			elseif isentity(ent.ProxyentPaintColor) and isfunction(ent.ProxyentPaintColor.GetPaintVector) then -- TF2 Paint tool compat
				mat:SetVector(self.ResultTo, ent.ProxyentPaintColor:GetPaintVector())
			elseif ent.pac_matproxies and ent.pac_matproxies.ItemTintColor then -- pac3 compat
				mat:SetVector(self.ResultTo, ent.pac_matproxies.ItemTintColor)
			else
				--mat:SetVector(self.ResultTo, self.OriginalColor or vector_origin)
				mat:SetVector(self.ResultTo, vector_origin)
			end
		end
	})
end

timer.Simple(0, patch)
hook.Run('pac_Initialized', 'TF2PM', patch)

function _G.LocalTF2PM()
	local ply = LocalPlayer()
	if not IsValid(ply) then return end
	return ply:GetTF2PM()
end

hook.Add('NotifyShouldTransmit', 'tf2_pm', function(ent, shouldtransmit)
	if ent.tf2_pm then
		if not ent.tf2_pm.modeldef or ent.tf2_pm.modeldef.model ~= ent:GetModel() then
			ent.tf2_pm:RebuildModelDef()
		end

		if ent.tf2_pm.modeldef then
			if shouldtransmit then
				ent.tf2_pm:HideModels(false)
				ent.tf2_pm:MergeModelsTo(ent)
			else
				ent.tf2_pm:HideModels(true)
			end
		end
	end
end)

hook.Add('PlayerLeaveVehicle', 'tf2_pm', function(ent)
	if ent.tf2_pm and not ent:IsDormant() then
		if not ent.tf2_pm.modeldef or ent.tf2_pm.modeldef.model ~= ent:GetModel() then
			ent.tf2_pm:RebuildModelDef()
		end

		if ent.tf2_pm.modeldef then
			ent.tf2_pm:HideModels(false)
			ent.tf2_pm:MergeModelsTo(ent)
		end
	end
end)

hook.Add('Think', 'tf2pm_death', function()
	hook.Call('TF2PMDeathThink')
end)

hook.Add('CreateClientsideRagdoll', 'tf2pm_death', function(ply, ent)
	if not ply:IsPlayer() then return end
	local pm = ply:GetTF2PM()

	if not pm.modeldef or pm.modeldef.model ~= ply:GetModel() then
		pm:RebuildModelDef()
	end

	pm:MergeModelsTo(ent)
	pm:HideModels(false)

	local hookid = tostring(ent)

	hook.Add('TF2PMDeathThink', hookid, function()
		if not IsValid(ply) then
			hook.Remove('TF2PMDeathThink', hookid)
			return
		end

		if not IsValid(ent) then
			pm:MergeModelsTo(ply)
			hook.Remove('TF2PMDeathThink', hookid)
		else
			pm:MergeModelsTo(ent)
		end
	end)
end)
