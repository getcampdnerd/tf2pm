
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm
local net = DLib.net

local function check_model(ply)
	if not ply.tf2_pm then return end

	if not ply.tf2_pm.modeldef or ply.tf2_pm.modeldef.model ~= ply:GetModel() then
		ply.tf2_pm:RebuildModelDef()
	end

	net.Start('tf2_pm_player_model_update')
	net.WriteEntity(ply)
	net.Broadcast()

	ply.tf2_pm:ProcessBodygroups()
	ply.tf2_pm:ProcessSkins()
end

local function queue_model_update(ply)
	timer.Simple(0, function()
		if IsValid(ply) then check_model(ply) end
	end)
end

hook.Add('PlayerSetModel', 'tf2_pm', queue_model_update)
hook.Add('PlayerSpawn', 'tf2_pm', queue_model_update)
hook.Add('PlayerLoadout', 'tf2_pm', queue_model_update)
hook.Add('PlayerSetHandsModel', 'tf2_pm', queue_model_update)
