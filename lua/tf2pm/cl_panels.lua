
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm

local PANEL = {}

function PANEL:Init()
	self.current_model_angle = Angle(0, 45, 0)
end

local proxies = {
	'WearHat',
	'RemoveHat',
	'RemoveAllHats',
	'ProcessBodygroups',
	'ProcessSkins',
	'SetHatColor',
	'SetHatStyle',
	'GetTeam',
	'SetTeam',
	'UnsetTeam',
	'UnsetHatColor',
	'UnsetHatStyle',
	'GetHatArray',
	'GetModelDef',
	'DeserializeString',
	'RebuildModelDef',
	'HasHatColor',
	'GetHatColor',
	'HasHatStyle',
	'GetHatStyle',
	'HideModels',
	'SetCustomBodygroup',
	'UnsetCustomBodygroup',
}

AccessorFunc(PANEL, 'save_target', 'SaveTarget')

function PANEL:MarkDirty()
	if IsValid(self.save_target) then
		self.save_target:MarkDirty()
	end
end

function PANEL:GetTF2Data()
	return self.Entity:GetTF2PM()
end

for _, proxyname in ipairs(proxies) do
	PANEL[proxyname] = function(self, ...)
		local data = self:GetTF2Data()
		return data[proxyname](data, ...)
	end
end

function PANEL:OnRemove()
	if IsValid(self.Entity) and self.Entity.tf2_pm then
		self.Entity.tf2_pm:Remove()
	end

	DModelPanel.OnRemove(self)
end

function PANEL:PostDrawModel(ent)
	for _, ent2 in ipairs(ent:GetChildren()) do
		ent2:DrawModel()
	end
end

function PANEL:OnMousePressed(code)
	if code == MOUSE_LEFT then
		self.rotate_pressed = true
		self.mouse_x, self.mouse_y = input.GetCursorPos()
	end
end

function PANEL:OnMouseReleased(code)
	if code == MOUSE_LEFT then
		self.rotate_pressed = false
	end
end

function PANEL:Think()
	DModelPanel.Think(self)

	if self.rotate_pressed then
		self.rotate_pressed = input.IsMouseDown(MOUSE_LEFT)
	end

	if self.rotate_pressed then
		local x, y = input.GetCursorPos()
		local diffx, diffy = x - self.mouse_x, y - self.mouse_y
		self.mouse_x, self.mouse_y = x, y
		self.current_model_angle.y = self.current_model_angle.y + diffx
	end
end

function PANEL:LayoutEntity(ent)
	self:RunAnimation()
	ent:SetAngles(self.current_model_angle)
end

vgui.Register('TF2PM_ModelPanel', PANEL, 'DModelPanel')

local toprecache = {}

hook.Add('PostRenderVGUI', 'tf2_pm_precache_icons', function()
	if #toprecache == 0 then return end

	local hatdef = table.remove(toprecache)

	if hatdef.icon:endsWith('.png') then
		hatdef.icon_cached = Material(hatdef.icon, 'smooth')
	else
		hatdef.icon_cached = Material(hatdef.icon)
	end
end)

local PANEL = {}

PANEL._tf2pm_hat_model = true

function PANEL:Init()
	self:SetText('')
	self:SetSize(200, 200)
end

AccessorFunc(PANEL, 'hatdef', 'HatDef')
AccessorFunc(PANEL, 'chooser_panel', 'ChooserPanel')
AccessorFunc(PANEL, 'model_target', 'ModelPanelTarget')
AccessorFunc(PANEL, 'no_wear_reason', 'CantWearReason')
AccessorFunc(PANEL, 'update_function', 'UpdateFunction')
AccessorFunc(PANEL, 'disabled_icon', 'DisabledIcon')

local paint_icon = Material('vgui/backpack_jewel_paint_splatter')
local missing_icon = Material('gui/tf2pm_missing_icon.png', 'smooth')

function PANEL:Paint(w, h)
	surface.SetDrawColor(255, 255, 255)
	surface.DrawLine(4, 4, w - 8, 4)
	surface.DrawLine(w - 8, 4, w - 8, h - 8)
	surface.DrawLine(4, 4, 4, h - 8)
	surface.DrawLine(4, h - 8, w - 8, h - 8)

	local disabled = not self:IsEnabled()

	if disabled then
		surface.SetDrawColor(100, 0, 0, 150)
		surface.DrawRect(5, 5, w - 12, h - 12)
	end

	if self.hatdef then
		if not self.hatdef.icon_cached and self.hatdef.icon then
			if not table.qhasValue(toprecache, self.hatdef) then
				table.insert(toprecache, self.hatdef)
			end
		end

		if self.hatdef.icon_cached and not self.hatdef.icon_cached:IsError() then
			surface.SetMaterial(self.hatdef.icon_cached)

			if disabled then
				surface.SetDrawColor(150, 150, 150)
			else
				surface.SetDrawColor(255, 255, 255)
			end

			local min = w > h and h or w
			surface.DrawTexturedRect(w / 2 - min / 2, h / 2 - min / 2, min, min)
		elseif self.hatdef.icon and (not self.hatdef.icon_cached or not self.hatdef.icon_cached:IsError()) then
			draw.DrawText('...', 'CloseCaption_Bold', w / 2, h / 2, color_white, TEXT_ALIGN_CENTER)
		else
			surface.SetMaterial(missing_icon)

			if disabled then
				surface.SetDrawColor(150, 150, 150)
			else
				surface.SetDrawColor(255, 255, 255)
			end

			local min = w > h and h or w
			surface.DrawTexturedRect(w / 2 - min / 2, h / 2 - min / 2, min, min)
		end

		if IsValid(self.model_target) then
			local color = self.model_target:GetHatColor(self.hatdef.classname)

			if color then
				surface.SetDrawColor(color)
				surface.SetMaterial(paint_icon)
				surface.DrawTexturedRect(w - 43, 9, 32, 32)
			end
		end
	end

	if self.disabled_icon then
		surface.SetMaterial(self.disabled_icon)
		surface.SetDrawColor(180, 180, 180)
		surface.DrawTexturedRect(w - 24, 4, 16, 16)
	end
end

function PANEL:DoClick()
	if IsValid(self.chooser_panel) then
		self.chooser_panel:OnHatChoose(self, self.hatdef)
		return
	end

	local frame = vgui.Create('TF2PM_HatChooser')
	frame:SetParentHatCell(self)
	frame:SetModelDef(self.model_target:GetModelDef())

	local listing = self.model_target:GetHatArray()

	for i, hatdef in ipairs(listing) do
		if hatdef == self.hatdef then
			table.remove(listing, i)
			break
		end
	end

	frame:SetHatList(listing)

	frame:BuildList()
	frame.quicksearch:RequestFocus()
end

function PANEL:DoRightClick()
	if IsValid(self.chooser_panel) then return end
	if not self.hatdef then return end

	local menu = DermaMenu()

	menu:AddOption('gui.tf2pm.editor.hat_control.unwear', function()
		if self.model_target:RemoveHat(self.hatdef.classname) then
			self.hatdef = nil
		end

		self.model_target:MarkDirty()

		if self.update_function then
			self:update_function()
		end
	end):SetIcon('icon16/delete.png')

	if self.hatdef.styles then
		menu:AddOption('gui.tf2pm.editor.hat_control.choose_style', function()
			self:OpenStyleMenu()
		end):SetIcon('icon16/cog_edit.png')

		if self.model_target:HasHatStyle(self.hatdef.classname) then
			menu:AddOption('gui.tf2pm.editor.hat_control.unset_style', function()
				self.model_target:UnsetHatStyle(self.hatdef.classname)
				self.model_target:MarkDirty()
			end):SetIcon('icon16/cog_delete.png')
		end
	end

	if self.hatdef.paintable then
		menu:AddOption('gui.tf2pm.editor.hat_control.set_color', function()
			self:OpenPaintMenu()
		end):SetIcon('icon16/color_wheel.png')

		if self.model_target:HasHatColor(self.hatdef.classname) then
			menu:AddOption('gui.tf2pm.editor.hat_control.unset_color', function()
				self.model_target:UnsetHatColor(self.hatdef.classname)
				self.model_target:MarkDirty()
			end):SetIcon('icon16/contrast.png')
		end
	end

	menu:Open()
end

function PANEL:GetOpenSide(w, h)
	local x, y = self:LocalToScreen(0, self:GetTall())

	if x + w > ScrW() then
		-- x = x - w
		x = ScrW() - w
	end

	if y + h > ScrH() then
		-- y = y - h
		y = ScrH() - h
	end

	return x, y
end

function PANEL:OpenStyleMenu()
	if self.hatdef and not self.hatdef.styles then return end

	if IsValid(self.current_inspect_panel) then
		self.current_inspect_panel:Center()
		self.current_inspect_panel:MakePopup()
		return
	end

	local frame = vgui.Create('TF2PM_HatStyleInspector')
	frame:SetTargetPanel(self)
	frame:SetHatDef(self.hatdef)

	frame:SetPos(self:GetOpenSide(frame:GetSize()))
	self.current_inspect_panel = frame
end

function PANEL:OpenPaintMenu()
	if self.hatdef and not self.hatdef.paintable then return end

	if IsValid(self.current_inspect_panel) then
		self.current_inspect_panel:Center()
		self.current_inspect_panel:MakePopup()
		return
	end

	local frame = vgui.Create('TF2PM_HatColorInspector')
	frame:SetTargetPanel(self)
	frame:SetHatDef(self.hatdef)

	frame:SetPos(self:GetOpenSide(frame:GetSize()))
	self.current_inspect_panel = frame

	if self.model_target:HasHatColor(self.hatdef.classname) then
		frame:SetColor(self.model_target:GetHatColor(self.hatdef.classname))
	end
end

function PANEL:OnClose()
	if IsValid(self.current_inspect_panel) then
		self.current_inspect_panel:Remove()
	end
end

function PANEL:OnColorChanged(color)
	if not IsValid(self.model_target) then return end
	if self.hatdef and not self.hatdef.paintable then return end
	self.model_target:SetHatColor(self.hatdef.classname, color)
	self.model_target:MarkDirty()
end

function PANEL:OnStyleChanged(index)
	if not IsValid(self.model_target) then return end
	if self.hatdef and not self.hatdef.styles then return end
	self.model_target:SetHatStyle(self.hatdef.classname, index)
	self.model_target:MarkDirty()
end

function PANEL:OnHatChoosen(chooser, hatdef, inflictor)
	if not IsValid(self.model_target) then return end

	if self.hatdef then
		if self.model_target:RemoveHat(self.hatdef.classname) then
			self.hatdef = nil
		end
	end

	if self.model_target:WearHat(hatdef.classname) then
		self.hatdef = hatdef
	end

	self.model_target:MarkDirty()

	if self.update_function then
		self:update_function()
	end
end

vgui.Register('TF2PM_HatCell', PANEL, 'DButton')

local PANEL = {}

AccessorFunc(PANEL, 'modeldef', 'ModelDef')
AccessorFunc(PANEL, 'hatlist', 'HatList')
AccessorFunc(PANEL, 'setter_panel', 'ChooseTarget')
AccessorFunc(PANEL, 'parent_hat_cell', 'ParentHatCell')

function PANEL:Init()
	self:SetTitle('gui.tf2pm.editor.hat_selector')

	self.sheet = vgui.Create('DPropertySheet', self)
	self.sheet:Dock(FILL)

	self.quicksearch = vgui.Create('DTextEntry', self)
	self.quicksearch:SetPlaceholderText(DLib.i18n.localize('gui.tf2pm.editor.quicksearch'))
	self.quicksearch:SetUpdateOnType(true)
	self.quicksearch.target = self
	self.quicksearch.OnValueChange = self.OnValueChange

	self.hat_panels = {}
	self.categories = {}
end

function PANEL:OnValueChange(value)
	for i, panel in ipairs(self.target.hat_panels) do
		panel:Remove()
	end

	self.target.hat_panels = {}

	self.target:BuildList()
end

function PANEL:PerformLayout2(w, h)
	local wide = math.clamp(w - 200, 30, 400)
	self.quicksearch:SetPos(w - wide - 100, 4)
	self.quicksearch:SetWide(wide)
end

function PANEL:GetCategoryCanvas(name)
	if self.categories[name] then return self.categories[name] end

	local scrollpanel = vgui.Create('DScrollPanel', self)
	scrollpanel:Dock(FILL)

	self.sheet:AddSheet('gui.tf2pm.hat_category.' .. name, scrollpanel)

	if IsValid(self.iconlayout) then
		self.iconlayout:Remove()
	end

	local iconlayout = vgui.Create('DIconLayout', scrollpanel)
	iconlayout:Dock(FILL)

	iconlayout:SetSpaceX(2)
	iconlayout:SetSpaceY(2)

	self.categories[name] = iconlayout
	return iconlayout
end

local i18n = DLib.i18n

local function tester(hatdef, search)
	if not search or search == '' then return true end

	if hatdef.localized_name then
		return string.find(i18n.localize(hatdef.localized_name):lower(), search, 1, false) ~= nil
	end

	return true
end

-- local disabled_icon = Material('icon16/cross.png', 'smooth')

function PANEL:BuildList()
	local modeldef = self.modeldef
	local search = string.lower(self.quicksearch:GetValue() or '')

	for i, hatdef in ipairs(tf2_pm.GetSortedHats()) do
		if tf2_pm.IsApplicable(hatdef, modeldef) and tester(hatdef, search) then
			local canvas = self:GetCategoryCanvas(hatdef.gui_category)
			local button = vgui.Create('TF2PM_HatCell', canvas)

			button:SetHatDef(hatdef)
			button:SetChooserPanel(self)
			button:SetSize(100, 100)

			local conflicting = tf2_pm.IsConflictingListFast(hatdef, self.hatlist)

			if conflicting then
				button:SetEnabled(false)
				-- button:SetTooltip('gui.tf2pm.editor.hat_is_conflicting')
				button:SetCantWearReason('gui.tf2pm.editor.hat_is_conflicting')
				-- button:SetDisabledIcon(disabled_icon)
			end

			table.insert(self.hat_panels, button)
		end
	end
end

function PANEL:OnHatChoose(button, hatdef)
	if IsValid(self.parent_hat_cell) then
		self.parent_hat_cell:OnHatChoosen(self, hatdef, button)
	end

	self:Close()
end

vgui.Register('TF2PM_HatChooser', PANEL, 'DLib_Window')

local PANEL = {}

AccessorFunc(PANEL, 'hatdef', 'HatDef')
AccessorFunc(PANEL, 'target_panel', 'TargetPanel')

function PANEL:Init()
	self.mixer = vgui.Create('DLibColorMixer', self)
	self.mixer:Dock(FILL)
	self.mixer:SetAlphaBar(false)

	--[[self.model = vgui.Create('DModelPanel', self)
	self.model:Dock(BOTTOM)]]

	function self.mixer.ValueChanged(_, color)
		--[[if IsValid(self.model.Entity) then
			self.model.Entity.tf2_pm_paint = color:ToVector()
		end]]

		if IsValid(self.target_panel) then
			self.target_panel:OnColorChanged(color)
		end
	end

	self:SetSize(600, 500)
	self:SetTitle('gui.tf2pm.editor.hat_control.color_inspection')
	self:Center()
end

function PANEL:SetColor(color)
	self.mixer:SetColor(color)
end

--[[function PANEL:PerformLayout2(w, h)
	local h2 = math.round(h / 2)
	self.model:SetTall(h2 * 0.8)
	self.mixer:SetTall(h - h2)
end]]

function PANEL:SetHatDef(hatdef)
	self.hatdef = hatdef
	--self.model:SetModel(hatdef.model)
end

vgui.Register('TF2PM_HatColorInspector', PANEL, 'DLib_Window')

local PANEL = {}

AccessorFunc(PANEL, 'hatdef', 'HatDef')
AccessorFunc(PANEL, 'target_panel', 'TargetPanel')

function PANEL:Init()
	self.list = vgui.Create('DListView', self)
	self.list:Dock(FILL)

	--[[self.model = vgui.Create('DModelPanel', self)
	self.model:Dock(BOTTOM)]]

	--self:SetSize(600, 700)
	self:SetSize(400, 500)
	self:SetTitle('gui.tf2pm.editor.hat_control.style_inspection')
	self:Center()

	function self.list.DoDoubleClick(_, index, line)
		self:ChooseStyle(index - (self.has_zero_style and 1 or 0))
	end

	function self.list.OnRowRightClick(_, index, line)
		local menu = DermaMenu()

		menu:AddOption('gui.tf2pm.editor.hat_control.choose_style', function()
			self:ChooseStyle(index - (self.has_zero_style and 1 or 0))
		end):SetIcon('icon16/database_go.png')

		menu:Open()
	end

	self.list:AddColumn('gui.tf2pm.editor.hat_control.column_style')
end

function PANEL:ChooseStyle(index)
	if not IsValid(self.target_panel) then return end
	self.target_panel:OnStyleChanged(index)
end

--[[function PANEL:PerformLayout2(w, h)
	local h2 = math.round(h / 2)
	self.model:SetTall(h2 * 0.8)
	self.list:SetTall(h - h2)
end]]

function PANEL:SetHatDef(hatdef)
	self.hatdef = hatdef
	--self.model:SetModel(hatdef.model)
	self.list:Clear()

	if hatdef.styles then
		self.has_zero_style = hatdef.styles[0] ~= nil

		for i = self.has_zero_style and 0 or 1, #hatdef.styles do
			local style = hatdef.styles[i]
			local name = style.localized_name and i18n.localize(style.localized_name) or style.name or i18n.localize('gui.tf2pm.editor.hat_control.style_num', i)
			self.list:AddLine(name)
		end
	end
end

vgui.Register('TF2PM_HatStyleInspector', PANEL, 'DLib_Window')

