
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm

local models = {
	'scout', 'demo', 'engineer', 'heavy',
	'medic', 'pyro', 'sniper', 'soldier',
	'spy'
}

for _, model in ipairs(models) do
	player_manager.AddValidModel('tf2_' .. model, 'models/player/tf2_pm/' .. model .. '.mdl')

	tf2_pm.RegisterModel(model, 'models/player/tf2_pm/' .. model .. '.mdl', {
		model_skins = {0, 1},
		player_color_skin = 2,

		hands = 'models/player/arms/tf2_pm/c_' .. model .. '_arms.mdl',

		hands_bodygroups = true,
		hands_skin = true,
		hands_skin_map = {0, 1},

		targets = {
			zombie_body = {
				model_skins = {3, 4}
			}
		},
	})

	player_manager.AddValidHands('tf2_' .. model, 'models/player/arms/tf2_pm/c_' .. model .. '_arms.mdl', 0, '0')
end
