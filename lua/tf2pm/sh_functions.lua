
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm

function tf2_pm.IsApplicable(hatdef, modeldef)
	if isstring(hatdef) then
		hatdef = tf2_pm.hat_registry[hatdef]
	end

	if isstring(modeldef) then
		modeldef = tf2_pm._model_registry[modeldef]
	end

	if not istable(hatdef) or not istable(modeldef) then return false end

	if hatdef.applicable == false then return false end

	if istable(modeldef.blacklist) and table.qhasValue(modeldef.blacklist, hatdef.classname) then
		return false
	end

	if istable(modeldef.targets_blacklist) and istable(hatdef.targets) then
		for k, v in ipairs(hatdef.targets) do
			if table.qhasValue(modeldef.targets_blacklist, v) then
				return false
			end
		end
	end

	if hatdef.applicable ~= true and istable(hatdef.applicable) then
		local hit = false

		for i, category in ipairs(modeldef.category) do
			if table.qhasValue(hatdef.applicable, category) then
				hit = true
				break
			end
		end

		if not hit then return false end
	end

	return true
end

function tf2_pm.IsConflicting(hatdef, otherhatdef)
	if isstring(hatdef) then
		hatdef = tf2_pm.hat_registry[hatdef]
	end

	if not istable(hatdef) or not istable(otherhatdef) then return false end
	if not istable(otherhatdef.targets) or not istable(hatdef.targets) then return false end

	for i, target in ipairs(otherhatdef.targets) do
		if table.qhasValue(hatdef.targets, target) then
			return true
		end
	end

	return false
end

function tf2_pm.IsConflictingList(hatdef, listing)
	if isstring(hatdef) then
		hatdef = tf2_pm.hat_registry[hatdef]
	end

	if not istable(hatdef) then return false end

	for k, v in pairs(listing) do
		if tf2_pm.IsConflicting(hatdef, v) then
			return true
		end
	end

	return false
end

function tf2_pm.IsConflictingListFast(hatdef, listing)
	if isstring(hatdef) then
		hatdef = tf2_pm.hat_registry[hatdef]
	end

	if not istable(hatdef) then return false end

	for i = 1, #listing do
		if tf2_pm.IsConflicting(hatdef, listing[i]) then
			return true
		end
	end

	return false
end

function tf2_pm.GetWrappedDescription(input, target)
	if not input or input == '' then return '' end
	local lines
	input = input:replace('\n', ' ')
	local accumulated_length = 0
	local accumulated_str
	target = target or 50

	for _, line in ipairs(input:split(' ')) do
		local len = utf8.len(line)
		if not len then return 'malformed UTF8 string' end

		if accumulated_length ~= 0 and (accumulated_length + len) > target then
			if not lines then
				lines = accumulated_str
			else
				lines = lines .. '\n' .. accumulated_str
			end

			accumulated_str = nil
			accumulated_length = 0
		end

		accumulated_length = accumulated_length + len

		if not accumulated_str then
			accumulated_str = line
		else
			accumulated_str = accumulated_str .. ' ' .. line
		end
	end

	if accumulated_length ~= 0 then
		if not lines then
			lines = accumulated_str
		else
			lines = lines .. '\n' .. accumulated_str
		end
	end

	return lines or ''
end
