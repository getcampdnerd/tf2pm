
-- Auto generated at 2022-02-26 20:22:44 UTC+07:00
return {
	{
		classes = {"soldier"},
		targets = {},
		paintable = true,
		name = "Soldier's Stash",
		localized_name = "info.tf2pm.hat.soldier_hat_1",
		localized_description = "info.tf2pm.hat.soldier_hat_1_desc",
		icon = "backpack/player/items/soldier/soldier_hat",
		models = {
			["basename"] = "models/player/items/soldier/soldier_hat.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Stainless Pot",
		localized_name = "info.tf2pm.hat.soldier_pot_hat",
		localized_description = "info.tf2pm.hat.soldier_pot_hat_desc",
		icon = "backpack/player/items/soldier/soldier_pot",
		models = {
			["basename"] = "models/player/items/soldier/soldier_pot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Tyrant's Helm",
		localized_name = "info.tf2pm.hat.soldier_viking_hat",
		localized_description = "info.tf2pm.hat.soldier_viking_hat_desc",
		icon = "backpack/player/items/soldier/soldier_viking",
		models = {
			["basename"] = "models/player/items/soldier/soldier_viking.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"medal"},
		paintable = false,
		name = "Web Easteregg Medal",
		localized_name = "info.tf2pm.hat.soldier_medal_web_sleuth",
		localized_description = "info.tf2pm.hat.soldier_medal_web_sleuth_desc",
		icon = "backpack/player/items/soldier/medal",
		models = {
			["basename"] = "",
		},
		bodygroup_overrides = {
			["medal"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"feet"},
		paintable = false,
		name = "The Gunboats",
		localized_name = "info.tf2pm.hat.unique_blast_boots",
		icon = "backpack/weapons/c_models/c_rocketboots_soldier",
		models = {
			["basename"] = "models/weapons/c_models/c_rocketboots_soldier.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Soldier Samurai Hat",
		localized_name = "info.tf2pm.hat.soldiersamurai",
		localized_description = "info.tf2pm.hat.soldiersamurai_desc",
		icon = "backpack/player/items/soldier/soldier_samurai",
		models = {
			["basename"] = "models/player/items/soldier/soldier_samurai.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Soldier Drill Hat",
		localized_name = "info.tf2pm.hat.soldierdrillhat",
		localized_description = "info.tf2pm.hat.soldierdrillhat_desc",
		icon = "backpack/player/items/soldier/soldier_sargehat",
		models = {
			["basename"] = "models/player/items/soldier/soldier_sargehat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Grenadier's Softcap",
		localized_name = "info.tf2pm.hat.thegrenadierssoftcap",
		icon = "backpack/player/items/soldier/grenadier_softcap",
		models = {
			["basename"] = "models/player/items/soldier/grenadier_softcap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades", "hat"},
		paintable = false,
		name = "Worms Gear",
		localized_name = "info.tf2pm.hat.worms_gear",
		icon = "backpack/player/items/soldier/worms_gear",
		models = {
			["basename"] = "models/player/items/soldier/worms_gear.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Chieftain's Challenge",
		localized_name = "info.tf2pm.hat.soldierchiefrocketeer",
		icon = "backpack/player/items/soldier/chief_rocketeer",
		models = {
			["basename"] = "models/player/items/soldier/chief_rocketeer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Stout Shako",
		localized_name = "info.tf2pm.hat.soldiershako",
		localized_description = "info.tf2pm.hat.soldiershako_desc",
		icon = "backpack/player/items/soldier/soldier_shako",
		models = {
			["basename"] = "models/player/items/soldier/soldier_shako.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {},
		paintable = true,
		name = "Exquisite Rack",
		localized_name = "info.tf2pm.hat.exquisiterack",
		icon = "backpack/player/items/soldier/soldier_holiday_antlers",
		models = {
			["basename"] = "models/player/items/soldier/soldier_holiday_antlers.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"face", "hat"},
		paintable = true,
		name = "Defiant Spartan",
		localized_name = "info.tf2pm.hat.defiantspartan",
		icon = "backpack/player/items/soldier/soldier_spartan",
		models = {
			["basename"] = "models/player/items/soldier/soldier_spartan.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses"},
		paintable = true,
		name = "Hero's Hachimaki",
		localized_name = "info.tf2pm.hat.homefront_blindfold",
		icon = "backpack/player/items/soldier/homefront_blindfold",
		models = {
			["basename"] = "models/player/items/soldier/homefront_blindfold.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "Honcho's Headgear",
		localized_name = "info.tf2pm.hat.honchosheadgear",
		localized_description = "info.tf2pm.hat.honchosheadgear_desc",
		icon = "backpack/player/items/soldier/honchos_headgear",
		models = {
			["basename"] = "models/player/items/soldier/honchos_headgear.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Furious Fukaamigasa",
		localized_name = "info.tf2pm.hat.furiousfukaamigasa",
		localized_description = "info.tf2pm.hat.furiousfukaamigasa_desc",
		icon = "backpack/player/items/soldier/asian_merc",
		models = {
			["basename"] = "models/player/items/soldier/asian_merc.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Jumper's Jeepcap",
		localized_name = "info.tf2pm.hat.jumpersjeepcap",
		icon = "backpack/player/items/soldier/soldier_jeepcap",
		models = {
			["basename"] = "models/player/items/soldier/soldier_jeepcap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses", "hat"},
		paintable = false,
		name = "Bucket Hat",
		localized_name = "info.tf2pm.hat.buckethat",
		localized_description = "info.tf2pm.hat.buckethat_desc",
		icon = "backpack/player/items/soldier/bucket",
		models = {
			["basename"] = "models/player/items/soldier/bucket.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Lord Cockswain's Pith Helmet",
		localized_name = "info.tf2pm.hat.lordcockswainpith",
		icon = "backpack/player/items/soldier/drg_pith_hat",
		models = {
			["basename"] = "models/player/items/soldier/drg_pith_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_cigar", "beard"},
		paintable = true,
		name = "Lord Cockswain's Novelty Mutton Chops and Pipe",
		localized_name = "info.tf2pm.hat.lordcockswainchops",
		icon = "backpack/player/items/crafting/drg_hair_beard_icon",
		models = {
			["basename"] = "models/player/items/soldier/drg_hair_beard.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.lordcockswainchops_style0",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.lordcockswainchops_style1",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 0,
		},
	},
	{
		classes = {"soldier"},
		targets = {"feet"},
		paintable = false,
		name = "The Mantreads",
		localized_name = "info.tf2pm.hat.mantreads",
		icon = "backpack/workshop/player/items/soldier/mantreads/mantreads",
		models = {
			["basename"] = "models/workshop/player/items/soldier/mantreads/mantreads.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Armored Authority",
		localized_name = "info.tf2pm.hat.armoredauthority",
		icon = "backpack/player/items/soldier/armored_authority",
		models = {
			["basename"] = "models/player/items/soldier/armored_authority.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"shirt"},
		paintable = true,
		name = "Fancy Dress Uniform",
		localized_name = "info.tf2pm.hat.fancydressuniform",
		icon = "backpack/player/items/soldier/fdu",
		models = {
			["basename"] = "models/player/items/soldier/fdu.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Stahlhelm",
		localized_name = "info.tf2pm.hat.ro_soldierhelmet",
		icon = "backpack/player/items/soldier/ro_helmet",
		models = {
			["basename"] = "models/player/items/soldier/ro_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Idiot Box",
		localized_name = "info.tf2pm.hat.hwn_soldierhat",
		icon = "backpack/player/items/soldier/hwn_soldier_hat",
		models = {
			["basename"] = "models/player/items/soldier/hwn_soldier_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"sleeves"},
		paintable = false,
		name = "Steel Pipes",
		localized_name = "info.tf2pm.hat.hwn_soldiermisc1",
		icon = "backpack/player/items/soldier/hwn_soldier_misc1",
		models = {
			["basename"] = "models/player/items/soldier/hwn_soldier_misc1.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"feet"},
		paintable = false,
		name = "Shoestring Budget",
		localized_name = "info.tf2pm.hat.hwn_soldiermisc2",
		icon = "backpack/player/items/soldier/hwn_soldier_misc2",
		models = {
			["basename"] = "models/player/items/soldier/hwn_soldier_misc2.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Infernal Impaler",
		localized_name = "info.tf2pm.hat.infernalimpaler",
		icon = "backpack/workshop/player/items/soldier/infernal_impaler/infernal_impaler",
		models = {
			["basename"] = "models/workshop/player/items/soldier/infernal_impaler/infernal_impaler.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = false,
		name = "The Kringle Collection",
		localized_name = "info.tf2pm.hat.soldierwintercoat",
		localized_description = "info.tf2pm.hat.soldierwintercoat_desc",
		icon = "backpack/player/items/soldier/xms_snowcoat",
		models = {
			["basename"] = "models/player/items/soldier/xms_snowcoat.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Lucky Shot",
		localized_name = "info.tf2pm.hat.luckyshot",
		localized_description = "info.tf2pm.hat.luckyshot_desc",
		icon = "backpack/player/items/soldier/luckyshot",
		models = {
			["basename"] = "models/player/items/soldier/luckyShot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Battle Bob",
		localized_name = "info.tf2pm.hat.jag_haircut",
		localized_description = "info.tf2pm.hat.jag_haircut_desc",
		icon = "backpack/player/items/soldier/jag_bob_haircut",
		models = {
			["basename"] = "models/player/items/soldier/jag_bob_haircut.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.jag_haircut_style_nohelmet",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.jag_haircut_style_withhelmet",
			},
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Conquistador",
		localized_name = "info.tf2pm.hat.conquistador",
		localized_description = "info.tf2pm.hat.conquistador_desc",
		icon = "backpack/player/items/soldier/morion",
		models = {
			["basename"] = "models/player/items/soldier/morion.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades"},
		paintable = false,
		name = "The Captain's Cocktails",
		localized_name = "info.tf2pm.hat.ds_cangrenades",
		localized_description = "info.tf2pm.hat.ds_cangrenades_desc",
		icon = "backpack/player/items/soldier/ds_can_grenades",
		models = {
			["basename"] = "models/player/items/soldier/ds_can_grenades.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Helmet Without a Home",
		localized_name = "info.tf2pm.hat.ds_footballhelmet",
		localized_description = "info.tf2pm.hat.ds_footballhelmet_desc",
		icon = "backpack/player/items/soldier/ds_football_helmet",
		models = {
			["basename"] = "models/player/items/soldier/ds_football_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Cross-Comm Crash Helmet",
		localized_name = "info.tf2pm.hat.grfs_1",
		localized_description = "info.tf2pm.hat.grfs_1_desc",
		icon = "backpack/player/items/soldier/grfs_soldier",
		models = {
			["basename"] = "models/player/items/soldier/grfs_soldier.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The War Pig",
		localized_name = "info.tf2pm.hat.soldierwarpig",
		localized_description = "info.tf2pm.hat.soldierwarpig_desc",
		icon = "backpack/workshop/player/items/soldier/soldier_warpig/soldier_warpig",
		models = {
			["basename"] = "models/workshop/player/items/soldier/soldier_warpig/soldier_warpig.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldierwarpig_style0",
				models = {
					["basename"] = "models/workshop/player/items/soldier/soldier_warpig/soldier_warpig.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldierwarpig_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/soldier_warpig_s2/soldier_warpig_s2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Tin Pot",
		localized_name = "info.tf2pm.hat.soldier_robot_helmet",
		localized_description = "info.tf2pm.hat.soldier_robot_helmet_desc",
		icon = "backpack/player/items/mvm_loot/soldier/robot_helmet",
		models = {
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldier_robot_helmet_style0",
				models = {
					["basename"] = "models/player/items/mvm_loot/soldier/robot_helmet_bullet.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.soldier_robot_helmet_style1",
				models = {
					["basename"] = "models/player/items/mvm_loot/soldier/robot_helmet.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_cigar"},
		paintable = false,
		name = "The Soldier's Stogie",
		localized_name = "info.tf2pm.hat.soldiercigar",
		localized_description = "info.tf2pm.hat.soldiercigar_desc",
		icon = "backpack/player/items/soldier/cigar",
		models = {
			["basename"] = "models/player/items/soldier/cigar.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Zipperface",
		localized_name = "info.tf2pm.hat.zipperface",
		localized_description = "info.tf2pm.hat.zipperface_desc",
		icon = "backpack/player/items/soldier/soldier_zipperhead",
		models = {
			["basename"] = "models/player/items/soldier/soldier_zipperhead.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Chief Constable",
		localized_name = "info.tf2pm.hat.hm_soldierhat",
		localized_description = "info.tf2pm.hat.hm_soldierhat_desc",
		icon = "backpack/workshop_partner/player/items/soldier/hm_cap/hm_cap",
		models = {
			["basename"] = "models/workshop_partner/player/items/soldier/hm_cap/hm_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Soldier's Slope Scopers",
		localized_name = "info.tf2pm.hat.soldiersslopescopers",
		localized_description = "info.tf2pm.hat.soldiersslopescopers_desc",
		icon = "backpack/player/items/soldier/soldier_skihat",
		models = {
			["basename"] = "models/player/items/soldier/soldier_skihat.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.soldiersslopescopers_style0",
				models = {
					["basename"] = "models/player/items/soldier/soldier_skihat.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.soldiersslopescopers_style1",
				models = {
					["basename"] = "models/player/items/soldier/soldier_skihat_s1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Doe-Boy",
		localized_name = "info.tf2pm.hat.doeboy",
		localized_description = "info.tf2pm.hat.doeboy_desc",
		icon = "backpack/player/items/soldier/bio_soldier_founders",
		models = {
			["basename"] = "models/player/items/soldier/bio_soldier_founders_cover.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.doeboy_style0",
				models = {
					["basename"] = "models/player/items/soldier/bio_soldier_founders_cover.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.doeboy_style1",
				models = {
					["basename"] = "models/player/items/soldier/bio_soldier_founders.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Soldier",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_soldier_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_soldier_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_soldier_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The War on Smissmas Battle Hood",
		localized_name = "info.tf2pm.hat.winter2013_battlehood",
		localized_description = "info.tf2pm.hat.winter2013_battlehood_desc",
		icon = "backpack/player/items/soldier/soldier_hood",
		models = {
			["basename"] = "models/player/items/soldier/soldier_hood.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.winter2013_battlehood_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/player/items/soldier/soldier_hood.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.winter2013_battlehood_style1",
				models = {
					["basename"] = "models/player/items/soldier/soldier_hood_helmet.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades"},
		paintable = false,
		name = "The War on Smissmas Battle Socks",
		localized_name = "info.tf2pm.hat.winter2013_battlesocks",
		localized_description = "info.tf2pm.hat.winter2013_battlesocks_desc",
		icon = "backpack/player/items/soldier/xms_nade_socks",
		models = {
			["basename"] = "models/player/items/soldier/xms_nade_socks.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Big Daddy",
		localized_name = "info.tf2pm.hat.bigdaddy",
		localized_description = "info.tf2pm.hat.bigdaddy_desc",
		icon = "backpack/workshop/player/items/soldier/bi_washington_mask/bi_washington_mask",
		models = {
			["basename"] = "models/workshop/player/items/soldier/bi_washington_mask/bi_washington_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The First American",
		localized_name = "info.tf2pm.hat.firstamerican",
		localized_description = "info.tf2pm.hat.firstamerican_desc",
		icon = "backpack/workshop/player/items/soldier/bi_franklin_mask/bi_franklin_mask",
		models = {
			["basename"] = "models/workshop/player/items/soldier/bi_franklin_mask/bi_franklin_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Gilded Guard",
		localized_name = "info.tf2pm.hat.gildedguard",
		icon = "backpack/workshop_partner/player/items/soldier/thief_soldier_helmet/thief_soldier_helmet",
		models = {
			["basename"] = "models/workshop_partner/player/items/soldier/thief_soldier_helmet/thief_soldier_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_legs"},
		paintable = false,
		name = "The Man in Slacks",
		localized_name = "info.tf2pm.hat.short2014_man_in_slacks",
		icon = "backpack/workshop/player/items/soldier/short2014_man_in_slacks/short2014_man_in_slacks",
		models = {
			["basename"] = "models/workshop/player/items/soldier/short2014_man_in_slacks/short2014_man_in_slacks.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"shirt"},
		paintable = true,
		name = "The Patriot's Pouches",
		localized_name = "info.tf2pm.hat.may16_patriots_pouches",
		icon = "backpack/workshop/player/items/soldier/shooters_supplies/shooters_supplies",
		models = {
			["basename"] = "models/workshop/player/items/soldier/shooters_supplies/shooters_supplies.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.may16_patriots_pouches_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/shooters_supplies/shooters_supplies.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.may16_patriots_pouches_style2",
				models = {
					["basename"] = "models/workshop/player/items/soldier/shooters_supplies_style1/shooters_supplies_style1.mdl",
				}
			},
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses", "hat"},
		paintable = true,
		name = "The Cadaver's Capper",
		localized_name = "info.tf2pm.hat.hw2013_soldier_jiangshi_hat",
		icon = "backpack/workshop/player/items/soldier/hw2013_soldier_jiangshi_hat/hw2013_soldier_jiangshi_hat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_soldier_jiangshi_hat/hw2013_soldier_jiangshi_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades"},
		paintable = false,
		name = "Flakcatcher",
		localized_name = "info.tf2pm.hat.spr17_flakcatcher",
		localized_description = "info.tf2pm.hat.spr17_flakcatcher_desc",
		icon = "backpack/workshop/player/items/soldier/spr17_flakcatcher/spr17_flakcatcher",
		models = {
			["basename"] = "models/workshop/player/items/soldier/spr17_flakcatcher/spr17_flakcatcher.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Spook Specs",
		localized_name = "info.tf2pm.hat.short2014_soldier_fedhair",
		icon = "backpack/workshop/player/items/soldier/short2014_soldier_fedhair/short2014_soldier_fedhair",
		models = {
			["basename"] = "models/workshop/player/items/soldier/short2014_soldier_fedhair/short2014_soldier_fedhair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Sky High Fly Guy",
		localized_name = "info.tf2pm.hat.spr18_sky_high_fly_guy",
		icon = "backpack/workshop/player/items/soldier/spr18_sky_high_fly_guy/spr18_sky_high_fly_guy",
		models = {
			["basename"] = "models/workshop/player/items/soldier/spr18_sky_high_fly_guy/spr18_sky_high_fly_guy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Caribbean Conqueror",
		localized_name = "info.tf2pm.hat.jul13_the_caribbean_conqueror",
		localized_description = "info.tf2pm.hat.jul13_the_caribbean_conqueror_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_the_caribbean_conqueror/jul13_the_caribbean_conqueror",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_the_caribbean_conqueror/jul13_the_caribbean_conqueror.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Slo-Poke",
		localized_name = "info.tf2pm.hat.xms2013_soldier_marshal_hat",
		icon = "backpack/workshop/player/items/soldier/xms2013_soldier_marshal_hat/xms2013_soldier_marshal_hat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/xms2013_soldier_marshal_hat/xms2013_soldier_marshal_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 Skullcap",
		localized_name = "info.tf2pm.hat.dec2014_skullcap",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/soldier/dec2014_skullcap/dec2014_skullcap",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec2014_skullcap/dec2014_skullcap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades"},
		paintable = false,
		name = "Ghoul Gibbin' Gear",
		localized_name = "info.tf2pm.hat.sf14_ghoul_gibbing_gear",
		icon = "backpack/workshop/player/items/soldier/sf14_ghoul_gibbing_gear/sf14_ghoul_gibbing_gear",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sf14_ghoul_gibbing_gear/sf14_ghoul_gibbing_gear.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "The Compatriot",
		localized_name = "info.tf2pm.hat.jul13_soldier_eagle",
		localized_description = "info.tf2pm.hat.jul13_soldier_eagle_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_soldier_eagle/jul13_soldier_eagle",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_soldier_eagle/jul13_soldier_eagle.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Valley Forge",
		localized_name = "info.tf2pm.hat.jul13_the_presidential",
		localized_description = "info.tf2pm.hat.jul13_the_presidential_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_the_presidential/jul13_the_presidential",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_the_presidential/jul13_the_presidential.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Hellmet",
		localized_name = "info.tf2pm.hat.hwn2015_hellmet",
		icon = "backpack/workshop/player/items/soldier/hwn2015_hellmet/hwn2015_hellmet",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2015_hellmet/hwn2015_hellmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Bazaar Bauble",
		localized_name = "info.tf2pm.hat.sum20_bazaar_bauble",
		icon = "backpack/workshop/player/items/soldier/sum20_bazaar_bauble/sum20_bazaar_bauble",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum20_bazaar_bauble/sum20_bazaar_bauble.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Dancing Doe",
		localized_name = "info.tf2pm.hat.sum19_dancing_doe",
		icon = "backpack/workshop/player/items/soldier/sum19_dancing_doe/sum19_dancing_doe",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum19_dancing_doe/sum19_dancing_doe.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Hawk Warrior",
		localized_name = "info.tf2pm.hat.sum20_hawk_warrior",
		icon = "backpack/workshop/player/items/soldier/sum20_hawk_warrior/sum20_hawk_warrior",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum20_hawk_warrior/sum20_hawk_warrior.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Brass Bucket",
		localized_name = "info.tf2pm.hat.dec17_brass_bucket",
		icon = "backpack/workshop/player/items/soldier/dec17_brass_bucket/dec17_brass_bucket",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec17_brass_bucket/dec17_brass_bucket.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Dumb Bell",
		localized_name = "info.tf2pm.hat.dec18_dumb_bell",
		icon = "backpack/workshop/player/items/soldier/dec18_dumb_bell/dec18_dumb_bell",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec18_dumb_bell/dec18_dumb_bell.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"feet"},
		paintable = false,
		name = "The Colonial Clogs",
		localized_name = "info.tf2pm.hat.jul13_colonial_clogs",
		localized_description = "info.tf2pm.hat.jul13_colonial_clogs_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_colonial_clogs/jul13_colonial_clogs",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_colonial_clogs/jul13_colonial_clogs.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Crit Cloak",
		localized_name = "info.tf2pm.hat.fall17_crit_cloak",
		localized_description = "info.tf2pm.hat.fall17_crit_cloak_desc",
		icon = "backpack/workshop/player/items/soldier/fall17_crit_cloak/fall17_crit_cloak",
		models = {
			["basename"] = "models/workshop/player/items/soldier/fall17_crit_cloak/fall17_crit_cloak.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "Sharp Chest Pain",
		localized_name = "info.tf2pm.hat.fall17_chest_pain",
		localized_description = "info.tf2pm.hat.fall17_chest_pain_desc",
		icon = "backpack/workshop/player/items/soldier/fall17_chest_pain/fall17_chest_pain",
		models = {
			["basename"] = "models/workshop/player/items/soldier/fall17_chest_pain/fall17_chest_pain.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {},
		paintable = true,
		name = "Poopy Doe",
		localized_name = "info.tf2pm.hat.hwn2021_poopy_doe",
		icon = "backpack/workshop/player/items/soldier/hwn2021_poopy_doe/hwn2021_poopy_doe",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2021_poopy_doe/hwn2021_poopy_doe.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Shellmet",
		localized_name = "info.tf2pm.hat.fall17_shellmet",
		localized_description = "info.tf2pm.hat.fall17_shellmet_desc",
		icon = "backpack/workshop/player/items/soldier/fall17_bullet_bowl/fall17_bullet_bowl",
		models = {
			["basename"] = "models/workshop/player/items/soldier/fall17_bullet_bowl/fall17_bullet_bowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Climbing Commander",
		localized_name = "info.tf2pm.hat.dec18_climbing_commander",
		icon = "backpack/workshop/player/items/soldier/dec18_climbing_commander/dec18_climbing_commander",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec18_climbing_commander/dec18_climbing_commander.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_cigar"},
		paintable = false,
		name = "Soldier's Sparkplug",
		localized_name = "info.tf2pm.hat.robo_soldier_sparkplug",
		localized_description = "info.tf2pm.hat.robo_soldier_sparkplug_desc",
		icon = "backpack/workshop/player/items/soldier/robo_soldier_sparkplug/robo_soldier_sparkplug",
		models = {
			["basename"] = "models/workshop/player/items/soldier/robo_soldier_sparkplug/robo_soldier_sparkplug.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Freedom Feathers",
		localized_name = "info.tf2pm.hat.hw2013_feathered_freedom",
		icon = "backpack/workshop/player/items/soldier/hw2013_feathered_freedom/hw2013_feathered_freedom",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_feathered_freedom/hw2013_feathered_freedom.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {},
		paintable = true,
		name = "War Dog",
		localized_name = "info.tf2pm.hat.hwn2021_war_dog",
		icon = "backpack/workshop/player/items/soldier/hwn2021_war_dog/hwn2021_war_dog",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2021_war_dog/hwn2021_war_dog.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_war_dog_style0",
				bodygroup_overrides = {
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2021_war_dog/hwn2021_war_dog.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_war_dog_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2021_war_dog_style2/hwn2021_war_dog_style2.mdl",
				}
			},
		},
	},
	{
		classes = {"soldier"},
		targets = {"beard"},
		paintable = false,
		name = "The Faux Manchu",
		localized_name = "info.tf2pm.hat.hw2013_faux_manchu",
		icon = "backpack/workshop/player/items/soldier/hw2013_faux_manchu/hw2013_faux_manchu",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_faux_manchu/hw2013_faux_manchu.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_pocket"},
		paintable = false,
		name = "Attack Packs",
		localized_name = "info.tf2pm.hat.fall17_attack_packs",
		localized_description = "info.tf2pm.hat.fall17_attack_packs_desc",
		icon = "backpack/workshop/player/items/soldier/fall17_attack_packs/fall17_attack_packs",
		models = {
			["basename"] = "models/workshop/player/items/soldier/fall17_attack_packs/fall17_attack_packs.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Full Metal Drill Hat",
		localized_name = "info.tf2pm.hat.robo_soldier_fullmetaldrillhat",
		localized_description = "info.tf2pm.hat.robo_soldier_fullmetaldrillhat_desc",
		icon = "backpack/workshop/player/items/soldier/robo_soldier_fullmetaldrillhat/robo_soldier_fullmetaldrillhat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/robo_soldier_fullmetaldrillhat/robo_soldier_fullmetaldrillhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_soldierhat",
		localized_name = "info.tf2pm.hat.eotl_soldierhat",
		icon = "backpack/workshop/player/items/soldier/eotl_soldierhat/eotl_soldierhat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/eotl_soldierhat/eotl_soldierhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Crack Pot",
		localized_name = "info.tf2pm.hat.dec18_crack_pot",
		icon = "backpack/workshop/player/items/soldier/dec18_crack_pot/dec18_crack_pot",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec18_crack_pot/dec18_crack_pot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Larval Lid",
		localized_name = "info.tf2pm.hat.hw2013_mr_maggot",
		icon = "backpack/workshop/player/items/soldier/hw2013_mr_maggot/hw2013_mr_maggot",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_mr_maggot/hw2013_mr_maggot.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"feet"},
		paintable = true,
		name = "EOTL_Coldfront Curbstompers",
		localized_name = "info.tf2pm.hat.eotl_coldfront_curbstompers",
		icon = "backpack/workshop/player/items/soldier/coldfront_curbstompers/coldfront_curbstompers",
		models = {
			["basename"] = "models/workshop/player/items/soldier/coldfront_curbstompers/coldfront_curbstompers.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "tw_soldierbot_helmet",
		localized_name = "info.tf2pm.hat.tw_soldierbot_helmet",
		icon = "backpack/workshop/player/items/soldier/tw_soldierbot_helmet/tw_soldierbot_helmet",
		models = {
			["basename"] = "models/workshop/player/items/soldier/tw_soldierbot_helmet/tw_soldierbot_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Steel Shako",
		localized_name = "info.tf2pm.hat.robo_soldier_shako",
		localized_description = "info.tf2pm.hat.robo_soldier_shako_desc",
		icon = "backpack/workshop/player/items/soldier/robo_soldier_shako/robo_soldier_shako",
		models = {
			["basename"] = "models/workshop/player/items/soldier/robo_soldier_shako/robo_soldier_shako.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_pocket"},
		paintable = true,
		name = "The Shaolin Sash",
		localized_name = "info.tf2pm.hat.hw2013_shaolin_sash",
		icon = "backpack/workshop/player/items/soldier/hw2013_shaolin_sash/hw2013_shaolin_sash",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_shaolin_sash/hw2013_shaolin_sash.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_soldierbot_armor",
		localized_name = "info.tf2pm.hat.tw_soldierbot_armor",
		icon = "backpack/workshop/player/items/soldier/tw_soldierbot_armor/tw_soldierbot_armor",
		models = {
			["basename"] = "models/workshop/player/items/soldier/tw_soldierbot_armor/tw_soldierbot_armor.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"grenades"},
		paintable = true,
		name = "Grub Grenades",
		localized_name = "info.tf2pm.hat.hw2013_combat_maggots",
		icon = "backpack/workshop/player/items/soldier/hw2013_combat_maggots/hw2013_combat_maggots",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_combat_maggots/hw2013_combat_maggots.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_legs"},
		paintable = true,
		name = "War Blunder",
		localized_name = "info.tf2pm.hat.hwn2020_war_blunder",
		icon = "backpack/workshop/player/items/soldier/hwn2020_war_blunder/hwn2020_war_blunder",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2020_war_blunder/hwn2020_war_blunder.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Vampire Vanquisher",
		localized_name = "info.tf2pm.hat.hwn2018_vampire_vanquisher",
		icon = "backpack/workshop/player/items/soldier/hwn2018_vampire_vanquisher/hwn2018_vampire_vanquisher",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2018_vampire_vanquisher/hwn2018_vampire_vanquisher.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Tank Top",
		localized_name = "info.tf2pm.hat.hwn2018_tank_top",
		icon = "backpack/workshop/player/items/soldier/hwn2018_tank_top/hwn2018_tank_top",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2018_tank_top/hwn2018_tank_top.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "The Diplomat",
		localized_name = "info.tf2pm.hat.dec15_diplomat",
		icon = "backpack/workshop/player/items/soldier/dec15_diplomat/dec15_diplomat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec15_diplomat/dec15_diplomat.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_legs"},
		paintable = false,
		name = "The Killer's Kit",
		localized_name = "info.tf2pm.hat.sbox2014_killers_kit",
		icon = "backpack/workshop/player/items/soldier/sbox2014_killers_kit/sbox2014_killers_kit",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sbox2014_killers_kit/sbox2014_killers_kit.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_killers_kit_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/sbox2014_killers_kit/sbox2014_killers_kit.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_killers_kit_style2",
				models = {
					["basename"] = "models/workshop/player/items/soldier/sbox2014_killers_kit_2/sbox2014_killers_kit_2.mdl",
				}
			},
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Peacebreaker",
		localized_name = "info.tf2pm.hat.sum19_peacebreaker",
		icon = "backpack/workshop/player/items/soldier/sum19_peacebreaker/sum19_peacebreaker",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum19_peacebreaker/sum19_peacebreaker.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "EOTL_soldier_garrison",
		localized_name = "info.tf2pm.hat.eotl_soldier_garrison",
		icon = "backpack/workshop/player/items/soldier/soldier_garrison/soldier_garrison",
		models = {
			["basename"] = "models/workshop/player/items/soldier/soldier_garrison/soldier_garrison.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Ground Control",
		localized_name = "info.tf2pm.hat.sbox2014_soldier_major",
		icon = "backpack/workshop/player/items/soldier/sbox2014_soldier_major/sbox2014_soldier_major",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sbox2014_soldier_major/sbox2014_soldier_major.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "eotl_winter_coat",
		localized_name = "info.tf2pm.hat.eotl_winter_coat",
		icon = "backpack/workshop/player/items/soldier/eotl_winter_coat/eotl_winter_coat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/eotl_winter_coat/eotl_winter_coat.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"beard"},
		paintable = true,
		name = "The Lone Survivor",
		localized_name = "info.tf2pm.hat.sept2014_lone_survivor",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/soldier/sept2014_lone_survivor/sept2014_lone_survivor",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sept2014_lone_survivor/sept2014_lone_survivor.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Soldier",
		localized_name = "info.tf2pm.hat.item_zombiesoldier",
		icon = "backpack/player/items/soldier/soldier_zombie",
		models = {
			["basename"] = "models/player/items/soldier/soldier_zombie.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Tyrantium Helmet",
		localized_name = "info.tf2pm.hat.robo_soldier_tyrantium",
		localized_description = "info.tf2pm.hat.robo_soldier_tyrantium_desc",
		icon = "backpack/workshop/player/items/soldier/robo_soldier_tyrantium/robo_soldier_tyrantium",
		models = {
			["basename"] = "models/workshop/player/items/soldier/robo_soldier_tyrantium/robo_soldier_tyrantium.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Racc Mann",
		localized_name = "info.tf2pm.hat.hwn2019_racc_mann",
		icon = "backpack/workshop/player/items/soldier/hwn2019_racc_mann/hwn2019_racc_mann",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2019_racc_mann/hwn2019_racc_mann.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Whirly Warrior",
		localized_name = "info.tf2pm.hat.jul13_helicopter_helmet",
		localized_description = "info.tf2pm.hat.jul13_helicopter_helmet_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_helicopter_helmet/jul13_helicopter_helmet",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_helicopter_helmet/jul13_helicopter_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "Party Poncho",
		localized_name = "info.tf2pm.hat.hwn2019_party_poncho",
		icon = "backpack/workshop/player/items/soldier/hwn2019_party_poncho/hwn2019_party_poncho",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2019_party_poncho/hwn2019_party_poncho.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "El Zapateador",
		localized_name = "info.tf2pm.hat.hwn2019_el_zapateador",
		icon = "backpack/workshop/player/items/soldier/hwn2019_el_zapateador/hwn2019_el_zapateador",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2019_el_zapateador/hwn2019_el_zapateador.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Thousand-Yard Stare",
		localized_name = "info.tf2pm.hat.sum21_thousand_yard",
		icon = "backpack/workshop/player/items/soldier/sum21_thousand_yard_style1/sum21_thousand_yard_style1",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum21_thousand_yard_style2/sum21_thousand_yard_style2.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum21_thousand_yard_style2",
				models = {
					["basename"] = "models/workshop/player/items/soldier/sum21_thousand_yard_style2/sum21_thousand_yard_style2.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum21_thousand_yard_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/sum21_thousand_yard_style1/sum21_thousand_yard_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"sleeves"},
		paintable = false,
		name = "The Space Bracers",
		localized_name = "info.tf2pm.hat.hw2013_galactic_gauntlets",
		icon = "backpack/workshop/player/items/soldier/hw2013_galactic_gauntlets/hw2013_galactic_gauntlets",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_galactic_gauntlets/hw2013_galactic_gauntlets.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "The Panisher",
		localized_name = "info.tf2pm.hat.sum21_panisher",
		icon = "backpack/workshop/player/items/soldier/sum21_panisher/sum21_panisher",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum21_panisher/sum21_panisher.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_legs"},
		paintable = false,
		name = "The Jupiter Jumpers",
		localized_name = "info.tf2pm.hat.hw2013_jupiter_jumpers",
		icon = "backpack/workshop/player/items/soldier/hw2013_jupiter_jumpers/hw2013_jupiter_jumpers",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_jupiter_jumpers/hw2013_jupiter_jumpers.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "Antarctic Parka",
		localized_name = "info.tf2pm.hat.xms2013_soldier_parka",
		icon = "backpack/workshop/player/items/soldier/xms2013_soldier_parka/xms2013_soldier_parka",
		models = {
			["basename"] = "models/workshop/player/items/soldier/xms2013_soldier_parka/xms2013_soldier_parka.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Roaming Roman",
		localized_name = "info.tf2pm.hat.sum21_roaming_roman",
		icon = "backpack/workshop/player/items/soldier/sum21_roaming_roman/sum21_roaming_roman",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum21_roaming_roman/sum21_roaming_roman.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Calamitous Cauldron",
		localized_name = "info.tf2pm.hat.hwn2020_calamitous_cauldron",
		icon = "backpack/workshop/player/items/soldier/hwn2020_calamitous_cauldron/hwn2020_calamitous_cauldron",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2020_calamitous_cauldron/hwn2020_calamitous_cauldron.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_calamitous_cauldron_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2020_calamitous_cauldron/hwn2020_calamitous_cauldron.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_calamitous_cauldron_style2",
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2020_calamitous_cauldron_style2/hwn2020_calamitous_cauldron_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"shirt"},
		paintable = false,
		name = "The Founding Father",
		localized_name = "info.tf2pm.hat.jul13_dandy_yankee",
		localized_description = "info.tf2pm.hat.jul13_dandy_yankee_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_dandy_yankee/jul13_dandy_yankee",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_dandy_yankee/jul13_dandy_yankee.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "The Brawling Buccaneer",
		localized_name = "info.tf2pm.hat.jul13_gangplank_garment",
		localized_description = "info.tf2pm.hat.jul13_gangplank_garment_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_gangplank_garment/jul13_gangplank_garment",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_gangplank_garment/jul13_gangplank_garment.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "Veterans Attire",
		localized_name = "info.tf2pm.hat.spr18_veterans_attire",
		icon = "backpack/workshop/player/items/soldier/spr18_veterans_attire/spr18_veterans_attire",
		models = {
			["basename"] = "models/workshop/player/items/soldier/spr18_veterans_attire/spr18_veterans_attire.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"sleeves"},
		paintable = true,
		name = "The Shogun's Shoulder Guard",
		localized_name = "info.tf2pm.hat.jul13_shoguns_guard",
		localized_description = "info.tf2pm.hat.jul13_shoguns_guard_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_shoguns_guard/jul13_shoguns_guard",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_shoguns_guard/jul13_shoguns_guard.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_pocket"},
		paintable = false,
		name = "Lieutenant Bites",
		localized_name = "info.tf2pm.hat.jul13_lt_bites",
		localized_description = "info.tf2pm.hat.jul13_lt_bites_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_lt_bites/jul13_lt_bites",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_lt_bites/jul13_lt_bites.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Semi-tame Trapper's Hat",
		localized_name = "info.tf2pm.hat.hwn2020_trappers_hat",
		icon = "backpack/workshop/player/items/soldier/hwn2020_trappers_hat/hwn2020_trappers_hat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hwn2020_trappers_hat/hwn2020_trappers_hat.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2020_trappers_hat_style1",
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2020_trappers_hat/hwn2020_trappers_hat.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2020_trappers_hat_style2",
				models = {
					["basename"] = "models/workshop/player/items/soldier/hwn2020_trappers_hat_style2/hwn2020_trappers_hat_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_pocket"},
		paintable = false,
		name = "Lieutenant Bites the Dust",
		localized_name = "info.tf2pm.hat.hw2013_zombites",
		icon = "backpack/workshop/player/items/soldier/hw2013_zombites/hw2013_zombites",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_zombites/hw2013_zombites.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "The Hornblower",
		localized_name = "info.tf2pm.hat.jul13_generals_attire",
		localized_description = "info.tf2pm.hat.jul13_generals_attire_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_generals_attire/jul13_generals_attire",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_generals_attire/jul13_generals_attire.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Cloud Crasher",
		localized_name = "info.tf2pm.hat.cloud_crasher",
		localized_description = "info.tf2pm.hat.cloud_crasher_desc",
		icon = "backpack/workshop/player/items/soldier/cloud_crasher/cloud_crasher",
		models = {
			["basename"] = "models/workshop/player/items/soldier/cloud_crasher/cloud_crasher.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = false,
		name = "The Caped Crusader",
		localized_name = "info.tf2pm.hat.bak_caped_crusader",
		icon = "backpack/workshop/player/items/soldier/bak_caped_crusader/bak_caped_crusader",
		models = {
			["basename"] = "models/workshop/player/items/soldier/bak_caped_crusader/bak_caped_crusader.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.bak_caped_crusader_style1",
			},
			[1] = {
				responsive_skins = {1, 2},
				localized_name = "info.tf2pm.hat.bak_caped_crusader_style2",
			},
		},
	},
	{
		classes = {"soldier"},
		targets = {"beard"},
		paintable = true,
		name = "Marshall's Mutton Chops",
		localized_name = "info.tf2pm.hat.xms2013_soldier_marshal_beard",
		icon = "backpack/workshop/player/items/soldier/xms2013_soldier_marshal_beard/xms2013_soldier_marshal_beard",
		models = {
			["basename"] = "models/workshop/player/items/soldier/xms2013_soldier_marshal_beard/xms2013_soldier_marshal_beard.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"beard"},
		paintable = true,
		name = "EOTL_beard",
		localized_name = "info.tf2pm.hat.eotl_beard",
		icon = "backpack/workshop/player/items/soldier/beard/beard",
		models = {
			["basename"] = "models/workshop/player/items/soldier/beard/beard.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "The Classified Coif",
		localized_name = "info.tf2pm.hat.short2014_soldier_fed_coat",
		icon = "backpack/workshop/player/items/soldier/short2014_soldier_fed_coat/short2014_soldier_fed_coat",
		models = {
			["basename"] = "models/workshop/player/items/soldier/short2014_soldier_fed_coat/short2014_soldier_fed_coat.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Hidden Dragon",
		localized_name = "info.tf2pm.hat.hw2013_hidden_dragon",
		icon = "backpack/workshop/player/items/soldier/hw2013_hidden_dragon/hw2013_hidden_dragon",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_hidden_dragon/hw2013_hidden_dragon.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"necklace"},
		paintable = true,
		name = "Private Maggot Muncher",
		localized_name = "info.tf2pm.hat.spr18_private_maggot_muncher",
		icon = "backpack/workshop/player/items/soldier/spr18_private_maggot_muncher/spr18_private_maggot_muncher",
		models = {
			["basename"] = "models/workshop/player/items/soldier/spr18_private_maggot_muncher/spr18_private_maggot_muncher.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Battle Bird",
		localized_name = "info.tf2pm.hat.sf14_the_battle_bird",
		icon = "backpack/workshop/player/items/soldier/sf14_the_battle_bird/sf14_the_battle_bird",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sf14_the_battle_bird/sf14_the_battle_bird.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"beard"},
		paintable = true,
		name = "Supernatural Stalker",
		localized_name = "info.tf2pm.hat.sf14_the_supernatural_stalker",
		icon = "backpack/workshop/player/items/soldier/sf14_the_supernatural_stalker/sf14_the_supernatural_stalker",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sf14_the_supernatural_stalker/sf14_the_supernatural_stalker.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {},
		paintable = true,
		name = "Public Speaker",
		localized_name = "info.tf2pm.hat.dec19_public_speaker",
		icon = "backpack/workshop/player/items/soldier/dec19_public_speaker/dec19_public_speaker",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec19_public_speaker/dec19_public_speaker.mdl",
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = false,
		name = "Hellhunter's Headpiece",
		localized_name = "info.tf2pm.hat.sf14_hellhunters_headpiece",
		icon = "backpack/workshop/player/items/soldier/sf14_hellhunters_headpiece/sf14_hellhunters_headpiece",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sf14_hellhunters_headpiece/sf14_hellhunters_headpiece.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Elf Defence",
		localized_name = "info.tf2pm.hat.dec21_elf_defence",
		icon = "backpack/workshop/player/items/soldier/dec21_elf_defence/dec21_elf_defence",
		models = {
			["basename"] = "models/workshop/player/items/soldier/dec21_elf_defence/dec21_elf_defence.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"glasses", "hat"},
		paintable = false,
		name = "The Hardium Helm",
		localized_name = "info.tf2pm.hat.hw2013_rocket_ranger",
		icon = "backpack/workshop/player/items/soldier/hw2013_rocket_ranger/hw2013_rocket_ranger",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_rocket_ranger/hw2013_rocket_ranger.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Spellbinder's Bonnet",
		localized_name = "info.tf2pm.hat.hw2013_the_crit_wizard",
		icon = "backpack/workshop/player/items/soldier/hw2013_the_crit_wizard/hw2013_the_crit_wizard",
		models = {
			["basename"] = "models/workshop/player/items/soldier/hw2013_the_crit_wizard/hw2013_the_crit_wizard.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Rebel Rouser",
		localized_name = "info.tf2pm.hat.jul13_ol_jack",
		localized_description = "info.tf2pm.hat.jul13_ol_jack_desc",
		icon = "backpack/workshop/player/items/soldier/jul13_ol_jack/jul13_ol_jack",
		models = {
			["basename"] = "models/workshop/player/items/soldier/jul13_ol_jack/jul13_ol_jack.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Breach and Bomb",
		localized_name = "info.tf2pm.hat.sum20_breach_and_bomb",
		icon = "backpack/workshop/player/items/soldier/sum20_breach_and_bomb/sum20_breach_and_bomb",
		models = {
			["basename"] = "models/workshop/player/items/soldier/sum20_breach_and_bomb/sum20_breach_and_bomb.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},

}
