
-- Auto generated at 2022-02-26 20:22:44 UTC+07:00
return {
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Prussian Pickelhaube",
		localized_name = "info.tf2pm.hat.medic_hat_1",
		localized_description = "info.tf2pm.hat.medic_hat_1_desc",
		icon = "backpack/player/items/medic/medic_helmet",
		models = {
			["basename"] = "models/player/items/medic/medic_helmet.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Vintage Tyrolean",
		localized_name = "info.tf2pm.hat.medic_tyrolean_hat",
		icon = "backpack/player/items/medic/medic_tyrolean",
		models = {
			["basename"] = "models/player/items/medic/medic_tyrolean.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Otolaryngologist's Mirror",
		localized_name = "info.tf2pm.hat.medic_mirror_hat",
		localized_description = "info.tf2pm.hat.medic_mirror_hat_desc",
		icon = "backpack/player/items/medic/medic_mirror",
		models = {
			["basename"] = "models/player/items/medic/medic_mirror.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Medic Mask",
		localized_name = "info.tf2pm.hat.medicmask",
		localized_description = "info.tf2pm.hat.medicmask_desc",
		icon = "backpack/workshop/player/items/medic/medic_mask/medic_mask",
		models = {
			["basename"] = "models/workshop/player/items/medic/medic_mask/medic_mask.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Medic Goggles",
		localized_name = "info.tf2pm.hat.medicgoggles",
		localized_description = "info.tf2pm.hat.medicgoggles_desc",
		icon = "backpack/player/items/medic/medic_goggles",
		models = {
			["basename"] = "models/player/items/medic/medic_goggles.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Medic Gatsby",
		localized_name = "info.tf2pm.hat.medicgatsby",
		localized_description = "info.tf2pm.hat.medicgatsby_desc",
		icon = "backpack/player/items/medic/medic_gatsby",
		models = {
			["basename"] = "models/player/items/medic/medic_gatsby.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"face", "hat"},
		paintable = false,
		name = "Berliner's Bucket Helm",
		localized_name = "info.tf2pm.hat.berlinersbuckethelm",
		icon = "backpack/player/items/medic/berliners_bucket_helm",
		models = {
			["basename"] = "models/player/items/medic/berliners_bucket_helm.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"face"},
		paintable = true,
		name = "Blighted Beak",
		localized_name = "info.tf2pm.hat.blightedbeak",
		icon = "backpack/player/items/medic/blighted_beak",
		models = {
			["basename"] = "models/player/items/medic/medic_blighted_beak.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.blightedbeak_style1",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.blightedbeak_style2",
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "German Gonzila",
		localized_name = "info.tf2pm.hat.germangonzila",
		icon = "backpack/player/items/medic/medic_german_gonzila",
		models = {
			["basename"] = "models/player/items/medic/medic_german_gonzila.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Medic Geisha Hair",
		localized_name = "info.tf2pm.hat.medicgeishahair",
		localized_description = "info.tf2pm.hat.medicgeishahair_desc",
		icon = "backpack/workshop_partner/player/items/medic/shogun_geishahair/shogun_geishahair",
		models = {
			["basename"] = "models/workshop_partner/player/items/medic/shogun_geishahair/shogun_geishahair.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Medic's Mountain Cap",
		localized_name = "info.tf2pm.hat.mountaincap",
		localized_description = "info.tf2pm.hat.mountaincap_desc",
		icon = "backpack/workshop/player/items/medic/fieldcap/fieldcap",
		models = {
			["basename"] = "models/workshop/player/items/medic/fieldcap/fieldcap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Grimm Hatte",
		localized_name = "info.tf2pm.hat.grimmhatte",
		localized_description = "info.tf2pm.hat.grimmhatte_desc",
		icon = "backpack/workshop/player/items/medic/grimm_hatte/grimm_hatte",
		models = {
			["basename"] = "models/workshop/player/items/medic/grimm_hatte/grimm_hatte.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Doctor's Sack",
		localized_name = "info.tf2pm.hat.doctors_sack",
		localized_description = "info.tf2pm.hat.doctors_sack_desc",
		icon = "backpack/player/items/medic/icepack",
		models = {
			["basename"] = "models/player/items/medic/icepack.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Medic MtG Hat",
		localized_name = "info.tf2pm.hat.medicmtghat",
		localized_description = "info.tf2pm.hat.medicmtghat_desc",
		icon = "backpack/player/items/medic/medic_mtg",
		models = {
			["basename"] = "models/player/items/medic/medic_mtg.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Einstein",
		localized_name = "info.tf2pm.hat.hwn_medichat",
		icon = "backpack/player/items/medic/hwn_medic_hat",
		models = {
			["basename"] = "models/player/items/medic/hwn_medic_hat.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"face"},
		paintable = false,
		name = "Dr. Gogglestache",
		localized_name = "info.tf2pm.hat.hwn_medicmisc1",
		icon = "backpack/player/items/medic/hwn_medic_misc1",
		models = {
			["basename"] = "models/player/items/medic/hwn_medic_misc1.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medigun_accessories"},
		paintable = false,
		name = "Emerald Jarate",
		localized_name = "info.tf2pm.hat.hwn_medicmisc2",
		icon = "backpack/player/items/medic/hwn_medic_misc2",
		models = {
			["basename"] = "models/player/items/medic/hwn_medic_misc2.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Surgeon's Stahlhelm",
		localized_name = "info.tf2pm.hat.medichat1",
		localized_description = "info.tf2pm.hat.medichat1_desc",
		icon = "backpack/player/items/medic/fwk_medic_stahlhelm",
		models = {
			["basename"] = "models/player/items/medic/fwk_medic_stahlhelm.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"medal"},
		paintable = true,
		name = "Couvre Corner",
		localized_name = "info.tf2pm.hat.medicpocketsquare",
		localized_description = "info.tf2pm.hat.medicpocketsquare_desc",
		icon = "backpack/player/items/medic/fwk_medic_pocketsquare",
		models = {
			["basename"] = "models/player/items/medic/fwk_medic_pocketsquare.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"necklace"},
		paintable = true,
		name = "The Surgeon's Stethoscope",
		localized_name = "info.tf2pm.hat.medicstethoscope",
		localized_description = "info.tf2pm.hat.medicstethoscope_desc",
		icon = "backpack/player/items/medic/fwk_medic_stethoscope",
		models = {
			["basename"] = "models/player/items/medic/fwk_medic_stethoscope.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_pipe"},
		paintable = true,
		name = "The Nine-Pipe Problem",
		localized_name = "info.tf2pm.hat.medicsmokingpipe",
		localized_description = "info.tf2pm.hat.medicsmokingpipe_desc",
		icon = "backpack/player/items/medic/medic_smokingpipe",
		models = {
			["basename"] = "models/player/items/medic/medic_smokingpipe.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.medicsmokingpipe_style0",
				models = {
					["basename"] = "models/player/items/medic/medic_smokingpipe.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.medicsmokingpipe_style1",
				models = {
					["basename"] = "models/player/items/medic/medic_smokingpipe2.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_gloves"},
		paintable = false,
		name = "The Quadwrangler",
		localized_name = "info.tf2pm.hat.qc_medicglove",
		localized_description = "info.tf2pm.hat.qc_medicglove_desc",
		icon = "backpack/player/items/medic/qc_glove",
		models = {
			["basename"] = "models/player/items/medic/qc_glove.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip"},
		paintable = true,
		name = "The Surgeon's Side Satchel",
		localized_name = "info.tf2pm.hat.medicclipboard",
		localized_description = "info.tf2pm.hat.medicclipboard_desc",
		icon = "backpack/player/items/medic/medic_clipboard",
		models = {
			["basename"] = "models/player/items/medic/medic_clipboard.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Gentleman's Ushanka",
		localized_name = "info.tf2pm.hat.medicushanka",
		localized_description = "info.tf2pm.hat.medicushanka_desc",
		icon = "backpack/player/items/medic/medic_ushanka",
		models = {
			["basename"] = "models/player/items/medic/medic_ushanka.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Medi-Mask",
		localized_name = "info.tf2pm.hat.medicgasmask",
		localized_description = "info.tf2pm.hat.medicgasmask_desc",
		icon = "backpack/workshop/player/items/medic/medic_gasmask/medic_gasmask",
		models = {
			["basename"] = "models/workshop/player/items/medic/medic_gasmask/medic_gasmask.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"left_shoulder"},
		paintable = false,
		name = "Archimedes",
		localized_name = "info.tf2pm.hat.archimedes",
		localized_description = "info.tf2pm.hat.archimedes_desc",
		icon = "backpack/player/items/medic/archimedes",
		models = {
			["basename"] = "models/player/items/medic/archimedes.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"back"},
		paintable = false,
		name = "The Medic Mech-bag",
		localized_name = "info.tf2pm.hat.medic_robot_backpack",
		localized_description = "info.tf2pm.hat.medic_robot_backpack_desc",
		icon = "backpack/player/items/mvm_loot/medic/robo_backpack",
		models = {
			["basename"] = "models/player/items/mvm_loot/medic/robo_backpack.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "The Combat Medic's Crusher Cap",
		localized_name = "info.tf2pm.hat.coh2_medic",
		localized_description = "info.tf2pm.hat.coh2_medic_desc",
		icon = "backpack/player/items/medic/coh_medichat",
		models = {
			["basename"] = "models/player/items/medic/coh_medichat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Foppish Physician",
		localized_name = "info.tf2pm.hat.tw_medic_coat",
		localized_description = "info.tf2pm.hat.tw_medic_coat_desc",
		icon = "backpack/workshop_partner/player/items/medic/tw_coat/tw_coat_medic_necktie",
		models = {
			["basename"] = "models/workshop_partner/player/items/medic/tw_coat/tw_coat_medic_necktie.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_medic_coat_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/medic/tw_coat/tw_coat_medic_necktie.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_medic_coat_style2",
				models = {
					["basename"] = "models/workshop_partner/player/items/medic/tw_coat/tw_coat_medic.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Der Wintermantel",
		localized_name = "info.tf2pm.hat.derwintermantel",
		localized_description = "info.tf2pm.hat.derwintermantel_desc",
		icon = "backpack/workshop/player/items/medic/medic_wintercoat_s01/medic_wintercoat_s01",
		models = {
			["basename"] = "models/workshop/player/items/medic/medic_wintercoat_s01/medic_wintercoat_s01.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.derwintermantel_style0",
				models = {
					["basename"] = "models/workshop/player/items/medic/medic_wintercoat_s01/medic_wintercoat_s01.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.derwintermantel_style1",
				models = {
					["basename"] = "models/workshop/player/items/medic/medic_wintercoat_s02/medic_wintercoat_s02.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Doc's Holiday",
		localized_name = "info.tf2pm.hat.docsholiday",
		localized_description = "info.tf2pm.hat.docsholiday_desc",
		icon = "backpack/workshop/player/items/medic/hawaiian_shirt/hawaiian_shirt",
		models = {
			["basename"] = "models/workshop/player/items/medic/hawaiian_shirt/hawaiian_shirt.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.docsholiday_style0",
				models = {
					["basename"] = "models/workshop/player/items/medic/hawaiian_shirt/hawaiian_shirt.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.docsholiday_style1",
				models = {
					["basename"] = "models/workshop/player/items/medic/hawaiian_shirt_s2/hawaiian_shirt_s2.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.docsholiday_style2",
				models = {
					["basename"] = "models/workshop/player/items/medic/hawaiian_shirt_s3/hawaiian_shirt_s3.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "The Weather Master",
		localized_name = "info.tf2pm.hat.weathermaster",
		localized_description = "info.tf2pm.hat.weathermaster_desc",
		icon = "backpack/workshop_partner/player/items/medic/as_medic_cloud_hat/as_medic_cloud_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/medic/as_medic_cloud_hat/as_medic_cloud_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Medic",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_medic_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_medic_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_medic_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Das Maddendoktor",
		localized_name = "info.tf2pm.hat.jul13_madmans_mop",
		localized_description = "info.tf2pm.hat.jul13_madmans_mop_desc",
		icon = "backpack/workshop/player/items/medic/jul13_madmans_mop/jul13_madmans_mop",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_madmans_mop/jul13_madmans_mop.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Slick Cut",
		localized_name = "info.tf2pm.hat.fall2013_medic_wc_hair",
		icon = "backpack/workshop/player/items/medic/fall2013_medic_wc_hair/fall2013_medic_wc_hair",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall2013_medic_wc_hair/fall2013_medic_wc_hair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "A Brush with Death",
		localized_name = "info.tf2pm.hat.fall2013_medic_wc_beard",
		icon = "backpack/workshop/player/items/medic/fall2013_medic_wc_beard/fall2013_medic_wc_beard",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall2013_medic_wc_beard/fall2013_medic_wc_beard.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"pants"},
		paintable = true,
		name = "Night Ward",
		localized_name = "info.tf2pm.hat.dec20_night_ward",
		icon = "backpack/workshop/player/items/medic/dec20_night_ward/dec20_night_ward",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec20_night_ward/dec20_night_ward.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec20_night_ward_style0",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec20_night_ward/dec20_night_ward.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec20_night_ward_style1",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec20_night_ward_style1/dec20_night_ward_style1.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Das Gutenkutteharen",
		localized_name = "info.tf2pm.hat.jul13_class_act",
		localized_description = "info.tf2pm.hat.jul13_class_act_desc",
		icon = "backpack/workshop/player/items/medic/jul13_class_act/jul13_class_act",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_class_act/jul13_class_act.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"back"},
		paintable = false,
		name = "Wings of Purity",
		localized_name = "info.tf2pm.hat.sf14_purity_wings",
		icon = "backpack/workshop/player/items/medic/sf14_purity_wings/sf14_purity_wings",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_purity_wings/sf14_purity_wings.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"face"},
		paintable = true,
		name = "The Byte'd Beak",
		localized_name = "info.tf2pm.hat.robo_medic_blighted_beak",
		localized_description = "info.tf2pm.hat.robo_medic_blighted_beak_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_blighted_beak/robo_medic_blighted_beak",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_blighted_beak/robo_medic_blighted_beak.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"whole_head"},
		paintable = false,
		name = "Templar's Spirit",
		localized_name = "info.tf2pm.hat.sf14_templar_hood",
		icon = "backpack/workshop/player/items/medic/sf14_templar_hood/sf14_templar_hood",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_templar_hood/sf14_templar_hood.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_pipe"},
		paintable = false,
		name = "The Steam Pipe",
		localized_name = "info.tf2pm.hat.robo_medic_ninepipe_problem",
		localized_description = "info.tf2pm.hat.robo_medic_ninepipe_problem_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_ninepipe_problem/robo_medic_ninepipe_problem",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_ninepipe_problem/robo_medic_ninepipe_problem.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Ward",
		localized_name = "info.tf2pm.hat.fall2013_medic_wc_coat",
		icon = "backpack/workshop/player/items/medic/fall2013_medic_wc_coat/fall2013_medic_wc_coat",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall2013_medic_wc_coat/fall2013_medic_wc_coat.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"feet"},
		paintable = true,
		name = "The Gaiter Guards",
		localized_name = "info.tf2pm.hat.sbox2014_medic_wintergarb_gaiter",
		icon = "backpack/workshop/player/items/medic/sbox2014_medic_wintergarb_gaiter/sbox2014_medic_wintergarb_gaiter",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_medic_wintergarb_gaiter/sbox2014_medic_wintergarb_gaiter.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Scourge of the Sky",
		localized_name = "info.tf2pm.hat.spr18_scourge_of_the_sky",
		icon = "backpack/workshop/player/items/medic/spr18_scourge_of_the_sky/spr18_scourge_of_the_sky",
		models = {
			["basename"] = "models/workshop/player/items/medic/spr18_scourge_of_the_sky/spr18_scourge_of_the_sky.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Battle Boonie",
		localized_name = "info.tf2pm.hat.fall17_battle_boonie",
		localized_description = "info.tf2pm.hat.fall17_battle_boonie_desc",
		icon = "backpack/workshop/player/items/medic/fall17_battle_boonie/fall17_battle_boonie",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall17_battle_boonie/fall17_battle_boonie.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Vitals Vest",
		localized_name = "info.tf2pm.hat.fall17_vitals_vest",
		localized_description = "info.tf2pm.hat.fall17_vitals_vest_desc",
		icon = "backpack/workshop/player/items/medic/fall17_vitals_vest/fall17_vitals_vest",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall17_vitals_vest/fall17_vitals_vest.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Mann of Reason",
		localized_name = "info.tf2pm.hat.xms2013_medic_knecht_hat",
		icon = "backpack/workshop/player/items/medic/xms2013_medic_knecht_hat/xms2013_medic_knecht_hat",
		models = {
			["basename"] = "models/workshop/player/items/medic/xms2013_medic_knecht_hat/xms2013_medic_knecht_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Elf Care Provider",
		localized_name = "info.tf2pm.hat.dec20_elf_care_provider",
		icon = "backpack/workshop/player/items/medic/dec20_elf_care_provider/dec20_elf_care_provider",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec20_elf_care_provider/dec20_elf_care_provider.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Virus Doctor",
		localized_name = "info.tf2pm.hat.robo_medic_grimm_hatte",
		localized_description = "info.tf2pm.hat.robo_medic_grimm_hatte_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_grimm_hatte/robo_medic_grimm_hatte",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_grimm_hatte/robo_medic_grimm_hatte.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Mighty Mitre",
		localized_name = "info.tf2pm.hat.dec18_mighty_mitre",
		icon = "backpack/workshop/player/items/medic/dec18_mighty_mitre/dec18_mighty_mitre",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec18_mighty_mitre/dec18_mighty_mitre.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Platinum Pickelhaube",
		localized_name = "info.tf2pm.hat.robo_medic_pickelhaube",
		localized_description = "info.tf2pm.hat.robo_medic_pickelhaube_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_pickelhaube/robo_medic_pickelhaube",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_pickelhaube/robo_medic_pickelhaube.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip"},
		paintable = true,
		name = "Pocket-Medes",
		localized_name = "info.tf2pm.hat.dec19_pocketmedes",
		icon = "backpack/workshop/player/items/medic/dec19_pocketmedes/dec19_pocketmedes",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec19_pocketmedes/dec19_pocketmedes.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_pocketmedes_style0",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec19_pocketmedes/dec19_pocketmedes.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec19_pocketmedes_style1",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec19_pocketmedes_s2/dec19_pocketmedes_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Halogen Head Lamp",
		localized_name = "info.tf2pm.hat.robo_medic_otolaryngologists_mirror",
		localized_description = "info.tf2pm.hat.robo_medic_otolaryngologists_mirror_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_otolaryngologists_mirror/robo_medic_otolaryngologists_mirror",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_otolaryngologists_mirror/robo_medic_otolaryngologists_mirror.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Mecha-Medes",
		localized_name = "info.tf2pm.hat.robo_medic_archimedes",
		localized_description = "info.tf2pm.hat.robo_medic_archimedes_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_archimedes/robo_medic_archimedes",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_archimedes/robo_medic_archimedes.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "The Angel of Death",
		localized_name = "info.tf2pm.hat.xms2013_medic_robe",
		icon = "backpack/workshop/player/items/medic/xms2013_medic_robe/xms2013_medic_robe",
		models = {
			["basename"] = "models/workshop/player/items/medic/xms2013_medic_robe/xms2013_medic_robe.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "The Medical Mystery",
		localized_name = "info.tf2pm.hat.fall2013_aichi_investigator",
		icon = "backpack/workshop/player/items/medic/fall2013_aichi_investigator/fall2013_aichi_investigator",
		models = {
			["basename"] = "models/workshop/player/items/medic/fall2013_aichi_investigator/fall2013_aichi_investigator.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Nunhood",
		localized_name = "info.tf2pm.hat.xms2013_medic_hood",
		icon = "backpack/workshop/player/items/medic/xms2013_medic_hood/xms2013_medic_hood",
		models = {
			["basename"] = "models/workshop/player/items/medic/xms2013_medic_hood/xms2013_medic_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Practitioner's Processing Mask",
		localized_name = "info.tf2pm.hat.robo_medic_physician_mask",
		localized_description = "info.tf2pm.hat.robo_medic_physician_mask_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_physician_mask/robo_medic_physician_mask",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_physician_mask/robo_medic_physician_mask.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Snowcapped",
		localized_name = "info.tf2pm.hat.dec18_snowcapped",
		icon = "backpack/workshop/player/items/medic/dec18_snowcapped/dec18_snowcapped",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec18_snowcapped/dec18_snowcapped.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "The Trepanabotomizer",
		localized_name = "info.tf2pm.hat.hw2013_quacks_cureall",
		icon = "backpack/workshop/player/items/medic/hw2013_quacks_cureall/hw2013_quacks_cureall",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_quacks_cureall/hw2013_quacks_cureall.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt", "sleeves"},
		paintable = false,
		name = "Ramses' Regalia",
		localized_name = "info.tf2pm.hat.hw2013_ramses_regalia",
		icon = "backpack/workshop/player/items/medic/hw2013_ramses_regalia/hw2013_ramses_regalia",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_ramses_regalia/hw2013_ramses_regalia.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Das Naggenvatcher",
		localized_name = "info.tf2pm.hat.jul13_bro_plate",
		localized_description = "info.tf2pm.hat.jul13_bro_plate_desc",
		icon = "backpack/workshop/player/items/medic/jul13_bro_plate/jul13_bro_plate",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_bro_plate/jul13_bro_plate.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "Das Metalmeatencasen",
		localized_name = "info.tf2pm.hat.jul13_heavy_defender",
		localized_description = "info.tf2pm.hat.jul13_heavy_defender_desc",
		icon = "backpack/workshop/player/items/medic/jul13_heavy_defender/jul13_heavy_defender",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_heavy_defender/jul13_heavy_defender.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Santarchimedes",
		localized_name = "info.tf2pm.hat.dec16_santarchimedes",
		localized_description = "info.tf2pm.hat.dec16_santarchimedes_desc",
		icon = "backpack/workshop/player/items/medic/dec16_santarchimedes/dec16_santarchimedes",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec16_santarchimedes/dec16_santarchimedes.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Das Ubersternmann",
		localized_name = "info.tf2pm.hat.jul13_secret_state_surgeon",
		localized_description = "info.tf2pm.hat.jul13_secret_state_surgeon_desc",
		icon = "backpack/workshop/player/items/medic/jul13_secret_state_surgeon/jul13_secret_state_surgeon",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_secret_state_surgeon/jul13_secret_state_surgeon.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Colonel's Coat",
		localized_name = "info.tf2pm.hat.sbox2014_medic_colonel_coat",
		icon = "backpack/workshop/player/items/medic/sbox2014_medic_colonel_coat/sbox2014_medic_colonel_coat",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_medic_colonel_coat/sbox2014_medic_colonel_coat.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "Medimedes",
		localized_name = "info.tf2pm.hat.hw2013_medicmedes",
		icon = "backpack/workshop/player/items/medic/hw2013_medicmedes/hw2013_medicmedes",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_medicmedes/hw2013_medicmedes.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Physician's Protector",
		localized_name = "info.tf2pm.hat.cc_summer2015_the_physicians_protector",
		icon = "backpack/workshop/player/items/medic/cc_summer2015_the_physicians_protector/cc_summer2015_the_physicians_protector",
		models = {
			["basename"] = "models/workshop/player/items/medic/cc_summer2015_the_physicians_protector/cc_summer2015_the_physicians_protector.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Bunnyhopper's Ballistics Vest",
		localized_name = "info.tf2pm.hat.dec15_bunnyhoppers_ballistics_vest",
		icon = "backpack/workshop/player/items/medic/dec15_bunnyhoppers_ballistics_vest/dec15_bunnyhoppers_ballistics_vest",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec15_bunnyhoppers_ballistics_vest/dec15_bunnyhoppers_ballistics_vest.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"beard", "hat"},
		paintable = true,
		name = "Berlin Brain Bowl",
		localized_name = "info.tf2pm.hat.dec15_berlin_brain_bowl",
		icon = "backpack/workshop/player/items/medic/dec15_berlin_brain_bowl/dec15_berlin_brain_bowl",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec15_berlin_brain_bowl/dec15_berlin_brain_bowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Transylvanian Toupe",
		localized_name = "info.tf2pm.hat.sf14_vampire_makeover",
		icon = "backpack/workshop/player/items/medic/sf14_vampire_makeover/sf14_vampire_makeover",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_vampire_makeover/sf14_vampire_makeover.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Smock Surgeon",
		localized_name = "info.tf2pm.hat.sbox2014_medic_apron",
		icon = "backpack/workshop/player/items/medic/sbox2014_medic_apron/sbox2014_medic_apron",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_medic_apron/sbox2014_medic_apron.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Heat of Winter",
		localized_name = "info.tf2pm.hat.sbxo2014_medic_wintergarb_coat",
		icon = "backpack/workshop/player/items/medic/sbxo2014_medic_wintergarb_coat/sbxo2014_medic_wintergarb_coat",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbxo2014_medic_wintergarb_coat/sbxo2014_medic_wintergarb_coat.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Das Blutliebhaber",
		localized_name = "info.tf2pm.hat.hw2013_das_blutliebhaber",
		icon = "backpack/workshop/player/items/medic/hw2013_das_blutliebhaber/hw2013_das_blutliebhaber",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_das_blutliebhaber/hw2013_das_blutliebhaber.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "Burly Beast",
		localized_name = "info.tf2pm.hat.hwn2016_burly_beast",
		icon = "backpack/workshop/player/items/medic/hwn2016_burly_beast/hwn2016_burly_beast",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2016_burly_beast/hwn2016_burly_beast.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"necklace"},
		paintable = false,
		name = "The Chronoscarf",
		localized_name = "info.tf2pm.hat.short2014_chronoscarf",
		icon = "backpack/workshop/player/items/medic/short2014_chronoscarf/short2014_chronoscarf",
		models = {
			["basename"] = "models/workshop/player/items/medic/short2014_chronoscarf/short2014_chronoscarf.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"feet"},
		paintable = false,
		name = "The Lo-Grav Loafers",
		localized_name = "info.tf2pm.hat.hw2013_moon_boots",
		icon = "backpack/workshop/player/items/medic/hw2013_moon_boots/hw2013_moon_boots",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_moon_boots/hw2013_moon_boots.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "The Vascular Vestment",
		localized_name = "info.tf2pm.hat.cc_summer2015_the_vascular_vestment",
		icon = "backpack/workshop/player/items/medic/cc_summer2015_the_vascular_vestment/cc_summer2015_the_vascular_vestment",
		models = {
			["basename"] = "models/workshop/player/items/medic/cc_summer2015_the_vascular_vestment/cc_summer2015_the_vascular_vestment.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Medic",
		localized_name = "info.tf2pm.hat.item_zombiemedic",
		icon = "backpack/player/items/medic/medic_zombie",
		models = {
			["basename"] = "models/player/items/medic/medic_zombie.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip"},
		paintable = true,
		name = "Das Feelinbeterbager",
		localized_name = "info.tf2pm.hat.jul13_emergency_supplies",
		localized_description = "info.tf2pm.hat.jul13_emergency_supplies_desc",
		icon = "backpack/workshop/player/items/medic/jul13_emergency_supplies/jul13_emergency_supplies",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_emergency_supplies/jul13_emergency_supplies.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Das Hazmattenhatten",
		localized_name = "info.tf2pm.hat.jul13_positive_pressure_veil",
		localized_description = "info.tf2pm.hat.jul13_positive_pressure_veil_desc",
		icon = "backpack/workshop/player/items/medic/jul13_positive_pressure_veil/jul13_positive_pressure_veil",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_positive_pressure_veil/jul13_positive_pressure_veil.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"medal"},
		paintable = true,
		name = "Pocket Heavy",
		localized_name = "info.tf2pm.hat.sept2014_pocket_heavy",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/medic/sept2014_pocket_heavy/sept2014_pocket_heavy",
		models = {
			["basename"] = "models/workshop/player/items/medic/sept2014_pocket_heavy/sept2014_pocket_heavy.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"beard", "hat"},
		paintable = true,
		name = "Teutonkahmun",
		localized_name = "info.tf2pm.hat.hw2013_teutonkahmun",
		icon = "backpack/workshop/player/items/medic/hw2013_teutonkahmun/hw2013_teutonkahmun",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_teutonkahmun/hw2013_teutonkahmun.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"sleeves"},
		paintable = false,
		name = "Kriegsmaschine-9000",
		localized_name = "info.tf2pm.hat.sf14_medic_kriegsmaschine_9000",
		icon = "backpack/workshop/player/items/medic/sf14_medic_kriegsmaschine_9000/sf14_medic_kriegsmaschine_9000",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_medic_kriegsmaschine_9000/sf14_medic_kriegsmaschine_9000.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"whole_head"},
		paintable = true,
		name = "Hundekopf",
		localized_name = "info.tf2pm.hat.sf14_medic_hundkopf",
		icon = "backpack/workshop/player/items/medic/sf14_medic_hundkopf/sf14_medic_hundkopf",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_medic_hundkopf/sf14_medic_hundkopf.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip"},
		paintable = true,
		name = "Rolfe Copter",
		localized_name = "info.tf2pm.hat.hwn2020_rolfe_copter",
		icon = "backpack/workshop/player/items/medic/hwn2020_rolfe_copter/hwn2020_rolfe_copter",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2020_rolfe_copter/hwn2020_rolfe_copter.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip", "shirt"},
		paintable = false,
		name = "Herzensbrecher",
		localized_name = "info.tf2pm.hat.sf14_medic_herzensbrecher",
		icon = "backpack/workshop/player/items/medic/sf14_medic_herzensbrecher/sf14_medic_herzensbrecher",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_medic_herzensbrecher/sf14_medic_herzensbrecher.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Coldfront Carapace",
		localized_name = "info.tf2pm.hat.dec17_coldfront_carapace",
		icon = "backpack/workshop/player/items/medic/dec17_coldfront_carapace/dec17_coldfront_carapace",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec17_coldfront_carapace/dec17_coldfront_carapace.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Medical Monarch",
		localized_name = "info.tf2pm.hat.dec15_medic_winter_jacket2_emblem",
		icon = "backpack/workshop/player/items/medic/dec15_medic_winter_jacket2_emblem/dec15_medic_winter_jacket2_emblem",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec15_medic_winter_jacket2_emblem/dec15_medic_winter_jacket2_emblem.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_medic_winter_jacket2_emblem_style1",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec15_medic_winter_jacket2_emblem/dec15_medic_winter_jacket2_emblem.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec15_medic_winter_jacket2_emblem_style2",
				models = {
					["basename"] = "models/workshop/player/items/medic/dec15_medic_winter_jacket2_emblem2/dec15_medic_winter_jacket2_emblem2.mdl",
				}
			},
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Coldfront Commander",
		localized_name = "info.tf2pm.hat.dec17_coldfront_commander",
		icon = "backpack/workshop/player/items/medic/dec17_coldfront_commander/dec17_coldfront_commander",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec17_coldfront_commander/dec17_coldfront_commander.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Ze Ubermensch",
		localized_name = "info.tf2pm.hat.short2014_medic_nietzsche",
		icon = "backpack/workshop/player/items/medic/short2014_medic_nietzsche/short2014_medic_nietzsche",
		models = {
			["basename"] = "models/workshop/player/items/medic/short2014_medic_nietzsche/short2014_medic_nietzsche.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Teutonic Toque",
		localized_name = "info.tf2pm.hat.sbox2014_teutonic_toque",
		icon = "backpack/workshop/player/items/medic/sbox2014_teutonic_toque/sbox2014_teutonic_toque",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_teutonic_toque/sbox2014_teutonic_toque.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Derangement Garment",
		localized_name = "info.tf2pm.hat.hwn2019_derangement_garment",
		icon = "backpack/workshop/player/items/medic/hwn2019_derangement_garment/hwn2019_derangement_garment",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2019_derangement_garment/hwn2019_derangement_garment.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Madmann's Muzzle",
		localized_name = "info.tf2pm.hat.hwn2019_madmanns_muzzle",
		icon = "backpack/workshop/player/items/medic/hwn2019_madmanns_muzzle/hwn2019_madmanns_muzzle",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2019_madmanns_muzzle/hwn2019_madmanns_muzzle.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "The Fashionable Megalomaniac",
		localized_name = "info.tf2pm.hat.sbox2014_fashionable_megalomaniac",
		icon = "backpack/workshop/player/items/medic/sbox2014_fashionable_megalomaniac/sbox2014_fashionable_megalomaniac",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_fashionable_megalomaniac/sbox2014_fashionable_megalomaniac.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"medic_hip"},
		paintable = true,
		name = "The Medicine Manpurse",
		localized_name = "info.tf2pm.hat.short2014_medic_messenger_bag",
		icon = "backpack/workshop/player/items/medic/short2014_medic_messenger_bag/short2014_medic_messenger_bag",
		models = {
			["basename"] = "models/workshop/player/items/medic/short2014_medic_messenger_bag/short2014_medic_messenger_bag.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = false,
		name = "tw_medibot_hat",
		localized_name = "info.tf2pm.hat.tw_medibot_hat",
		icon = "backpack/workshop/player/items/medic/tw_medibot_hat/tw_medibot_hat",
		models = {
			["basename"] = "models/workshop/player/items/medic/tw_medibot_hat/tw_medibot_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Archimedes the Undying",
		localized_name = "info.tf2pm.hat.hw2013_zombie_archimedes",
		icon = "backpack/workshop/player/items/medic/hw2013_zombie_archimedes/hw2013_zombie_archimedes",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_zombie_archimedes/hw2013_zombie_archimedes.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"feet"},
		paintable = false,
		name = "tw_medibot_chariot",
		localized_name = "info.tf2pm.hat.tw_medibot_chariot",
		icon = "backpack/workshop/player/items/medic/tw_medibot_chariot/tw_medibot_chariot",
		models = {
			["basename"] = "models/workshop/player/items/medic/tw_medibot_chariot/tw_medibot_chariot.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Das Fantzipantzen",
		localized_name = "info.tf2pm.hat.jul13_uncivil_servant",
		localized_description = "info.tf2pm.hat.jul13_uncivil_servant_desc",
		icon = "backpack/workshop/player/items/medic/jul13_uncivil_servant/jul13_uncivil_servant",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_uncivil_servant/jul13_uncivil_servant.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Titanium Tyrolean",
		localized_name = "info.tf2pm.hat.robo_medic_tyrolean",
		localized_description = "info.tf2pm.hat.robo_medic_tyrolean_desc",
		icon = "backpack/workshop/player/items/medic/robo_medic_tyrolean/robo_medic_tyrolean",
		models = {
			["basename"] = "models/workshop/player/items/medic/robo_medic_tyrolean/robo_medic_tyrolean.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"arms"},
		paintable = true,
		name = "The Surgeon's Sidearms",
		localized_name = "info.tf2pm.hat.hwn2016_surgeons_sidearms",
		icon = "backpack/workshop/player/items/medic/hwn2016_surgeons_sidearms/hwn2016_surgeons_sidearms",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2016_surgeons_sidearms/hwn2016_surgeons_sidearms.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "The Surgeon's Space Suit",
		localized_name = "info.tf2pm.hat.hw2013_spacemans_suit",
		icon = "backpack/workshop/player/items/medic/hw2013_spacemans_suit/hw2013_spacemans_suit",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_spacemans_suit/hw2013_spacemans_suit.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"head_skin"},
		paintable = false,
		name = "The Second Opinion",
		localized_name = "info.tf2pm.hat.hw2013_second_opinion",
		icon = "backpack/workshop/player/items/medic/hw2013_second_opinion/hw2013_second_opinion",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_second_opinion/hw2013_second_opinion.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"glasses"},
		paintable = true,
		name = "Flatliner",
		localized_name = "info.tf2pm.hat.sum20_flatliner",
		icon = "backpack/workshop/player/items/medic/sum20_flatliner/sum20_flatliner",
		models = {
			["basename"] = "models/workshop/player/items/medic/sum20_flatliner/sum20_flatliner.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"necklace"},
		paintable = true,
		name = "Self-Care",
		localized_name = "info.tf2pm.hat.sum20_self_care",
		icon = "backpack/workshop/player/items/medic/sum20_self_care/sum20_self_care",
		models = {
			["basename"] = "models/workshop/player/items/medic/sum20_self_care/sum20_self_care.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Baron von Havenaplane",
		localized_name = "info.tf2pm.hat.jul13_montys_menace",
		localized_description = "info.tf2pm.hat.jul13_montys_menace_desc",
		icon = "backpack/workshop/player/items/medic/jul13_montys_menace/jul13_montys_menace",
		models = {
			["basename"] = "models/workshop/player/items/medic/jul13_montys_menace/jul13_montys_menace.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"face", "hat"},
		paintable = true,
		name = "The Alternative Medicine Mann",
		localized_name = "info.tf2pm.hat.hw2013_witching_ward",
		icon = "backpack/workshop/player/items/medic/hw2013_witching_ward/hw2013_witching_ward",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_witching_ward/hw2013_witching_ward.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["head"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "Miser's Muttonchops",
		localized_name = "info.tf2pm.hat.dec18_misers_muttonchops",
		icon = "backpack/workshop/player/items/medic/dec18_misers_muttonchops/dec18_misers_muttonchops",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec18_misers_muttonchops/dec18_misers_muttonchops.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Shaman's Skull",
		localized_name = "info.tf2pm.hat.hw2013_shamans_skull",
		icon = "backpack/workshop/player/items/medic/hw2013_shamans_skull/hw2013_shamans_skull",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_shamans_skull/hw2013_shamans_skull.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Heer's Helmet",
		localized_name = "info.tf2pm.hat.sbox2014_medic_wintergarb_helmet",
		icon = "backpack/workshop/player/items/medic/sbox2014_medic_wintergarb_helmet/sbox2014_medic_wintergarb_helmet",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_medic_wintergarb_helmet/sbox2014_medic_wintergarb_helmet.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Optic Nerve",
		localized_name = "info.tf2pm.hat.hwn2021_optic_nerve",
		icon = "backpack/workshop/player/items/medic/hwn2021_optic_nerve/hwn2021_optic_nerve",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2021_optic_nerve/hwn2021_optic_nerve.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Colossal Cranium",
		localized_name = "info.tf2pm.hat.hwn2016_colossal_cranium_2",
		icon = "backpack/workshop/player/items/medic/hwn2016_colossal_cranium_2/hwn2016_colossal_cranium_2",
		models = {
			["basename"] = "models/workshop/player/items/medic/hwn2016_colossal_cranium_2/hwn2016_colossal_cranium_2.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "Vampiric Vesture",
		localized_name = "info.tf2pm.hat.sf14_vampiric_vesture",
		icon = "backpack/workshop/player/items/medic/sf14_vampiric_vesture/sf14_vampiric_vesture",
		models = {
			["basename"] = "models/workshop/player/items/medic/sf14_vampiric_vesture/sf14_vampiric_vesture.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "Field Practice",
		localized_name = "info.tf2pm.hat.spr17_field_practice",
		localized_description = "info.tf2pm.hat.spr17_field_practice_desc",
		icon = "backpack/workshop/player/items/medic/spr17_field_practice/spr17_field_practice",
		models = {
			["basename"] = "models/workshop/player/items/medic/spr17_field_practice/spr17_field_practice.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"face"},
		paintable = true,
		name = "dec2014 medic_unknown_mann",
		localized_name = "info.tf2pm.hat.dec2014_medic_unknown_mann",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/medic/dec2014_medic_unknown_mann/dec2014_medic_unknown_mann",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec2014_medic_unknown_mann/dec2014_medic_unknown_mann.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 Surgeon's Shako",
		localized_name = "info.tf2pm.hat.dec2014_surgeons_shako",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/medic/dec2014_surgeons_shako/dec2014_surgeons_shako",
		models = {
			["basename"] = "models/workshop/player/items/medic/dec2014_surgeons_shako/dec2014_surgeons_shako.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Gauzed Gaze",
		localized_name = "info.tf2pm.hat.may16_gauzed_gaze",
		icon = "backpack/workshop/player/items/medic/surgical_stare/surgical_stare",
		models = {
			["basename"] = "models/workshop/player/items/medic/surgical_stare/surgical_stare.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"beard"},
		paintable = true,
		name = "The Ruffled Ruprecht",
		localized_name = "info.tf2pm.hat.xms2013_ruffled_beard",
		icon = "backpack/workshop/player/items/medic/xms2013_ruffled_beard/xms2013_ruffled_beard",
		models = {
			["basename"] = "models/workshop/player/items/medic/xms2013_ruffled_beard/xms2013_ruffled_beard.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"pants"},
		paintable = true,
		name = "The Surgical Survivalist",
		localized_name = "info.tf2pm.hat.may16_surgical_survivalist",
		icon = "backpack/workshop/player/items/medic/cardiologists_camo/cardiologists_camo",
		models = {
			["basename"] = "models/workshop/player/items/medic/cardiologists_camo/cardiologists_camo.mdl",
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = false,
		name = "The Vicar's Vestments",
		localized_name = "info.tf2pm.hat.hw2013_medic_undertaker_vest",
		icon = "backpack/workshop/player/items/medic/hw2013_medic_undertaker_vest/hw2013_medic_undertaker_vest",
		models = {
			["basename"] = "models/workshop/player/items/medic/hw2013_medic_undertaker_vest/hw2013_medic_undertaker_vest.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Dough Puncher",
		localized_name = "info.tf2pm.hat.sbox2014_chefs_coat",
		icon = "backpack/workshop/player/items/medic/sbox2014_chefs_coat/sbox2014_chefs_coat",
		models = {
			["basename"] = "models/workshop/player/items/medic/sbox2014_chefs_coat/sbox2014_chefs_coat.mdl",
		},
	},

}
