
-- Auto generated at 2022-02-26 20:22:44 UTC+07:00
return {
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Batter's Helmet",
		localized_name = "info.tf2pm.hat.scout_hat_1",
		localized_description = "info.tf2pm.hat.scout_hat_1_desc",
		icon = "backpack/player/items/scout/batter_helmet",
		models = {
			["basename"] = "models/player/items/scout/batter_helmet.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hatandheadphones_style",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nohat_noheadphones_style",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.nohat_style",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Bonk Helm",
		localized_name = "info.tf2pm.hat.scout_bonk_helmet",
		localized_description = "info.tf2pm.hat.scout_bonk_helmet_desc",
		icon = "backpack/player/items/scout/bonk_helmet",
		models = {
			["basename"] = "models/player/items/scout/bonk_helmet.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Ye Olde Baker Boy",
		localized_name = "info.tf2pm.hat.scout_newsboy_cap",
		localized_description = "info.tf2pm.hat.scout_newsboy_cap_desc",
		icon = "backpack/player/items/scout/newsboy_cap",
		models = {
			["basename"] = "models/player/items/scout/newsboy_cap.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "Baseball Bill's Sports Shine",
		localized_name = "info.tf2pm.hat.hatless_scout",
		localized_description = "info.tf2pm.hat.hatless_scout_desc",
		icon = "backpack/player/items/scout/scout_nohat",
		models = {
			["basename"] = "models/player/items/scout/scout_nohat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Scout Beanie",
		localized_name = "info.tf2pm.hat.scoutbeanie",
		localized_description = "info.tf2pm.hat.scoutbeanie_desc",
		icon = "backpack/player/items/scout/beanie",
		models = {
			["basename"] = "models/player/items/scout/beanie.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scoutbeanie_style0",
			},
			[1] = {
				responsive_skins = {4, 5},
				localized_name = "info.tf2pm.hat.scoutbeanie_style1",
			},
			[2] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.scoutbeanie_style2",
			},
			[3] = {
				responsive_skins = {6, 7},
				localized_name = "info.tf2pm.hat.scoutbeanie_style3",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Scout Whoopee Cap",
		localized_name = "info.tf2pm.hat.scoutwhoopee",
		localized_description = "info.tf2pm.hat.scoutwhoopee_desc",
		icon = "backpack/player/items/scout/scout_whoopee",
		models = {
			["basename"] = "models/player/items/scout/scout_whoopee.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Milkman",
		localized_name = "info.tf2pm.hat.themilkman",
		icon = "backpack/player/items/scout/milkhat",
		models = {
			["basename"] = "models/player/items/scout/milkhat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Bombing Run",
		localized_name = "info.tf2pm.hat.scoutbombingrun",
		localized_description = "info.tf2pm.hat.scoutbombingrun_desc",
		icon = "backpack/player/items/scout/pilot_protector",
		models = {
			["basename"] = "models/player/items/scout/pilot_protector.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Flipped Trilby",
		localized_name = "info.tf2pm.hat.flippedtrilby",
		icon = "backpack/player/items/scout/scout_flipped_trilby",
		models = {
			["basename"] = "models/player/items/scout/scout_flipped_trilby.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "MNC Mascot Hat",
		localized_name = "info.tf2pm.hat.mnc_mascot_hat",
		icon = "backpack/player/items/scout/mnc_mascot_hat",
		models = {
			["basename"] = "models/player/items/scout/mnc_mascot_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet", "sleeves"},
		paintable = true,
		name = "MNC Mascot Outfit",
		localized_name = "info.tf2pm.hat.mnc_mascot_outfit",
		icon = "backpack/player/items/scout/mnc_mascot_outfit",
		models = {
			["basename"] = "models/player/items/scout/mnc_mascot_outfit.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "Bonk Boy",
		localized_name = "info.tf2pm.hat.bonkboy",
		icon = "backpack/workshop/player/items/scout/bonk_mask/bonk_mask",
		models = {
			["basename"] = "models/workshop/player/items/scout/bonk_mask/bonk_mask.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bonkboy_style_withnohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.bonkboy_style_withhat",
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Hero's Tail",
		localized_name = "info.tf2pm.hat.scouthair",
		localized_description = "info.tf2pm.hat.scouthair_desc",
		icon = "backpack/player/items/crafting/scout_hair_icon",
		models = {
			["basename"] = "models/player/items/scout/scout_hair.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.herostail_style0",
			},
			[1] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.herostail_style1",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"necklace"},
		paintable = true,
		name = "Sign of the Wolf's School",
		localized_name = "info.tf2pm.hat.scoutmedallion",
		localized_description = "info.tf2pm.hat.scoutmedallion_desc",
		icon = "backpack/player/items/scout/scout_medallion",
		models = {
			["basename"] = "models/player/items/scout/scout_medallion.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "Scout MtG Hat",
		localized_name = "info.tf2pm.hat.scoutmtghat",
		localized_description = "info.tf2pm.hat.scoutmtghat_desc",
		icon = "backpack/player/items/scout/scout_mtg",
		models = {
			["basename"] = "models/player/items/scout/scout_mtg.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Scout Flip-Flops",
		localized_name = "info.tf2pm.hat.scout_flipflops",
		localized_description = "info.tf2pm.hat.scout_flipflops_desc",
		icon = "backpack/player/items/scout/flipflops",
		models = {
			["basename"] = "models/player/items/scout/flipflops.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Lucky No. 42",
		localized_name = "info.tf2pm.hat.scout_towels",
		localized_description = "info.tf2pm.hat.scout_towels_desc",
		icon = "backpack/player/items/scout/summer_pack",
		models = {
			["basename"] = "models/player/items/scout/summer_pack.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The El Jefe",
		localized_name = "info.tf2pm.hat.tropico4_hat",
		icon = "backpack/player/items/scout/rebel_cap",
		models = {
			["basename"] = "models/player/items/scout/rebel_cap.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Ball-Kicking Boots",
		localized_name = "info.tf2pm.hat.football_boots",
		localized_description = "info.tf2pm.hat.football_boots_desc",
		icon = "backpack/workshop_partner/player/items/scout/fm2012_cleats/fm2012_cleats",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/fm2012_cleats/fm2012_cleats.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "Wrap Battler",
		localized_name = "info.tf2pm.hat.hwn_scouthat",
		icon = "backpack/player/items/scout/hwn_scout_hat",
		models = {
			["basename"] = "models/player/items/scout/hwn_scout_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"necklace", "sleeves"},
		paintable = false,
		name = "B-ankh!",
		localized_name = "info.tf2pm.hat.hwn_scoutmisc1",
		icon = "backpack/player/items/scout/hwn_scout_misc1",
		models = {
			["basename"] = "models/player/items/scout/hwn_scout_misc1.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "Futankhamun",
		localized_name = "info.tf2pm.hat.hwn_scoutmisc2",
		icon = "backpack/player/items/scout/hwn_scout_misc2",
		models = {
			["basename"] = "models/player/items/scout/hwn_scout_misc2.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Backwards Ballcap",
		localized_name = "info.tf2pm.hat.scouthat2",
		localized_description = "info.tf2pm.hat.scouthat2_desc",
		icon = "backpack/player/items/scout/fwk_scout_cap",
		models = {
			["basename"] = "models/player/items/scout/fwk_scout_cap.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scoutbackwardscap_style0",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scoutbackwardscap_style1",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "The Stereoscopic Shades",
		localized_name = "info.tf2pm.hat.scoutglasses",
		localized_description = "info.tf2pm.hat.scoutglasses_desc",
		icon = "backpack/player/items/scout/fwk_scout_3d",
		models = {
			["basename"] = "models/player/items/scout/fwk_scout_3d.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Hermes",
		localized_name = "info.tf2pm.hat.scouthat3",
		localized_description = "info.tf2pm.hat.scouthat3_desc",
		icon = "backpack/player/items/scout/fwk_scout_provision",
		models = {
			["basename"] = "models/player/items/scout/fwk_scout_provision.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Big Elfin Deal",
		localized_name = "info.tf2pm.hat.theelf",
		localized_description = "info.tf2pm.hat.theelf_desc",
		icon = "backpack/workshop/player/items/scout/xms_scout_elf_hat/xms_scout_elf_hat",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms_scout_elf_hat/xms_scout_elf_hat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.theelf_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/xms_scout_elf_hat/xms_scout_elf_hat.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.theelf_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/xms_scout_elf_hat_style1/xms_scout_elf_hat_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "The Bootie Time",
		localized_name = "info.tf2pm.hat.scout_elf_boots",
		localized_description = "info.tf2pm.hat.scout_elf_boots_desc",
		icon = "backpack/workshop/player/items/scout/xms_scout_elf_sneakers/xms_scout_elf_sneakers",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms_scout_elf_sneakers/xms_scout_elf_sneakers.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scout_elf_boots_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/xms_scout_elf_sneakers/xms_scout_elf_sneakers.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scout_elf_boots_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/xms_scout_elf_sneakers_style1/xms_scout_elf_sneakers_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = false,
		name = "The Boston Boom-Bringer",
		localized_name = "info.tf2pm.hat.scoutboombox",
		localized_description = "info.tf2pm.hat.scoutboombox_desc",
		icon = "backpack/player/items/scout/boombox",
		models = {
			["basename"] = "models/player/items/scout/boombox.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt", "arm_tatoos"},
		paintable = true,
		name = "The Fast Learner",
		localized_name = "info.tf2pm.hat.scoutprepshirt",
		localized_description = "info.tf2pm.hat.scoutprepshirt_desc",
		icon = "backpack/player/items/scout/scout_prep_shirt",
		models = {
			["basename"] = "models/player/items/scout/scout_prep_shirt.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.scoutprepshirt_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.scoutprepshirt_style1",
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Front Runner",
		localized_name = "info.tf2pm.hat.scoutheadband",
		localized_description = "info.tf2pm.hat.scoutheadband_desc",
		icon = "backpack/player/items/scout/scout_headband",
		models = {
			["basename"] = "models/player/items/scout/scout_headband.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scoutheadband_style0",
				models = {
					["basename"] = "models/player/items/scout/scout_headband.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.scoutheadband_style1",
				models = {
					["basename"] = "models/player/items/scout/scout_headband_s01.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Cross-Comm Express",
		localized_name = "info.tf2pm.hat.grfs_2",
		localized_description = "info.tf2pm.hat.grfs_2_desc",
		icon = "backpack/player/items/scout/grfs_scout",
		models = {
			["basename"] = "models/player/items/scout/grfs_scout.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Fed-Fightin' Fedora",
		localized_name = "info.tf2pm.hat.pep_scouthat",
		localized_description = "info.tf2pm.hat.pep_scouthat_desc",
		icon = "backpack/player/items/scout/pep_hat",
		models = {
			["basename"] = "models/player/items/scout/pep_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Dillinger's Duffel",
		localized_name = "info.tf2pm.hat.pep_scoutbag",
		localized_description = "info.tf2pm.hat.pep_scoutbag_desc",
		icon = "backpack/player/items/scout/pep_bag",
		models = {
			["basename"] = "models/player/items/scout/pep_bag.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Void Monk Hair",
		localized_name = "info.tf2pm.hat.ha_scout",
		localized_description = "info.tf2pm.hat.ha_scout_desc",
		icon = "backpack/workshop_partner/player/items/scout/hero_academy_scout/hero_academy_scout",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/hero_academy_scout/hero_academy_scout.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt", "arm_tatoos"},
		paintable = true,
		name = "The Track Terrorizer",
		localized_name = "info.tf2pm.hat.scouttrackjacket",
		localized_description = "info.tf2pm.hat.scouttrackjacket_desc",
		icon = "backpack/player/items/scout/scout_trackjacket",
		models = {
			["basename"] = "models/player/items/scout/scout_trackjacket.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Robot Running Man",
		localized_name = "info.tf2pm.hat.scout_robot_cap",
		localized_description = "info.tf2pm.hat.scout_robot_cap_desc",
		icon = "backpack/player/items/mvm_loot/scout/robo_cap",
		models = {
			["basename"] = "models/player/items/mvm_loot/scout/robo_cap.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "Flunkyware",
		localized_name = "info.tf2pm.hat.henchboy_belt",
		localized_description = "info.tf2pm.hat.henchboy_belt_desc",
		icon = "backpack/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_belt",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_belt.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Hanger-On Hood",
		localized_name = "info.tf2pm.hat.henchboy_hat",
		localized_description = "info.tf2pm.hat.henchboy_hat_desc",
		icon = "backpack/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_hat",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_hat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.henchboy_hat_style0",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.henchboy_hat_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_wings"},
		paintable = false,
		name = "The Flight of the Monarch",
		localized_name = "info.tf2pm.hat.henchboy_wings",
		localized_description = "info.tf2pm.hat.henchboy_wings_desc",
		icon = "backpack/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_wings",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/scout_henchboy/scout_henchboy_wings.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "The Spooky Shoes",
		localized_name = "info.tf2pm.hat.spookyshoes",
		localized_description = "info.tf2pm.hat.spookyshoes_desc",
		icon = "backpack/workshop/player/items/scout/scout_halloweenshoes/scout_halloweenshoes",
		models = {
			["basename"] = "models/workshop/player/items/scout/scout_halloweenshoes/scout_halloweenshoes.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_hands"},
		paintable = true,
		name = "The Digit Divulger",
		localized_name = "info.tf2pm.hat.thedigitdivulger",
		localized_description = "info.tf2pm.hat.thedigitdivulger_desc",
		icon = "backpack/workshop/player/items/scout/scout_gloves_leather_open/scout_gloves_leather_open",
		models = {
			["basename"] = "models/workshop/player/items/scout/scout_gloves_leather_open/scout_gloves_leather_open.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.thedigitdivulger_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/scout_gloves_leather_open/scout_gloves_leather_open.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.thedigitdivulger_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/scout_gloves_suede_open/scout_gloves_suede_open.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.thedigitdivulger_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/scout_gloves_leather_closed/scout_gloves_leather_closed.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.thedigitdivulger_style3",
				models = {
					["basename"] = "models/workshop/player/items/scout/scout_gloves_suede_closed/scout_gloves_suede_closed.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_bandages"},
		paintable = false,
		name = "The Tomb Wrapper",
		localized_name = "info.tf2pm.hat.tombwrapper",
		localized_description = "info.tf2pm.hat.tombwrapper_desc",
		icon = "backpack/player/items/scout/tomb_tourniquet",
		models = {
			["basename"] = "models/player/items/scout/tomb_tourniquet.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "The Long Fall Loafers",
		localized_name = "info.tf2pm.hat.longfallloafers",
		localized_description = "info.tf2pm.hat.longfallloafers_desc",
		icon = "backpack/player/items/scout/pn2_longfall",
		models = {
			["basename"] = "models/player/items/scout/pn2_longfall.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Bacteria Blocker",
		localized_name = "info.tf2pm.hat.bacteriablocker",
		localized_description = "info.tf2pm.hat.bacteriablocker_desc",
		icon = "backpack/workshop_partner/player/items/scout/as_scout_cleansuit_hood/as_scout_cleansuit_hood",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/as_scout_cleansuit_hood/as_scout_cleansuit_hood.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.bacteriablocker_style0",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.bacteriablocker_style1",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Scout",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_scout_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_scout_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_scout_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = false,
		name = "The Sack Fulla Smissmas",
		localized_name = "info.tf2pm.hat.winter2013_smissmassack",
		localized_description = "info.tf2pm.hat.winter2013_smissmassack_desc",
		icon = "backpack/player/items/scout/xms_santa_sack",
		models = {
			["basename"] = "models/player/items/scout/xms_santa_sack.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Scout Shako",
		localized_name = "info.tf2pm.hat.xms2013_scout_drummer_hat",
		icon = "backpack/workshop/player/items/scout/xms2013_scout_drummer_hat/xms2013_scout_drummer_hat",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms2013_scout_drummer_hat/xms2013_scout_drummer_hat.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Tools of the Tourist",
		localized_name = "info.tf2pm.hat.sum20_tools_tourist",
		icon = "backpack/workshop/player/items/scout/sum20_tools_tourist/sum20_tools_tourist",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum20_tools_tourist/sum20_tools_tourist.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Flapjack",
		localized_name = "info.tf2pm.hat.fall2013_neo_tokyo_runner",
		icon = "backpack/workshop/player/items/scout/fall2013_neo_tokyo_runner/fall2013_neo_tokyo_runner",
		models = {
			["basename"] = "models/workshop/player/items/scout/fall2013_neo_tokyo_runner/fall2013_neo_tokyo_runner.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Little Drummer Mann",
		localized_name = "info.tf2pm.hat.xms2013_scout_drummer_shirt",
		icon = "backpack/workshop/player/items/scout/xms2013_scout_drummer_shirt/xms2013_scout_drummer_shirt",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms2013_scout_drummer_shirt/xms2013_scout_drummer_shirt.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Poolside Polo",
		localized_name = "info.tf2pm.hat.sum20_poolside_polo",
		icon = "backpack/workshop/player/items/scout/sum20_poolside_polo/sum20_poolside_polo",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum20_poolside_polo/sum20_poolside_polo.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = false,
		name = "The Pompous Privateer",
		localized_name = "info.tf2pm.hat.sum19_pompous_privateer",
		icon = "backpack/workshop/player/items/scout/sum19_pompous_privateer/sum19_pompous_privateer",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum19_pompous_privateer/sum19_pompous_privateer.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "California Cap",
		localized_name = "info.tf2pm.hat.sum20_california_cap",
		icon = "backpack/workshop/player/items/scout/sum20_california_cap/sum20_california_cap",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum20_california_cap/sum20_california_cap.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Bottle Cap",
		localized_name = "info.tf2pm.hat.sum19_bottle_cap",
		icon = "backpack/workshop/player/items/scout/sum19_bottle_cap/sum19_bottle_cap",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum19_bottle_cap/sum19_bottle_cap.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_bottle_cap_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum19_bottle_cap/sum19_bottle_cap.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 0},
				localized_name = "info.tf2pm.hat.sum19_bottle_cap_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum19_bottle_cap_style1/sum19_bottle_cap_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"pants"},
		paintable = false,
		name = "B'aaarrgh-n-Britches",
		localized_name = "info.tf2pm.hat.hwn2015_bargain_britches",
		icon = "backpack/workshop/player/items/scout/hwn2015_bargain_britches/hwn2015_bargain_britches",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2015_bargain_britches/hwn2015_bargain_britches.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "B'aaarrgh-n-Bicorne",
		localized_name = "info.tf2pm.hat.hwn2015_bargain_bicorne",
		icon = "backpack/workshop/player/items/scout/hwn2015_bargain_bicorne/hwn2015_bargain_bicorne",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2015_bargain_bicorne/hwn2015_bargain_bicorne.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Chucklenuts",
		localized_name = "info.tf2pm.hat.xms2013_scout_squirrel",
		icon = "backpack/workshop/player/items/scout/xms2013_scout_squirrel/xms2013_scout_squirrel",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms2013_scout_squirrel/xms2013_scout_squirrel.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "Squire's Sabatons",
		localized_name = "info.tf2pm.hat.may16_squires_sabatons",
		icon = "backpack/workshop/player/items/scout/medieval_glory_boots/medieval_glory_boots",
		models = {
			["basename"] = "models/workshop/player/items/scout/medieval_glory_boots/medieval_glory_boots.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = false,
		name = "Courtly Cuirass",
		localized_name = "info.tf2pm.hat.may16_courtly_cuirass",
		icon = "backpack/workshop/player/items/scout/medieval_glory_armor/medieval_glory_armor",
		models = {
			["basename"] = "models/workshop/player/items/scout/medieval_glory_armor/medieval_glory_armor.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Halloweiner",
		localized_name = "info.tf2pm.hat.hw2013_halloweiner",
		icon = "backpack/workshop/player/items/scout/hw2013_halloweiner/hw2013_halloweiner",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_halloweiner/hw2013_halloweiner.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "Fortunate Son",
		localized_name = "info.tf2pm.hat.cc_summer2015_fortunate_son",
		icon = "backpack/workshop/player/items/scout/cc_summer2015_fortunate_son/cc_summer2015_fortunate_son",
		models = {
			["basename"] = "models/workshop/player/items/scout/cc_summer2015_fortunate_son/cc_summer2015_fortunate_son.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Faun Feet",
		localized_name = "info.tf2pm.hat.hw2013_the_hell_runner",
		icon = "backpack/workshop/player/items/scout/hw2013_the_hell_runner/hw2013_the_hell_runner",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_the_hell_runner/hw2013_the_hell_runner.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "dec2014 Fools Footwear",
		localized_name = "info.tf2pm.hat.dec2014_fools_footwear",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/scout/dec2014_fools_footwear/dec2014_fools_footwear",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec2014_fools_footwear/dec2014_fools_footwear.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Ticket Boy",
		localized_name = "info.tf2pm.hat.sbox2014_ticket_boy",
		icon = "backpack/workshop/player/items/scout/sbox2014_ticket_boy/sbox2014_ticket_boy",
		models = {
			["basename"] = "models/workshop/player/items/scout/sbox2014_ticket_boy/sbox2014_ticket_boy.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_pants"},
		paintable = false,
		name = "The Corpse Carrier",
		localized_name = "info.tf2pm.hat.hwn2021_corpse_carrier",
		icon = "backpack/workshop/player/items/scout/hwn2021_corpse_carrier/hwn2021_corpse_carrier",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2021_corpse_carrier/hwn2021_corpse_carrier.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "dec2014 Marauders Mask",
		localized_name = "info.tf2pm.hat.dec2014_marauders_mask",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/scout/dec2014_marauders_mask/dec2014_marauders_mask",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec2014_marauders_mask/dec2014_marauders_mask.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {},
		paintable = false,
		name = "Orion's Belt",
		localized_name = "info.tf2pm.hat.tr_orions_belt",
		icon = "backpack/workshop_partner/player/items/scout/tr_orions_belt/tr_orions_belt",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/tr_orions_belt/tr_orions_belt.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = false,
		name = "Flak Jack",
		localized_name = "info.tf2pm.hat.cc_summer2015_flak_jack",
		icon = "backpack/workshop/player/items/scout/cc_summer2015_flak_jack/cc_summer2015_flak_jack",
		models = {
			["basename"] = "models/workshop/player/items/scout/cc_summer2015_flak_jack/cc_summer2015_flak_jack.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "The Bootenkhamuns",
		localized_name = "info.tf2pm.hat.tr_bootenkhamuns",
		icon = "backpack/workshop_partner/player/items/scout/tr_bootenkhamuns/tr_bootenkhamuns",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/tr_bootenkhamuns/tr_bootenkhamuns.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Pomade Prince",
		localized_name = "info.tf2pm.hat.scoutfancyhair",
		icon = "backpack/player/items/scout/scout_pompadour",
		models = {
			["basename"] = "models/player/items/scout/scout_pompadour.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Batter's Beak",
		localized_name = "info.tf2pm.hat.hwn2021_batters_beak",
		icon = "backpack/workshop/player/items/scout/hwn2021_batters_beak/hwn2021_batters_beak",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2021_batters_beak/hwn2021_batters_beak.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_pants"},
		paintable = true,
		name = "Transparent Trousers",
		localized_name = "info.tf2pm.hat.fall17_transparent_trousers",
		localized_description = "info.tf2pm.hat.fall17_transparent_trousers_desc",
		icon = "backpack/workshop/player/items/scout/fall17_transparent_trousers/fall17_transparent_trousers",
		models = {
			["basename"] = "models/workshop/player/items/scout/fall17_transparent_trousers/fall17_transparent_trousers.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt", "arm_tatoos"},
		paintable = false,
		name = "The Paisley Pro",
		localized_name = "info.tf2pm.hat.scoutfancyshirt",
		icon = "backpack/player/items/scout/scout_fancy_shirt",
		models = {
			["basename"] = "models/player/items/scout/scout_fancy_shirt.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"whole_head", "necklace"},
		paintable = true,
		name = "Nugget Noggin",
		localized_name = "info.tf2pm.hat.sf14_nugget_noggin",
		icon = "backpack/workshop/player/items/scout/sf14_nugget_noggin/sf14_nugget_noggin",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_nugget_noggin/sf14_nugget_noggin.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "The Bolt Boy",
		localized_name = "info.tf2pm.hat.robo_scout_bolt_boy",
		localized_description = "info.tf2pm.hat.robo_scout_bolt_boy_desc",
		icon = "backpack/workshop/player/items/scout/robo_scout_bolt_boy/robo_scout_bolt_boy",
		models = {
			["basename"] = "models/workshop/player/items/scout/robo_scout_bolt_boy/robo_scout_bolt_boy.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.robo_scout_bolt_boy_style0",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.robo_scout_bolt_boy_style1",
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Snowwing",
		localized_name = "info.tf2pm.hat.dec16_snowwing",
		localized_description = "info.tf2pm.hat.dec16_snowwing_desc",
		icon = "backpack/workshop/player/items/scout/dec16_snowwing/dec16_snowwing",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec16_snowwing/dec16_snowwing.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Remorseless Raptor",
		localized_name = "info.tf2pm.hat.hwn2019_remorseless_raptor",
		icon = "backpack/workshop/player/items/scout/hwn2019_remorseless_raptor/hwn2019_remorseless_raptor",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2019_remorseless_raptor/hwn2019_remorseless_raptor.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"face"},
		paintable = true,
		name = "The Face Plante",
		localized_name = "info.tf2pm.hat.hw2013_boston_bandy_mask",
		icon = "backpack/workshop/player/items/scout/hw2013_boston_bandy_mask/hw2013_boston_bandy_mask",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_boston_bandy_mask/hw2013_boston_bandy_mask.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {},
		paintable = true,
		name = "Wild Whip",
		localized_name = "info.tf2pm.hat.hwn2019_wild_whip",
		icon = "backpack/workshop/player/items/scout/hwn2019_wild_whip/hwn2019_wild_whip",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2019_wild_whip/hwn2019_wild_whip.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_hands"},
		paintable = false,
		name = "Claws And Infect",
		localized_name = "info.tf2pm.hat.sf14_scout_hunter_arm",
		icon = "backpack/workshop/player/items/scout/sf14_scout_hunter_arm/sf14_scout_hunter_arm",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_scout_hunter_arm/sf14_scout_hunter_arm.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_pants", "feet"},
		paintable = true,
		name = "The Terrier Trousers",
		localized_name = "info.tf2pm.hat.hw2013_terrifying_terrier_trousers",
		icon = "backpack/workshop/player/items/scout/hw2013_terrifying_terrier_trousers/hw2013_terrifying_terrier_trousers",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_terrifying_terrier_trousers/hw2013_terrifying_terrier_trousers.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Millennial Mercenary",
		localized_name = "info.tf2pm.hat.dec18_millennial_mercenary",
		icon = "backpack/workshop/player/items/scout/dec18_millennial_mercenary/dec18_millennial_mercenary",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec18_millennial_mercenary/dec18_millennial_mercenary.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec18_millennial_mercenary_style0",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_millennial_mercenary/dec18_millennial_mercenary.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec18_millennial_mercenary_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_millennial_mercenary_style1/dec18_millennial_mercenary_style1.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.dec18_millennial_mercenary_style2",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_millennial_mercenary_style2/dec18_millennial_mercenary_style2.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.dec18_millennial_mercenary_style3",
				bodygroup_overrides = {
					["headphones"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_millennial_mercenary_style3/dec18_millennial_mercenary_style3.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"necklace", "hat"},
		paintable = true,
		name = "Head Hunter",
		localized_name = "info.tf2pm.hat.sf14_scout_hunter_head",
		icon = "backpack/workshop/player/items/scout/sf14_scout_hunter_head/sf14_scout_hunter_head",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_scout_hunter_head/sf14_scout_hunter_head.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_pants", "feet"},
		paintable = false,
		name = "Crazy Legs",
		localized_name = "info.tf2pm.hat.sf14_scout_hunter_legs",
		icon = "backpack/workshop/player/items/scout/sf14_scout_hunter_legs/sf14_scout_hunter_legs",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_scout_hunter_legs/sf14_scout_hunter_legs.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "The Electric Twanger",
		localized_name = "info.tf2pm.hat.dec16_electric_twanger",
		localized_description = "info.tf2pm.hat.dec16_electric_twanger_desc",
		icon = "backpack/workshop/player/items/scout/dec16_electric_twanger/dec16_electric_twanger",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec16_electric_twanger/dec16_electric_twanger.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Juvenile's Jumper",
		localized_name = "info.tf2pm.hat.dec18_juveniles_jumper",
		icon = "backpack/workshop/player/items/scout/dec18_juveniles_jumper/dec18_juveniles_jumper",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec18_juveniles_jumper/dec18_juveniles_jumper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_juveniles_jumper_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_juveniles_jumper/dec18_juveniles_jumper.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_juveniles_jumper_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_juveniles_jumper_modern/dec18_juveniles_jumper_modern.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_juveniles_jumper_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_juveniles_jumper_plain/dec18_juveniles_jumper_plain.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "Talon Trotters",
		localized_name = "info.tf2pm.hat.sf14_talon_trotters",
		icon = "backpack/workshop/player/items/scout/sf14_talon_trotters/sf14_talon_trotters",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_talon_trotters/sf14_talon_trotters.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "tw_scoutbot_hat",
		localized_name = "info.tf2pm.hat.tw_scoutbot_hat",
		icon = "backpack/workshop/player/items/scout/tw_scoutbot_hat/tw_scoutbot_hat",
		models = {
			["basename"] = "models/workshop/player/items/scout/tw_scoutbot_hat/tw_scoutbot_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Bonk Leadwear",
		localized_name = "info.tf2pm.hat.robo_scout_bonk_helm",
		localized_description = "info.tf2pm.hat.robo_scout_bonk_helm_desc",
		icon = "backpack/workshop/player/items/scout/robo_scout_bonk_helm/robo_scout_bonk_helm",
		models = {
			["basename"] = "models/workshop/player/items/scout/robo_scout_bonk_helm/robo_scout_bonk_helm.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"right_shoulder"},
		paintable = true,
		name = "The Catcher's Companion",
		localized_name = "info.tf2pm.hat.dec18_catchers_companion",
		icon = "backpack/workshop/player/items/scout/dec18_catchers_companion/dec18_catchers_companion",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec18_catchers_companion/dec18_catchers_companion.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_catchers_companion_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_catchers_companion/dec18_catchers_companion.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec18_catchers_companion_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec18_catchers_companion_style/dec18_catchers_companion_style.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_scoutbot_armor",
		localized_name = "info.tf2pm.hat.tw_scoutbot_armor",
		icon = "backpack/workshop/player/items/scout/tw_scoutbot_armor/tw_scoutbot_armor",
		models = {
			["basename"] = "models/workshop/player/items/scout/tw_scoutbot_armor/tw_scoutbot_armor.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Jungle Jersey",
		localized_name = "info.tf2pm.hat.fall17_jungle_jersey",
		localized_description = "info.tf2pm.hat.fall17_jungle_jersey_desc",
		icon = "backpack/workshop/player/items/scout/fall17_jungle_jersey/fall17_jungle_jersey",
		models = {
			["basename"] = "models/workshop/player/items/scout/fall17_jungle_jersey/fall17_jungle_jersey.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Ye Oiled Baker Boy",
		localized_name = "info.tf2pm.hat.robo_scout_baker_boy",
		localized_description = "info.tf2pm.hat.robo_scout_baker_boy_desc",
		icon = "backpack/workshop/player/items/scout/robo_scout_baker_boy/robo_scout_baker_boy",
		models = {
			["basename"] = "models/workshop/player/items/scout/robo_scout_baker_boy/robo_scout_baker_boy.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "The Half-Pipe Hurdler",
		localized_name = "info.tf2pm.hat.jul13_skater_boy",
		localized_description = "info.tf2pm.hat.jul13_skater_boy_desc",
		icon = "backpack/workshop/player/items/scout/jul13_skater_boy/jul13_skater_boy",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_skater_boy/jul13_skater_boy.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Death Racer's Helmet",
		localized_name = "info.tf2pm.hat.hwn2015_death_racer_helmet",
		icon = "backpack/workshop/player/items/scout/hwn2015_death_racer_helmet/hwn2015_death_racer_helmet",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2015_death_racer_helmet/hwn2015_death_racer_helmet.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "The Caffeine Cooler",
		localized_name = "info.tf2pm.hat.jul13_koolboy",
		localized_description = "info.tf2pm.hat.jul13_koolboy_desc",
		icon = "backpack/workshop/player/items/scout/jul13_koolboy/jul13_koolboy",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_koolboy/jul13_koolboy.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.jul13_koolboy_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/jul13_koolboy/jul13_koolboy.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.jul13_koolboy_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/jul13_koolboy_2/jul13_koolboy_2.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.jul13_koolboy_style3",
				models = {
					["basename"] = "models/workshop/player/items/scout/jul13_koolboy_3/jul13_koolboy_3.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = false,
		name = "Thrilling Tracksuit",
		localized_name = "info.tf2pm.hat.hwn2015_death_racer_jacket",
		icon = "backpack/workshop/player/items/scout/hwn2015_death_racer_jacket/hwn2015_death_racer_jacket",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2015_death_racer_jacket/hwn2015_death_racer_jacket.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Wing Mann",
		localized_name = "info.tf2pm.hat.hwn2016_wing_mann",
		icon = "backpack/workshop/player/items/scout/hwn2016_wing_mann/hwn2016_wing_mann",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2016_wing_mann/hwn2016_wing_mann.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Speedster's Spandex",
		localized_name = "info.tf2pm.hat.spr17_speedsters_spandex",
		localized_description = "info.tf2pm.hat.spr17_speedsters_spandex_desc",
		icon = "backpack/workshop/player/items/scout/spr17_speedsters_spandex/spr17_speedsters_spandex",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr17_speedsters_spandex/spr17_speedsters_spandex.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Bonk Batter's Backup",
		localized_name = "info.tf2pm.hat.dec15_scout_baseball_bag",
		icon = "backpack/workshop/player/items/scout/dec15_scout_baseball_bag/dec15_scout_baseball_bag",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec15_scout_baseball_bag/dec15_scout_baseball_bag.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_thermal_sleeves",
		localized_name = "info.tf2pm.hat.eotl_thermal_sleeves",
		icon = "backpack/workshop/player/items/scout/thermal_sleeves/thermal_sleeves",
		models = {
			["basename"] = "models/workshop/player/items/scout/thermal_sleeves/thermal_sleeves.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Horrific Head of Hare",
		localized_name = "info.tf2pm.hat.hw2013_bunny_mann",
		icon = "backpack/workshop/player/items/scout/hw2013_bunny_mann/hw2013_bunny_mann",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_bunny_mann/hw2013_bunny_mann.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["whole_head"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "EOTL_blinks_breeches",
		localized_name = "info.tf2pm.hat.eotl_blinks_breeches",
		icon = "backpack/workshop/player/items/scout/blinks_breeches/blinks_breeches",
		models = {
			["basename"] = "models/workshop/player/items/scout/blinks_breeches/blinks_breeches.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"sleeves"},
		paintable = true,
		name = "Fowl Fists",
		localized_name = "info.tf2pm.hat.sf14_fowl_fists",
		icon = "backpack/workshop/player/items/scout/sf14_fowl_fists/sf14_fowl_fists",
		models = {
			["basename"] = "models/workshop/player/items/scout/sf14_fowl_fists/sf14_fowl_fists.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "The Sprinting Cephalopod",
		localized_name = "info.tf2pm.hat.hw2013_running_octopus",
		icon = "backpack/workshop/player/items/scout/hw2013_running_octopus/hw2013_running_octopus",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_running_octopus/hw2013_running_octopus.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Bigg Mann on Campus",
		localized_name = "info.tf2pm.hat.jul13_scout_varsity",
		localized_description = "info.tf2pm.hat.jul13_scout_varsity_desc",
		icon = "backpack/workshop/player/items/scout/jul13_scout_varsity/jul13_scout_varsity",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_scout_varsity/jul13_scout_varsity.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Athenian Attire",
		localized_name = "info.tf2pm.hat.hwn2018_athenian_attire",
		icon = "backpack/workshop/player/items/scout/hwn2018_athenian_attire/hwn2018_athenian_attire",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2018_athenian_attire/hwn2018_athenian_attire.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Thirst Blood",
		localized_name = "info.tf2pm.hat.sept2014_thirst_blood",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/scout/sept2014_thirst_blood/sept2014_thirst_blood",
		models = {
			["basename"] = "models/workshop/player/items/scout/sept2014_thirst_blood/sept2014_thirst_blood.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Lightning Lid",
		localized_name = "info.tf2pm.hat.spr17_the_lightning_lid",
		localized_description = "info.tf2pm.hat.spr17_the_lightning_lid_desc",
		icon = "backpack/workshop/player/items/scout/spr17_the_lightning_lid/spr17_the_lightning_lid",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr17_the_lightning_lid/spr17_the_lightning_lid.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Meal Dealer",
		localized_name = "info.tf2pm.hat.sum21_meal_dealer",
		icon = "backpack/workshop/player/items/scout/sum21_meal_dealer/sum21_meal_dealer",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum21_meal_dealer/sum21_meal_dealer.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = true,
		name = "Fast Food",
		localized_name = "info.tf2pm.hat.sum21_fast_food",
		icon = "backpack/workshop/player/items/scout/sum21_fast_food/sum21_fast_food",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum21_fast_food/sum21_fast_food.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Greased Lightning",
		localized_name = "info.tf2pm.hat.jul13_greased_lightning",
		localized_description = "info.tf2pm.hat.jul13_greased_lightning_desc",
		icon = "backpack/workshop/player/items/scout/jul13_greased_lightning/jul13_greased_lightning",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_greased_lightning/jul13_greased_lightning.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.jul13_greased_lightning_style0",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.jul13_greased_lightning_style1",
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Cool Cat Cardigan",
		localized_name = "info.tf2pm.hat.jul13_the_cunningmann",
		localized_description = "info.tf2pm.hat.jul13_the_cunningmann_desc",
		icon = "backpack/workshop/player/items/scout/jul13_the_cunningmann/jul13_the_cunningmann",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_the_cunningmann/jul13_the_cunningmann.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Seasonal Employee",
		localized_name = "info.tf2pm.hat.dec21_seasonal_employee",
		icon = "backpack/workshop/player/items/scout/dec21_seasonal_employee/dec21_seasonal_employee",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec21_seasonal_employee/dec21_seasonal_employee.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec21_seasonal_employee_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec21_seasonal_employee/dec21_seasonal_employee.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.dec21_seasonal_employee_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/dec21_seasonal_employee_style1/dec21_seasonal_employee_style1.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet", "zombie_body"},
		paintable = false,
		name = "Zombie Scout",
		localized_name = "info.tf2pm.hat.item_zombiescout",
		icon = "backpack/player/items/scout/scout_zombie",
		models = {
			["basename"] = "models/player/items/scout/scout_zombie.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Punk's Pomp",
		localized_name = "info.tf2pm.hat.spr18_punks_pomp",
		icon = "backpack/workshop/player/items/scout/spr18_punks_pomp/spr18_punks_pomp",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr18_punks_pomp/spr18_punks_pomp.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = false,
		name = "Messenger's Mail Bag",
		localized_name = "info.tf2pm.hat.spr17_messengers_mail_bag",
		localized_description = "info.tf2pm.hat.spr17_messengers_mail_bag_desc",
		icon = "backpack/workshop/player/items/scout/spr17_messengers_mail_bag/spr17_messengers_mail_bag",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr17_messengers_mail_bag/spr17_messengers_mail_bag.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "The Xeno Suit",
		localized_name = "info.tf2pm.hat.xenosuit",
		icon = "backpack/workshop_partner/player/items/scout/ai_legs/ai_legs",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/ai_legs/ai_legs.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Runner's Warm-Up",
		localized_name = "info.tf2pm.hat.xms2013_scout_skicap",
		icon = "backpack/workshop/player/items/scout/xms2013_scout_skicap/xms2013_scout_skicap",
		models = {
			["basename"] = "models/workshop/player/items/scout/xms2013_scout_skicap/xms2013_scout_skicap.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Airdog",
		localized_name = "info.tf2pm.hat.may16_airdog",
		icon = "backpack/workshop/player/items/scout/snowboard_hat/snowboard_hat",
		models = {
			["basename"] = "models/workshop/player/items/scout/snowboard_hat/snowboard_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Bat Backup",
		localized_name = "info.tf2pm.hat.bak_bat_backup",
		icon = "backpack/workshop/player/items/scout/bak_bat_backup/bak_bat_backup",
		models = {
			["basename"] = "models/workshop/player/items/scout/bak_bat_backup/bak_bat_backup.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Hound's Hood",
		localized_name = "info.tf2pm.hat.hw2013_horrifying_hound_hood",
		icon = "backpack/workshop/player/items/scout/hw2013_horrifying_hound_hood/hw2013_horrifying_hound_hood",
		models = {
			["basename"] = "models/workshop/player/items/scout/hw2013_horrifying_hound_hood/hw2013_horrifying_hound_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["beard"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Hephaistos' Handcraft",
		localized_name = "info.tf2pm.hat.hwn2018_hephaistos_handcraft",
		icon = "backpack/workshop/player/items/scout/hwn2018_hephaistos_handcraft/hwn2018_hephaistos_handcraft",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2018_hephaistos_handcraft/hwn2018_hephaistos_handcraft.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = false,
		name = "The Alien Cranium",
		localized_name = "info.tf2pm.hat.aliencranium",
		icon = "backpack/workshop_partner/player/items/scout/ai_head/ai_head",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/ai_head/ai_head.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"pants"},
		paintable = true,
		name = "Blizzard Britches",
		localized_name = "info.tf2pm.hat.spr18_blizzard_britches",
		icon = "backpack/workshop/player/items/scout/spr18_blizzard_britches/spr18_blizzard_britches",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr18_blizzard_britches/spr18_blizzard_britches.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Olympic Leapers",
		localized_name = "info.tf2pm.hat.hwn2018_olympic_leapers",
		icon = "backpack/workshop/player/items/scout/hwn2018_olympic_leapers/hwn2018_olympic_leapers",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2018_olympic_leapers/hwn2018_olympic_leapers.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"back"},
		paintable = false,
		name = "The Biomech Backpack",
		localized_name = "info.tf2pm.hat.biomechbackpack",
		icon = "backpack/workshop_partner/player/items/scout/ai_body/ai_body",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/ai_body/ai_body.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Fried Batter",
		localized_name = "info.tf2pm.hat.sum21_fried_batter",
		icon = "backpack/workshop/player/items/scout/sum21_fried_batter/sum21_fried_batter",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum21_fried_batter/sum21_fried_batter.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Southie Shinobi",
		localized_name = "info.tf2pm.hat.short2014_ninja_vest",
		icon = "backpack/workshop/player/items/scout/short2014_minja_vest/short2014_minja_vest",
		models = {
			["basename"] = "models/workshop/player/items/scout/short2014_minja_vest/short2014_minja_vest.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "Wipe Out Wraps",
		localized_name = "info.tf2pm.hat.spr18_wipe_out_wraps",
		icon = "backpack/workshop/player/items/scout/spr18_wipe_out_wraps/spr18_wipe_out_wraps",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr18_wipe_out_wraps/spr18_wipe_out_wraps.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat", "head_skin", "beard"},
		paintable = true,
		name = "The Frickin' Sweet Ninja Hood",
		localized_name = "info.tf2pm.hat.short2014_scout_ninja_mask",
		icon = "backpack/workshop/player/items/scout/short2014_scout_ninja_mask/short2014_scout_ninja_mask",
		models = {
			["basename"] = "models/workshop/player/items/scout/short2014_scout_ninja_mask/short2014_scout_ninja_mask.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["dogtags"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"beard"},
		paintable = true,
		name = "Fuel Injector",
		localized_name = "info.tf2pm.hat.hwn2019_fuel_injector",
		icon = "backpack/workshop/player/items/scout/hwn2019_fuel_injector/hwn2019_fuel_injector",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2019_fuel_injector/hwn2019_fuel_injector.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.hwn2019_fuel_injector_style0",
				models = {
					["basename"] = "models/workshop/player/items/scout/hwn2019_fuel_injector/hwn2019_fuel_injector.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.hwn2019_fuel_injector_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/hwn2019_fuel_injector_style2/hwn2019_fuel_injector_style2.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.hwn2019_fuel_injector_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/hwn2019_fuel_injector_style3/hwn2019_fuel_injector_style3.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "The Red Socks",
		localized_name = "info.tf2pm.hat.short2014_ninja_boots",
		icon = "backpack/workshop/player/items/scout/short2014_ninja_boots/short2014_ninja_boots",
		models = {
			["basename"] = "models/workshop/player/items/scout/short2014_ninja_boots/short2014_ninja_boots.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Speedy Scoundrel",
		localized_name = "info.tf2pm.hat.sum19_speedy_scoundrel",
		icon = "backpack/workshop/player/items/scout/sum19_speedy_scoundrel/sum19_speedy_scoundrel",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum19_speedy_scoundrel/sum19_speedy_scoundrel.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_speedy_scoundrel_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum19_speedy_scoundrel/sum19_speedy_scoundrel.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_speedy_scoundrel_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum19_speedy_scoundrel_style2/sum19_speedy_scoundrel_style2.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.sum19_speedy_scoundrel_style3",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum19_lion_of_the_seas/sum19_lion_of_the_seas.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Hot Heels",
		localized_name = "info.tf2pm.hat.dec15_hot_heels",
		icon = "backpack/workshop/player/items/scout/dec15_hot_heels/dec15_hot_heels",
		models = {
			["basename"] = "models/workshop/player/items/%s/dec15_hot_heels/dec15_hot_heels.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_pants"},
		paintable = true,
		name = "Grounded Flyboy",
		localized_name = "info.tf2pm.hat.hwn2020_grounded_flyboy",
		icon = "backpack/workshop/player/items/scout/hwn2020_grounded_flyboy/hwn2020_grounded_flyboy",
		models = {
			["basename"] = "models/workshop/player/items/scout/hwn2020_grounded_flyboy/hwn2020_grounded_flyboy.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "The Cheet Sheet",
		localized_name = "info.tf2pm.hat.tw2_cheetah_robe",
		localized_description = "info.tf2pm.hat.tw2_cheetah_robe_desc",
		icon = "backpack/workshop_partner/player/items/scout/tw2_cheetah_robe/tw2_cheetah_robe",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/tw2_cheetah_robe/tw2_cheetah_robe.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "The Delinquent's Down Vest",
		localized_name = "info.tf2pm.hat.jul13_scout_vestjacket",
		localized_description = "info.tf2pm.hat.jul13_scout_vestjacket_desc",
		icon = "backpack/workshop/player/items/scout/jul13_scout_vestjacket/jul13_scout_vestjacket",
		models = {
			["basename"] = "models/workshop/player/items/scout/jul13_scout_vestjacket/jul13_scout_vestjacket.mdl",
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "The Beastly Bonnet",
		localized_name = "info.tf2pm.hat.tw2_cheetah_head",
		localized_description = "info.tf2pm.hat.tw2_cheetah_head_desc",
		icon = "backpack/workshop_partner/player/items/scout/tw2_cheetah_head/tw2_cheetah_head",
		models = {
			["basename"] = "models/workshop_partner/player/items/scout/tw2_cheetah_head/tw2_cheetah_head.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = false,
		name = "The Argyle Ace",
		localized_name = "info.tf2pm.hat.scoutfancyshoes",
		icon = "backpack/player/items/scout/scout_brogues",
		models = {
			["basename"] = "models/player/items/scout/scout_brogues.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"feet"},
		paintable = true,
		name = "Forest Footwear",
		localized_name = "info.tf2pm.hat.fall17_forest_footwear",
		localized_description = "info.tf2pm.hat.fall17_forest_footwear_desc",
		icon = "backpack/workshop/player/items/scout/fall17_forest_footwear/fall17_forest_footwear",
		models = {
			["basename"] = "models/workshop/player/items/scout/fall17_forest_footwear/fall17_forest_footwear.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "Soda Specs",
		localized_name = "info.tf2pm.hat.sum20_soda_specs",
		icon = "backpack/workshop/player/items/scout/sum20_soda_specs_style1/sum20_soda_specs_style1",
		models = {
			["basename"] = "models/workshop/player/items/scout/sum20_soda_specs_style1/sum20_soda_specs_style1.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_soda_specs_style1",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum20_soda_specs_style1/sum20_soda_specs_style1.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 0},
				localized_name = "info.tf2pm.hat.sum20_soda_specs_style2",
				models = {
					["basename"] = "models/workshop/player/items/scout/sum20_soda_specs_style2/sum20_soda_specs_style2.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "Sidekick's Side Slick",
		localized_name = "info.tf2pm.hat.bak_sidekicks_side_slick",
		icon = "backpack/workshop/player/items/scout/bak_sidekicks_side_slick/bak_sidekicks_side_slick",
		models = {
			["basename"] = "models/workshop/player/items/scout/bak_sidekicks_side_slick/bak_sidekicks_side_slick.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.bak_sidekicks_side_slick_style1_nohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/scout/bak_sidekicks_side_slick/bak_sidekicks_side_slick.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.bak_sidekicks_side_slick_style1_hat",
				models = {
					["basename"] = "models/workshop/player/items/scout/bak_sidekicks_side_slick/bak_sidekicks_side_slick.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.bak_sidekicks_side_slick_style2_nohat",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
				models = {
					["basename"] = "models/workshop/player/items/scout/bak_sidekicks_side_slick_s2/bak_sidekicks_side_slick_s2.mdl",
				}
			},
			[3] = {
				localized_name = "info.tf2pm.hat.bak_sidekicks_side_slick_style2_hat",
				models = {
					["basename"] = "models/workshop/player/items/scout/bak_sidekicks_side_slick_s2/bak_sidekicks_side_slick_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"scout"},
		targets = {"glasses"},
		paintable = true,
		name = "Cadet Visor",
		localized_name = "info.tf2pm.hat.invasion_cadet_visor",
		icon = "backpack/workshop/player/items/scout/invasion_cadet_visor/invasion_cadet_visor",
		models = {
			["basename"] = "models/workshop/player/items/scout/invasion_cadet_visor/invasion_cadet_visor.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"shirt"},
		paintable = true,
		name = "dec2014 Truands Tunic",
		localized_name = "info.tf2pm.hat.dec2014_truands_tunic",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/scout/dec2014_truands_tunic/dec2014_truands_tunic",
		models = {
			["basename"] = "models/workshop/player/items/scout/dec2014_truands_tunic/dec2014_truands_tunic.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"hat"},
		paintable = true,
		name = "Herald's Helm",
		localized_name = "info.tf2pm.hat.may16_heralds_helm",
		icon = "backpack/workshop/player/items/scout/knight_helmet/knight_helmet",
		models = {
			["basename"] = "models/workshop/player/items/scout/knight_helmet/knight_helmet.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"sleeves"},
		paintable = true,
		name = "Crook Combatant",
		localized_name = "info.tf2pm.hat.bak_crook_combatant",
		icon = "backpack/workshop/player/items/scout/bak_crook_combatant/bak_crook_combatant",
		models = {
			["basename"] = "models/workshop/player/items/scout/bak_crook_combatant/bak_crook_combatant.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"scout"},
		targets = {"scout_backpack"},
		paintable = false,
		name = "Pocket Pauling",
		localized_name = "info.tf2pm.hat.spr17_pocket_pauling",
		localized_description = "info.tf2pm.hat.spr17_pocket_pauling_desc",
		icon = "backpack/workshop/player/items/scout/spr17_pocket_pauling/spr17_pocket_pauling",
		models = {
			["basename"] = "models/workshop/player/items/scout/spr17_pocket_pauling/spr17_pocket_pauling.mdl",
		},
	},

}
