
-- Auto generated at 2022-02-26 20:22:44 UTC+07:00
return {
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Fancy Fedora",
		localized_name = "info.tf2pm.hat.spy_hat_1",
		localized_description = "info.tf2pm.hat.spy_hat_1_desc",
		icon = "backpack/player/items/spy/spy_hat",
		models = {
			["basename"] = "models/player/items/spy/spy_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"beard"},
		paintable = true,
		name = "Camera Beard",
		localized_name = "info.tf2pm.hat.spy_camera_beard",
		localized_description = "info.tf2pm.hat.spy_camera_beard_desc",
		icon = "backpack/player/items/spy/spy_camera_beard",
		models = {
			["basename"] = "models/player/items/spy/spy_camera_beard.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Backbiter's Billycock",
		localized_name = "info.tf2pm.hat.spy_derby_hat",
		localized_description = "info.tf2pm.hat.spy_derby_hat_desc",
		icon = "backpack/player/items/spy/derby_hat",
		models = {
			["basename"] = "models/player/items/spy/derby_hat.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Spy Noble Hair",
		localized_name = "info.tf2pm.hat.spynoblehair",
		localized_description = "info.tf2pm.hat.spynoblehair_desc",
		icon = "backpack/player/items/spy/noblehair",
		models = {
			["basename"] = "models/player/items/spy/noblehair.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Spy Beret",
		localized_name = "info.tf2pm.hat.spyberet",
		localized_description = "info.tf2pm.hat.spyberet_desc",
		icon = "backpack/player/items/spy/spy_beret",
		models = {
			["basename"] = "models/player/items/spy/spy_beret.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"glasses", "hat"},
		paintable = false,
		name = "The Familiar Fez",
		localized_name = "info.tf2pm.hat.thefamiliarfez",
		icon = "backpack/player/items/spy/fez",
		models = {
			["basename"] = "models/player/items/spy/fez.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Detective Noir",
		localized_name = "info.tf2pm.hat.detectivenoir",
		icon = "backpack/player/items/spy/spy_detective_noir",
		models = {
			["basename"] = "models/player/items/spy/spy_detective_noir.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"head_skin"},
		paintable = false,
		name = "Le Party Phantom",
		localized_name = "info.tf2pm.hat.partyphantom",
		icon = "backpack/player/items/spy/spy_party_phantom",
		models = {
			["basename"] = "models/player/items/spy/spy_party_phantom.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = false,
		name = "Spy Oni Mask",
		localized_name = "info.tf2pm.hat.spyonimask",
		localized_description = "info.tf2pm.hat.spyonimask_desc",
		icon = "backpack/workshop_partner/player/items/spy/shogun_ninjamask/shogun_ninjamask",
		models = {
			["basename"] = "models/workshop_partner/player/items/spy/shogun_ninjamask/shogun_ninjamask.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Charmer's Chapeau",
		localized_name = "info.tf2pm.hat.charmers_chapeau",
		localized_description = "info.tf2pm.hat.charmers_chapeau_desc",
		icon = "backpack/player/items/spy/spy_charmers_chapeau",
		models = {
			["basename"] = "models/player/items/spy/spy_charmers_chapeau.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Janissary Hat",
		localized_name = "info.tf2pm.hat.janissaryhat",
		localized_description = "info.tf2pm.hat.janissaryhat_desc",
		icon = "backpack/player/items/spy/mbsf_spy",
		models = {
			["basename"] = "models/player/items/spy/mbsf_spy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Cosa Nostra Cap",
		localized_name = "info.tf2pm.hat.cosanostracap",
		localized_description = "info.tf2pm.hat.cosanostracap_desc",
		icon = "backpack/player/items/spy/spy_gang_cap",
		models = {
			["basename"] = "models/player/items/spy/spy_gang_cap.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"medal"},
		paintable = true,
		name = "The Made Man",
		localized_name = "info.tf2pm.hat.mademan",
		localized_description = "info.tf2pm.hat.mademan_desc",
		icon = "backpack/player/items/spy/spy_rose",
		models = {
			["basename"] = "models/player/items/spy/spy_rose.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt", "sleeves"},
		paintable = true,
		name = "Rogue's Col Roule",
		localized_name = "info.tf2pm.hat.roguescolroule",
		icon = "backpack/player/items/crafting/tneck_backpack_icon",
		models = {
			["basename"] = "models/player/items/spy/tneck.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Belltower Spec Ops",
		localized_name = "info.tf2pm.hat.dex_helmet",
		icon = "backpack/workshop_partner/player/items/spy/dex_belltower/dex_belltower",
		models = {
			["basename"] = "models/workshop_partner/player/items/spy/dex_belltower/dex_belltower.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = false,
		name = "Under Cover",
		localized_name = "info.tf2pm.hat.hwn_spyhat",
		icon = "backpack/player/items/spy/hwn_spy_hat",
		models = {
			["basename"] = "models/player/items/spy/hwn_spy_hat.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"glasses"},
		paintable = false,
		name = "Griffin's Gog",
		localized_name = "info.tf2pm.hat.hwn_spymisc1",
		icon = "backpack/player/items/spy/hwn_spy_misc1",
		models = {
			["basename"] = "models/player/items/spy/hwn_spy_misc1.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = false,
		name = "Intangible Ascot",
		localized_name = "info.tf2pm.hat.hwn_spymisc2",
		icon = "backpack/player/items/spy/hwn_spy_misc2",
		models = {
			["basename"] = "models/player/items/spy/hwn_spy_misc2.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Counterfeit Billycock",
		localized_name = "info.tf2pm.hat.spyhat1",
		localized_description = "info.tf2pm.hat.spyhat1_desc",
		icon = "backpack/player/items/spy/fwk_spy_disguisedhat",
		models = {
			["basename"] = "models/player/items/spy/fwk_spy_disguisedhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "L'Inspecteur",
		localized_name = "info.tf2pm.hat.spyhat2",
		localized_description = "info.tf2pm.hat.spyhat2_desc",
		icon = "backpack/player/items/spy/fwk_spy_inspector",
		models = {
			["basename"] = "models/player/items/spy/fwk_spy_inspector.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"glasses"},
		paintable = true,
		name = "The Spectre's Spectacles",
		localized_name = "info.tf2pm.hat.spyglasses",
		localized_description = "info.tf2pm.hat.spyglasses_desc",
		icon = "backpack/player/items/spy/fwk_spy_specs",
		models = {
			["basename"] = "models/player/items/spy/fwk_spy_specs.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Dashin' Hashshashin",
		localized_name = "info.tf2pm.hat.dashinhashshashin",
		localized_description = "info.tf2pm.hat.dashinhashshashin_desc",
		icon = "backpack/player/items/spy/acr_assassins_cowl",
		models = {
			["basename"] = "models/player/items/spy/acr_assassins_cowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"feet"},
		paintable = true,
		name = "The Sneaky Spats of Sneaking",
		localized_name = "info.tf2pm.hat.spyspats",
		localized_description = "info.tf2pm.hat.spyspats_desc",
		icon = "backpack/player/items/spy/spy_spats",
		models = {
			["basename"] = "models/player/items/spy/spy_spats.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Business Casual",
		localized_name = "info.tf2pm.hat.spyopenjacket",
		localized_description = "info.tf2pm.hat.spyopenjacket_desc",
		icon = "backpack/player/items/spy/spy_openjacket",
		models = {
			["basename"] = "models/player/items/spy/spy_openjacket.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Ninja Cowl",
		localized_name = "info.tf2pm.hat.ha_spy",
		localized_description = "info.tf2pm.hat.ha_spy_desc",
		icon = "backpack/workshop_partner/player/items/spy/hero_academy_spy/hero_academy_spy",
		models = {
			["basename"] = "models/workshop_partner/player/items/spy/hero_academy_spy/hero_academy_spy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Hat of Cards",
		localized_name = "info.tf2pm.hat.spycardhat",
		localized_description = "info.tf2pm.hat.spycardhat_desc",
		icon = "backpack/player/items/spy/spy_cardhat",
		models = {
			["basename"] = "models/player/items/spy/spy_cardhat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Stealth Steeler",
		localized_name = "info.tf2pm.hat.spy_robot_fedora",
		localized_description = "info.tf2pm.hat.spy_robot_fedora_desc",
		icon = "backpack/player/items/mvm_loot/spy/robo_fedora",
		models = {
			["basename"] = "models/player/items/mvm_loot/spy/robo_fedora.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = false,
		name = "The Lacking Moral Fiber Mask",
		localized_name = "info.tf2pm.hat.dishonored_mask",
		localized_description = "info.tf2pm.hat.dishonored_mask_desc",
		icon = "backpack/workshop_partner/player/items/spy/spy_dishonored/spy_dishonored",
		models = {
			["basename"] = "models/workshop_partner/player/items/spy/spy_dishonored/spy_dishonored.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Distinguished Rogue",
		localized_name = "info.tf2pm.hat.tw_spy_coat",
		localized_description = "info.tf2pm.hat.tw_spy_coat_desc",
		icon = "backpack/workshop_partner/player/items/spy/tw_coat/tw_coat_spy",
		models = {
			["basename"] = "models/workshop_partner/player/items/spy/tw_coat/tw_coat_spy_necktie.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_spy_coat_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/spy/tw_coat/tw_coat_spy_necktie.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_spy_coat_style2",
				models = {
					["basename"] = "models/workshop_partner/player/items/spy/tw_coat/tw_coat_spy.mdl",
				}
			},
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = false,
		name = "The Scarecrow",
		localized_name = "info.tf2pm.hat.scarecrow",
		localized_description = "info.tf2pm.hat.scarecrow_desc",
		icon = "backpack/workshop/player/items/spy/spy_scarecrowface/spy_scarecrowface",
		models = {
			["basename"] = "models/workshop/player/items/spy/spy_scarecrowface/spy_scarecrowface.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Cut-Throat Concierge",
		localized_name = "info.tf2pm.hat.thecutthroatconcierge",
		localized_description = "info.tf2pm.hat.thecutthroatconcierge_desc",
		icon = "backpack/player/items/spy/spy_winterjacket",
		models = {
			["basename"] = "models/player/items/spy/spy_winterjacket.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = false,
		name = "The Dapper Disguise",
		localized_name = "info.tf2pm.hat.dapperdisguise",
		localized_description = "info.tf2pm.hat.dapperdisguise_desc",
		icon = "backpack/player/items/spy/pn2_mask",
		models = {
			["basename"] = "models/player/items/spy/pn2_mask.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = false,
		name = "MvM GateBot Light Spy",
		icon = "backpack",
		models = {
			["basename"] = "models/bots/gameplay_cosmetic/light_spy_on.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_spy_on.mdl",
				}
			},
			[1] = {
				responsive_skins = {1, 1},
				models = {
					["basename"] = "models/bots/gameplay_cosmetic/light_spy_off.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Candyman's Cap",
		localized_name = "info.tf2pm.hat.hw2013_harmburg",
		icon = "backpack/workshop/player/items/spy/hw2013_harmburg/hw2013_harmburg",
		models = {
			["basename"] = "models/workshop/player/items/spy/hw2013_harmburg/hw2013_harmburg.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"feet"},
		paintable = true,
		name = "The Rogue's Brogues",
		localized_name = "info.tf2pm.hat.jul13_rogues_brogues",
		localized_description = "info.tf2pm.hat.jul13_rogues_brogues_desc",
		icon = "backpack/workshop/player/items/spy/jul13_rogues_brogues/jul13_rogues_brogues",
		models = {
			["basename"] = "models/workshop/player/items/spy/jul13_rogues_brogues/jul13_rogues_brogues.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Rogue's Robe",
		localized_name = "info.tf2pm.hat.short2014_invisible_ishikawa",
		icon = "backpack/workshop/player/items/spy/short2014_invisible_ishikawa/short2014_invisible_ishikawa",
		models = {
			["basename"] = "models/workshop/player/items/spy/short2014_invisible_ishikawa/short2014_invisible_ishikawa.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "The Lurking Legionnaire",
		localized_name = "info.tf2pm.hat.may16_lurking_legionnaire",
		icon = "backpack/workshop/player/items/spy/majors_mark/majors_mark",
		models = {
			["basename"] = "models/workshop/player/items/spy/majors_mark/majors_mark.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Hyperbaric Bowler",
		localized_name = "info.tf2pm.hat.hw2013_brain__bowler",
		icon = "backpack/workshop/player/items/spy/hw2013_brain__bowler/hw2013_brain__bowler",
		models = {
			["basename"] = "models/workshop/player/items/spy/hw2013_brain__bowler/hw2013_brain__bowler.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Shutterbug",
		localized_name = "info.tf2pm.hat.sum19_camera_hat",
		icon = "backpack/workshop/player/items/spy/sum19_camera_hat/sum19_camera_hat",
		models = {
			["basename"] = "models/workshop/player/items/spy/sum19_camera_hat/sum19_camera_hat.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_camera_hat_style1",
				models = {
					["basename"] = "models/workshop/player/items/spy/sum19_camera_hat/sum19_camera_hat.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_camera_hat_style2",
				models = {
					["basename"] = "models/workshop/player/items/spy/sum19_camera_hat_style1/sum19_camera_hat_style1.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum19_camera_hat_style3",
				models = {
					["basename"] = "models/workshop/player/items/spy/sum19_camera_hat_style2/sum19_camera_hat_style2.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Upgrade",
		localized_name = "info.tf2pm.hat.spr17_the_upgrade",
		localized_description = "info.tf2pm.hat.spr17_the_upgrade_desc",
		icon = "backpack/workshop/player/items/spy/spr17_the_upgrade/spr17_the_upgrade",
		models = {
			["basename"] = "models/workshop/player/items/spy/spr17_the_upgrade/spr17_the_upgrade.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = false,
		name = "Rogue's Rabbit",
		localized_name = "info.tf2pm.hat.sf14_the_rogues_rabbit",
		icon = "backpack/workshop/player/items/spy/sf14_the_rogues_rabbit/sf14_the_rogues_rabbit",
		models = {
			["basename"] = "models/workshop/player/items/spy/sf14_the_rogues_rabbit/sf14_the_rogues_rabbit.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "Bird's Eye Viewer",
		localized_name = "info.tf2pm.hat.sum20_birds_eye_viewer",
		icon = "backpack/workshop/player/items/spy/sum20_birds_eye_viewer_style2/sum20_birds_eye_viewer_style2",
		models = {
			["basename"] = "models/workshop/player/items/spy/sum20_birds_eye_viewer_style2/sum20_birds_eye_viewer_style2.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 0},
				localized_name = "info.tf2pm.hat.sum20_birds_eye_viewer_style2",
				models = {
					["basename"] = "models/workshop/player/items/spy/sum20_birds_eye_viewer_style2/sum20_birds_eye_viewer_style2.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sum20_birds_eye_viewer_style1",
				models = {
					["basename"] = "models/workshop/player/items/spy/sum20_birds_eye_viewer_style1/sum20_birds_eye_viewer_style1.mdl",
				}
			},
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Nightmare Hunter",
		localized_name = "info.tf2pm.hat.sf14_nightmare_fedora",
		icon = "backpack/workshop/player/items/spy/sf14_nightmare_fedora/sf14_nightmare_fedora",
		models = {
			["basename"] = "models/workshop/player/items/spy/sf14_nightmare_fedora/sf14_nightmare_fedora.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Crabe de Chapeau",
		localized_name = "info.tf2pm.hat.sum20_crabe_de_chapeau",
		icon = "backpack/workshop/player/items/spy/sum20_crabe_de_chapeau/sum20_crabe_de_chapeau",
		models = {
			["basename"] = "models/workshop/player/items/spy/sum20_crabe_de_chapeau/sum20_crabe_de_chapeau.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "Escapist",
		localized_name = "info.tf2pm.hat.fall2013_escapist",
		icon = "backpack/workshop/player/items/spy/fall2013_escapist/fall2013_escapist",
		models = {
			["basename"] = "models/workshop/player/items/spy/fall2013_escapist/fall2013_escapist.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.fall2013_escapist_style1",
				models = {
					["basename"] = "models/workshop/player/items/spy/fall2013_escapist/fall2013_escapist.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.fall2013_escapist_style2",
				models = {
					["basename"] = "models/workshop/player/items/spy/fall2013_escapist_s2/fall2013_escapist_s2.mdl",
				}
			},
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Shadowman's Shade",
		localized_name = "info.tf2pm.hat.sf14_hw2014_spy_voodoo_hat",
		icon = "backpack/workshop/player/items/spy/sf14_hw2014_spy_voodoo_hat/sf14_hw2014_spy_voodoo_hat",
		models = {
			["basename"] = "models/workshop/player/items/spy/sf14_hw2014_spy_voodoo_hat/sf14_hw2014_spy_voodoo_hat.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "L'homme Burglerre",
		localized_name = "info.tf2pm.hat.fall2013_superthief",
		localized_description = "info.tf2pm.hat.fall2013_superthief_desc",
		icon = "backpack/workshop/player/items/spy/fall2013_superthief/fall2013_superthief",
		models = {
			["basename"] = "models/workshop/player/items/spy/fall2013_superthief/fall2013_superthief.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = false,
		name = "Facepeeler",
		localized_name = "info.tf2pm.hat.sf14_spy_facepeeler",
		icon = "backpack/workshop/player/items/spy/sf14_spy_facepeeler/sf14_spy_facepeeler",
		models = {
			["basename"] = "models/workshop/player/items/spy/sf14_spy_facepeeler/sf14_spy_facepeeler.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = true,
		name = "The Dead Head",
		localized_name = "info.tf2pm.hat.may16_skullcap",
		icon = "backpack/workshop/player/items/spy/skullmask/skullmask",
		models = {
			["basename"] = "models/workshop/player/items/spy/skullmask/skullmask.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Bootleg Base Metal Billycock",
		localized_name = "info.tf2pm.hat.robo_spy_bootleg_billycock",
		localized_description = "info.tf2pm.hat.robo_spy_bootleg_billycock_desc",
		icon = "backpack/workshop/player/items/spy/robo_spy_bootleg_billycock/robo_spy_bootleg_billycock",
		models = {
			["basename"] = "models/workshop/player/items/spy/robo_spy_bootleg_billycock/robo_spy_bootleg_billycock.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"face"},
		paintable = true,
		name = "Gruesome Gourd",
		localized_name = "info.tf2pm.hat.hwn2021_gruesome_gourd",
		icon = "backpack/workshop/player/items/spy/hwn2021_gruesome_gourd/hwn2021_gruesome_gourd",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2021_gruesome_gourd/hwn2021_gruesome_gourd.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_gruesome_gourd_style0",
				models = {
					["basename"] = "models/workshop/player/items/spy/hwn2021_gruesome_gourd/hwn2021_gruesome_gourd.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_gruesome_gourd_style1",
				models = {
					["basename"] = "models/workshop/player/items/spy/hwn2021_gruesome_gourd_style2/hwn2021_gruesome_gourd_style2.mdl",
				}
			},
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Brain-Warming Wear",
		localized_name = "info.tf2pm.hat.dec16_brain_warming_wear",
		localized_description = "info.tf2pm.hat.dec16_brain_warming_wear_desc",
		icon = "backpack/workshop/player/items/spy/dec16_brain_warming_wear/dec16_brain_warming_wear",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec16_brain_warming_wear/dec16_brain_warming_wear.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"feet"},
		paintable = true,
		name = "Bozo's Brogues",
		localized_name = "info.tf2pm.hat.hw2013_rogues_brogues",
		icon = "backpack/workshop/player/items/spy/hw2013_rogues_brogues/hw2013_rogues_brogues",
		models = {
			["basename"] = "models/workshop/player/items/spy/hw2013_rogues_brogues/hw2013_rogues_brogues.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Backstabber's Boomslang",
		localized_name = "info.tf2pm.hat.sbox2014_spy_snake",
		icon = "backpack/workshop/player/items/spy/sbox2014_spy_snake/sbox2014_spy_snake",
		models = {
			["basename"] = "models/workshop/player/items/spy/sbox2014_spy_snake/sbox2014_spy_snake.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Napoleon Complex",
		localized_name = "info.tf2pm.hat.sbox2014_napolean_complex",
		icon = "backpack/workshop/player/items/spy/sbox2014_napolean_complex/sbox2014_napolean_complex",
		models = {
			["basename"] = "models/workshop/player/items/spy/sbox2014_napolean_complex/sbox2014_napolean_complex.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = false,
		name = "tw_spybot_hood",
		localized_name = "info.tf2pm.hat.tw_spybot_hood",
		icon = "backpack/workshop/player/items/spy/tw_spybot_hood/tw_spybot_hood",
		models = {
			["basename"] = "models/workshop/player/items/spy/tw_spybot_hood/tw_spybot_hood.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"back"},
		paintable = false,
		name = "The Foul Cowl",
		localized_name = "info.tf2pm.hat.hw2013_foul_cowl",
		icon = "backpack/workshop/player/items/spy/hw2013_foul_cowl/hw2013_foul_cowl",
		models = {
			["basename"] = "models/workshop/player/items/spy/hw2013_foul_cowl/hw2013_foul_cowl.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = false,
		name = "tw_spybot_armor",
		localized_name = "info.tf2pm.hat.tw_spybot_armor",
		icon = "backpack/workshop/player/items/spy/tw_spybot_armor/tw_spybot_armor",
		models = {
			["basename"] = "models/workshop/player/items/spy/tw_spybot_armor/tw_spybot_armor.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"zombie_body"},
		paintable = false,
		name = "Zombie Spy",
		localized_name = "info.tf2pm.hat.item_zombiespy",
		icon = "backpack/player/items/spy/spy_zombie",
		models = {
			["basename"] = "models/player/items/spy/spy_zombie.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Pom-Pommed Provocateur",
		localized_name = "info.tf2pm.hat.facestabber",
		localized_description = "info.tf2pm.hat.facestabber_desc",
		icon = "backpack/workshop/player/items/spy/facestabber/facestabber",
		models = {
			["basename"] = "models/workshop/player/items/spy/facestabber/facestabber.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "Chicago Overcoat",
		localized_name = "info.tf2pm.hat.dec15_chicago_overcoat",
		icon = "backpack/workshop/player/items/spy/dec15_chicago_overcoat/dec15_chicago_overcoat",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec15_chicago_overcoat/dec15_chicago_overcoat.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Murderer's Motif",
		localized_name = "info.tf2pm.hat.hwn2018_murderers_motif",
		icon = "backpack/workshop/player/items/spy/hwn2018_murderers_motif/hwn2018_murderers_motif",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2018_murderers_motif/hwn2018_murderers_motif.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {},
		paintable = true,
		name = "Avian Amante",
		localized_name = "info.tf2pm.hat.hwn2019_avian_amante",
		icon = "backpack/workshop/player/items/spy/hwn2019_avian_amante/hwn2019_avian_amante",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2019_avian_amante/hwn2019_avian_amante.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"feet"},
		paintable = true,
		name = "Bandit's Boots",
		localized_name = "info.tf2pm.hat.hwn2018_bandits_boots",
		icon = "backpack/workshop/player/items/spy/hwn2018_bandits_boots/hwn2018_bandits_boots",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2018_bandits_boots/hwn2018_bandits_boots.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Au Courant Assassin",
		localized_name = "info.tf2pm.hat.short2014_spy_ascot_vest",
		icon = "backpack/workshop/player/items/spy/short2014_spy_ascot_vest/short2014_spy_ascot_vest",
		models = {
			["basename"] = "models/workshop/player/items/spy/short2014_spy_ascot_vest/short2014_spy_ascot_vest.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "Dressperado",
		localized_name = "info.tf2pm.hat.hwn2018_dressperado",
		icon = "backpack/workshop/player/items/spy/hwn2018_dressperado/hwn2018_dressperado",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2018_dressperado/hwn2018_dressperado.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Lady Killer",
		localized_name = "info.tf2pm.hat.sept2014_lady_killer",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/spy/sept2014_lady_killer/sept2014_lady_killer",
		models = {
			["basename"] = "models/workshop/player/items/spy/sept2014_lady_killer/sept2014_lady_killer.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Bountiful Bow",
		localized_name = "info.tf2pm.hat.hw2013_blood_banker",
		icon = "backpack/workshop/player/items/spy/hw2013_blood_banker/hw2013_blood_banker",
		models = {
			["basename"] = "models/workshop/player/items/spy/hw2013_blood_banker/hw2013_blood_banker.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"beard"},
		paintable = false,
		name = "The Megapixel Beard",
		localized_name = "info.tf2pm.hat.robo_spy_camera_beard",
		localized_description = "info.tf2pm.hat.robo_spy_camera_beard_desc",
		icon = "backpack/workshop/player/items/spy/robo_spy_camera_beard/robo_spy_camera_beard",
		models = {
			["basename"] = "models/workshop/player/items/spy/robo_spy_camera_beard/robo_spy_camera_beard.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Aviator Assassin",
		localized_name = "info.tf2pm.hat.short2014_deadhead",
		icon = "backpack/workshop/player/items/spy/short2014_deadhead/short2014_deadhead",
		models = {
			["basename"] = "models/workshop/player/items/spy/short2014_deadhead/short2014_deadhead.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Voodoo Vizier",
		localized_name = "info.tf2pm.hat.hwn2019_voodoo_vizier",
		icon = "backpack/workshop/player/items/spy/hwn2019_voodoo_vizier/hwn2019_voodoo_vizier",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2019_voodoo_vizier/hwn2019_voodoo_vizier.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"left_shoulder"},
		paintable = true,
		name = "The Spycrab",
		localized_name = "info.tf2pm.hat.spycrab",
		icon = "backpack/workshop/player/items/spy/spycrab/spycrab",
		models = {
			["basename"] = "models/workshop/player/items/spy/spycrab/spycrab.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Crustaceous Cowl",
		localized_name = "info.tf2pm.hat.sum21_crustaceous_cowl",
		icon = "backpack/workshop/player/items/spy/sum21_crustaceous_cowl/sum21_crustaceous_cowl",
		models = {
			["basename"] = "models/workshop/player/items/spy/sum21_crustaceous_cowl/sum21_crustaceous_cowl.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"medal"},
		paintable = true,
		name = "dec2014 Pocket Momma",
		localized_name = "info.tf2pm.hat.dec2014_pocket_momma",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/spy/dec2014_pocket_momma/dec2014_pocket_momma",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec2014_pocket_momma/dec2014_pocket_momma.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "Smoking Jacket",
		localized_name = "info.tf2pm.hat.sum20_smoking_jacket",
		icon = "backpack/workshop/player/items/spy/sum20_smoking_jacket/sum20_smoking_jacket",
		models = {
			["basename"] = "models/workshop/player/items/spy/sum20_smoking_jacket/sum20_smoking_jacket.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Sky Captain",
		localized_name = "info.tf2pm.hat.short2014_confidence_trickster",
		icon = "backpack/workshop/player/items/spy/short2014_confidence_trickster/short2014_confidence_trickster",
		models = {
			["basename"] = "models/workshop/player/items/spy/short2014_confidence_trickster/short2014_confidence_trickster.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "The Blood Banker",
		localized_name = "info.tf2pm.hat.jul13_blood_banker",
		localized_description = "info.tf2pm.hat.jul13_blood_banker_desc",
		icon = "backpack/workshop/player/items/spy/jul13_blood_banker/jul13_blood_banker",
		models = {
			["basename"] = "models/workshop/player/items/spy/jul13_blood_banker/jul13_blood_banker.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Base Metal Billycock",
		localized_name = "info.tf2pm.hat.robo_spy_backbiter_billycock",
		localized_description = "info.tf2pm.hat.robo_spy_backbiter_billycock_desc",
		icon = "backpack/workshop/player/items/spy/robo_spy_backbiter_billycock/robo_spy_backbiter_billycock",
		models = {
			["basename"] = "models/workshop/player/items/spy/robo_spy_backbiter_billycock/robo_spy_backbiter_billycock.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "dec2014 The Puffy Provocateur",
		localized_name = "info.tf2pm.hat.dec2014_the_puffy_provocateur",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/spy/dec2014_the_puffy_provocateur/dec2014_the_puffy_provocateur",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec2014_the_puffy_provocateur/dec2014_the_puffy_provocateur.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = true,
		name = "Lurker's Leathers",
		localized_name = "info.tf2pm.hat.cc_summer2015_lurkers_leathers",
		icon = "backpack/workshop/player/items/spy/cc_summer2015_lurkers_leathers/cc_summer2015_lurkers_leathers",
		models = {
			["basename"] = "models/workshop/player/items/spy/cc_summer2015_lurkers_leathers/cc_summer2015_lurkers_leathers.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_lurkers_leathers_style0",
				models = {
					["basename"] = "models/workshop/player/items/spy/cc_summer2015_lurkers_leathers/cc_summer2015_lurkers_leathers.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.cc_summer2015_lurkers_leathers_style1",
				models = {
					["basename"] = "models/workshop/player/items/spy/cc_summer2015_lurkers_leathers_2/cc_summer2015_lurkers_leathers_2.mdl",
				}
			},
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Big Topper",
		localized_name = "info.tf2pm.hat.hwn2016_big_topper",
		icon = "backpack/workshop/player/items/spy/hwn2016_big_topper/hwn2016_big_topper",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2016_big_topper/hwn2016_big_topper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "dec2014 Stealthy Scarf",
		localized_name = "info.tf2pm.hat.dec2014_stealthy_scarf",
		localized_description = "info.tf2pm.hat.dec2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/spy/dec2014_stealthy_scarf/dec2014_stealthy_scarf",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec2014_stealthy_scarf/dec2014_stealthy_scarf.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "A Hat to Kill For",
		localized_name = "info.tf2pm.hat.dec15_a_hat_to_kill_for",
		icon = "backpack/workshop/player/items/spy/dec15_a_hat_to_kill_for/dec15_a_hat_to_kill_for",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec15_a_hat_to_kill_for/dec15_a_hat_to_kill_for.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"arms"},
		paintable = true,
		name = "Aristotle",
		localized_name = "info.tf2pm.hat.spr18_aristotle",
		icon = "backpack/workshop/player/items/spy/spr18_aristotle/spr18_aristotle",
		models = {
			["basename"] = "models/workshop/player/items/spy/spr18_aristotle/spr18_aristotle.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "Showstopper",
		localized_name = "info.tf2pm.hat.hwn2016_showstopper",
		icon = "backpack/workshop/player/items/spy/hwn2016_showstopper/hwn2016_showstopper",
		models = {
			["basename"] = "models/workshop/player/items/spy/hwn2016_showstopper/hwn2016_showstopper.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Belgian Detective",
		localized_name = "info.tf2pm.hat.jul13_double_clue",
		localized_description = "info.tf2pm.hat.jul13_double_clue_desc",
		icon = "backpack/workshop/player/items/spy/jul13_double_clue/jul13_double_clue",
		models = {
			["basename"] = "models/workshop/player/items/spy/jul13_double_clue/jul13_double_clue.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"spy_coat"},
		paintable = false,
		name = "The After Dark",
		localized_name = "info.tf2pm.hat.jul13_classy_royale",
		localized_description = "info.tf2pm.hat.jul13_classy_royale_desc",
		icon = "backpack/workshop/player/items/spy/jul13_classy_royale/jul13_classy_royale",
		models = {
			["basename"] = "models/workshop/player/items/spy/jul13_classy_royale/jul13_classy_royale.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "Festive Cover-Up",
		localized_name = "info.tf2pm.hat.dec21_festive_cover_up",
		icon = "backpack/workshop/player/items/spy/dec21_festive_cover_up/dec21_festive_cover_up",
		models = {
			["basename"] = "models/workshop/player/items/spy/dec21_festive_cover_up/dec21_festive_cover_up.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"hat"},
		paintable = true,
		name = "The Harmburg",
		localized_name = "info.tf2pm.hat.jul13_harmburg",
		localized_description = "info.tf2pm.hat.jul13_harmburg_desc",
		icon = "backpack/workshop/player/items/spy/jul13_harmburg/jul13_harmburg",
		models = {
			["basename"] = "models/workshop/player/items/spy/jul13_harmburg/jul13_harmburg.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy"},
		targets = {"head_skin"},
		paintable = true,
		name = "The Graylien",
		localized_name = "info.tf2pm.hat.invasion_the_graylien",
		icon = "backpack/workshop/player/items/spy/invasion_the_graylien/invasion_the_graylien",
		models = {
			["basename"] = "models/workshop/player/items/spy/invasion_the_graylien/invasion_the_graylien.mdl",
		},
	},
	{
		classes = {"spy"},
		targets = {"shirt"},
		paintable = true,
		name = "Assassin's Attire",
		localized_name = "info.tf2pm.hat.spr18_assassins_attire",
		icon = "backpack/workshop/player/items/spy/spr18_assassins_attire/spr18_assassins_attire",
		models = {
			["basename"] = "models/workshop/player/items/spy/spr18_assassins_attire/spr18_assassins_attire.mdl",
		},
	},

}
