
-- Auto generated at 2022-02-26 20:22:44 UTC+07:00
return {
	{
		classes = {"heavy", "soldier", "medic"},
		targets = {"hat"},
		paintable = false,
		name = "The Team Captain",
		localized_name = "info.tf2pm.hat.teamcaptain",
		localized_description = "info.tf2pm.hat.teamcaptain_desc",
		icon = "backpack/player/items/soldier/soldier_officer",
		models = {
			["basename"] = "models/player/items/%s/%s_officer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo", "spy", "medic"},
		targets = {"hat"},
		paintable = false,
		name = "Private Eye",
		localized_name = "info.tf2pm.hat.private_eye",
		localized_description = "info.tf2pm.hat.private_eye_desc",
		icon = "backpack/player/items/spy/spy_private_eye",
		models = {
			["basename"] = "models/player/items/spy/spy_private_eye.mdl",
		},
	},
	{
		classes = {"soldier", "heavy"},
		targets = {"heavy_pocket"},
		paintable = false,
		name = "Pocket Medic",
		localized_name = "info.tf2pm.hat.pocketmedic",
		localized_description = "info.tf2pm.hat.pocketmedic_desc",
		icon = "backpack/player/items/heavy/pocket_medic",
		models = {
			["soldier"] = "models/player/items/soldier/fwk_soldier_pocketmedic.mdl",
			["heavy"] = "models/player/items/heavy/pocket_medic.mdl",
		},
	},
	{
		classes = {"soldier", "demo", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Hat With No Name",
		localized_name = "info.tf2pm.hat.soldierhat2",
		localized_description = "info.tf2pm.hat.soldierhat2_desc",
		icon = "backpack/workshop/player/items/all_class/fwk_cowboyhat/fwk_cowboyhat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fwk_cowboyhat/fwk_cowboyhat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"spy", "medic"},
		targets = {"shirt", "necklace"},
		paintable = true,
		name = "Bowtie",
		localized_name = "info.tf2pm.hat.bowtie",
		icon = "backpack/player/items/medic/bowtie",
		models = {
			["basename"] = "models/player/items/%s/bowtie.mdl",
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "The Ornament Armament",
		localized_name = "info.tf2pm.hat.shinybauls",
		localized_description = "info.tf2pm.hat.shinybauls_desc",
		icon = "backpack/player/items/soldier/xms_soldier_ornaments",
		models = {
			["soldier"] = "models/player/items/soldier/xms_soldier_ornaments.mdl",
			["pyro"] = "models/player/items/soldier/xms_pyro_ornaments.mdl",
			["demoman"] = "models/player/items/soldier/xms_demo_ornaments.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"engineer", "sniper"},
		targets = {"engineer_pocket", "sniper_pocket"},
		paintable = false,
		name = "The Itsy Bitsy Spyer",
		localized_name = "info.tf2pm.hat.itsybitsyspyer",
		localized_description = "info.tf2pm.hat.itsybitsyspyer_desc",
		icon = "backpack/player/items/engineer/xms_engineer_voodoospy",
		models = {
			["engineer"] = "models/player/items/engineer/xms_engineer_voodoospy.mdl",
			["sniper"] = "models/player/items/sniper/sniper_voodoospy.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "soldier", "demo", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The All-Father",
		localized_name = "info.tf2pm.hat.allfather",
		localized_description = "info.tf2pm.hat.allfather_desc",
		icon = "backpack/workshop/player/items/all_class/xms_beard/xms_beard",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms_beard/xms_beard_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.allfather_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/xms_beard/xms_beard_%s.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.allfather_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/xms_beard_s2/xms_beard_s2_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"demo", "scout", "soldier", "engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "The Teufort Tooth Kicker",
		localized_name = "info.tf2pm.hat.cowboyboots",
		localized_description = "info.tf2pm.hat.cowboyboots_desc",
		icon = "backpack/player/items/all_class/cowboyboots_soldier",
		models = {
			["basename"] = "models/player/items/all_class/cowboyboots_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"pyro", "medic"},
		targets = {"back"},
		paintable = true,
		name = "The Scrap Pack",
		localized_name = "info.tf2pm.hat.shootmanyrobotspack",
		localized_description = "info.tf2pm.hat.shootmanyrobotspack_desc",
		icon = "backpack/player/items/pyro/shootmanyrobots_pyro",
		models = {
			["basename"] = "models/player/items/%s/shootmanyrobots_%s.mdl",
		},
		bodygroup_overrides = {
			["backpack"] = 1,
			["medic_backpack"] = 1,
		},
	},
	{
		classes = {"spy", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Doublecross-Comm",
		localized_name = "info.tf2pm.hat.grfs_3",
		localized_description = "info.tf2pm.hat.grfs_3_desc",
		icon = "backpack/player/items/sniper/grfs_sniper",
		models = {
			["basename"] = "models/player/items/%s/grfs_%s.mdl",
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "The Professor's Pineapple",
		localized_name = "info.tf2pm.hat.qc_flasks",
		localized_description = "info.tf2pm.hat.qc_flasks_desc",
		icon = "backpack/player/items/all_class/qc_flask_demo",
		models = {
			["basename"] = "models/player/items/all_class/qc_flask_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro", "sniper"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The HazMat Headcase",
		localized_name = "info.tf2pm.hat.pyrohazmat",
		localized_description = "info.tf2pm.hat.pyrohazmat_desc",
		icon = "backpack/workshop/player/items/all_class/pyro_hazmat/pyro_hazmat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/pyro_hazmat/pyro_hazmat_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.pyrohazmat_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/pyro_hazmat/pyro_hazmat_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.pyrohazmat_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/pyro_hazmat_2/pyro_hazmat_2_%s.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.pyrohazmat_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/pyro_hazmat_3/pyro_hazmat_3_%s.mdl",
				}
			},
			[3] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.pyrohazmat_style3",
				models = {
					["basename"] = "models/workshop/player/items/all_class/pyro_hazmat_4/pyro_hazmat_4_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["head"] = 1,
		},
	},
	{
		classes = {"heavy", "scout", "spy", "engineer", "sniper"},
		targets = {"shirt"},
		paintable = false,
		name = "The Triad Trinket",
		localized_name = "info.tf2pm.hat.sd_chain",
		localized_description = "info.tf2pm.hat.sd_chain_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_sniper",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_heavy.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_scout.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_sniper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sd_chain_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.sd_chain_style1",
			},
		},
	},
	{
		classes = {"scout", "heavy", "engineer", "sniper"},
		targets = {"arm_tatoos"},
		paintable = true,
		name = "The Champ Stamp",
		localized_name = "info.tf2pm.hat.sd_tattoos",
		localized_description = "info.tf2pm.hat.sd_tattoos_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_scout",
		models = {
			["scout"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_scout.mdl",
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_heavy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_sniper.mdl",
		},
	},
	{
		classes = {"heavy", "scout", "spy", "engineer", "sniper"},
		targets = {"shirt"},
		paintable = false,
		name = "Promo Triad Trinket",
		localized_name = "info.tf2pm.hat.sd_chain",
		localized_description = "info.tf2pm.hat.sd_chain_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_sniper",
		models = {
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_heavy.mdl",
			["scout"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_scout.mdl",
			["spy"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_spy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_shirt/sd_shirt_sniper.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sd_chain_style0",
			},
			[1] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.sd_chain_style1",
			},
		},
	},
	{
		classes = {"scout", "heavy", "engineer", "sniper"},
		targets = {"arm_tatoos"},
		paintable = true,
		name = "Promo Champ Stamp",
		localized_name = "info.tf2pm.hat.sd_tattoos",
		localized_description = "info.tf2pm.hat.sd_tattoos_desc",
		icon = "backpack/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_scout",
		models = {
			["scout"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_scout.mdl",
			["heavy"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_heavy.mdl",
			["engineer"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_engineer.mdl",
			["sniper"] = "models/workshop_partner/player/items/all_class/sd_tattoos/sd_tattoos_sniper.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "engineer", "soldier"},
		targets = {"hat"},
		paintable = true,
		name = "The Crafty Hair",
		localized_name = "info.tf2pm.hat.xcom_hair",
		localized_description = "info.tf2pm.hat.xcom_hair_desc",
		icon = "backpack/player/items/all_class/xcom_flattop_engineer",
		models = {
			["basename"] = "models/player/items/all_class/xcom_flattop_%s.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.xcom_hair_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.xcom_hair_style1",
			},
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "demo"},
		targets = {"beard"},
		paintable = true,
		name = "The Menpo",
		localized_name = "info.tf2pm.hat.tw_mask",
		localized_description = "info.tf2pm.hat.tw_mask_desc",
		icon = "backpack/workshop_partner/player/items/demo/tw_shogun/tw_shogun_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/%s/tw_shogun/tw_shogun_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_mask_style1",
				models = {
					["basename"] = "models/workshop_partner/player/items/%s/tw_shogun/tw_shogun_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_mask_style2",
				models = {
					["basename"] = "models/workshop_partner/player/items/%s/tw_shogun/tw_shogun2_%s.mdl",
				}
			},
			[2] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_mask_style3",
				models = {
					["basename"] = "models/workshop_partner/player/items/%s/tw_shogun/tw_shogun3_%s.mdl",
				}
			},
			[3] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.tw_mask_style4",
				models = {
					["basename"] = "models/workshop_partner/player/items/%s/tw_shogun/tw_shogun4_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"demo", "heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The K-9 Mane",
		localized_name = "info.tf2pm.hat.tw_mane",
		localized_description = "info.tf2pm.hat.tw_mane_desc",
		icon = "backpack/workshop_partner/player/items/heavy/tw_doghat/tw_doghat_heavy",
		models = {
			["demoman"] = "models/workshop_partner/player/items/demo/tw_doghat/tw_doghat_demo.mdl",
			["heavy"] = "models/workshop_partner/player/items/heavy/tw_doghat/tw_doghat_heavy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "The Bonedolier",
		localized_name = "info.tf2pm.hat.bonedolier",
		localized_description = "info.tf2pm.hat.bonedolier_desc",
		icon = "backpack/player/items/all_class/demo_grenade_skulls",
		models = {
			["basename"] = "models/player/items/all_class/%s_grenade_skulls.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"heavy", "scout", "demo", "engineer", "sniper"},
		targets = {"sleeves"},
		paintable = true,
		name = "The Spooky Sleeves",
		localized_name = "info.tf2pm.hat.spookysleeves",
		localized_description = "info.tf2pm.hat.spookysleeves_desc",
		icon = "backpack/workshop/player/items/all_class/halloweenjacket/halloweenjacket",
		models = {
			["basename"] = "models/workshop/player/items/all_class/halloweenjacket/halloweenjacket_%s.mdl",
		},
	},
	{
		classes = {"soldier", "spy", "pyro", "medic"},
		targets = {"shirt"},
		paintable = true,
		name = "The Exorcizor",
		localized_name = "info.tf2pm.hat.exorcizor",
		localized_description = "info.tf2pm.hat.exorcizor_desc",
		icon = "backpack/workshop/player/items/all_class/hwn_spy_priest/hwn_spy_priest",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn_spy_priest/hwn_spy_priest_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro", "soldier", "demo", "engineer", "sniper"},
		targets = {"grenades", "engineer_pocket", "sniper_pocket"},
		paintable = true,
		name = "The Deadliest Duckling",
		localized_name = "info.tf2pm.hat.hm_duck",
		localized_description = "info.tf2pm.hat.hm_duck_desc",
		icon = "backpack/workshop_partner/player/items/all_class/hm_duck/hm_duck_demo",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/hm_duck/hm_duck_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {2, 3},
				localized_name = "info.tf2pm.hat.hm_duck_style0",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hm_duck_style1",
			},
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"heavy", "soldier", "medic", "scout", "demo", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "The Mutton Mann",
		localized_name = "info.tf2pm.hat.themuttonmann",
		localized_description = "info.tf2pm.hat.themuttonmann_desc",
		icon = "backpack/workshop/player/items/all_class/winter_sideburns/winter_sideburns",
		models = {
			["basename"] = "models/workshop/player/items/all_class/winter_sideburns/winter_sideburns_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "engineer", "demo"},
		targets = {"hat"},
		paintable = true,
		name = "The Wilson Weave",
		localized_name = "info.tf2pm.hat.wilsonweave",
		localized_description = "info.tf2pm.hat.wilsonweave_desc",
		icon = "backpack/player/items/all_class/starve_demo",
		models = {
			["basename"] = "models/player/items/all_class/starve_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.nohat_style",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
			[1] = {
				localized_name = "info.tf2pm.hat.nohat_noheadphones_style",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "demo"},
		targets = {"feet"},
		paintable = true,
		name = "Buck Turner All-Stars",
		localized_name = "info.tf2pm.hat.turnerallstars",
		localized_description = "info.tf2pm.hat.turnerallstars_desc",
		icon = "backpack/player/items/demo/bit_trippers_demo",
		models = {
			["basename"] = "models/player/items/%s/bit_trippers_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes"] = 1,
			["shoes_socks"] = 1,
		},
	},
	{
		classes = {"spy", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "The Bloodhound",
		localized_name = "info.tf2pm.hat.bloodhound",
		localized_description = "info.tf2pm.hat.bloodhound_desc",
		icon = "backpack/player/items/spy/pn2_samhat_spy",
		models = {
			["basename"] = "models/player/items/%s/pn2_samhat_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Head Hedge",
		localized_name = "info.tf2pm.hat.spr18_head_hedge",
		icon = "backpack/workshop/player/items/all_class/spr18_head_hedge/spr18_head_hedge",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/spr18_head_hedge/spr18_head_hedge_soldier.mdl",
			["sniper"] = "models/workshop/player/items/all_class/spr18_head_hedge/spr18_head_hedge_sniper.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "demo", "engineer", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Antarctic Eyewear",
		localized_name = "info.tf2pm.hat.spr18_antarctic_eyewear",
		icon = "backpack/workshop/player/items/all_class/spr18_antarctic_eyewear/spr18_antarctic_eyewear",
		models = {
			["scout"] = "models/workshop/player/items/all_class/spr18_antarctic_eyewear/spr18_antarctic_eyewear_scout.mdl",
			["demoman"] = "models/workshop/player/items/all_class/spr18_antarctic_eyewear/spr18_antarctic_eyewear_demo.mdl",
			["engineer"] = "models/workshop/player/items/all_class/spr18_antarctic_eyewear/spr18_antarctic_eyewear_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/spr18_antarctic_eyewear/spr18_antarctic_eyewear_sniper.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Toy Soldier",
		localized_name = "info.tf2pm.hat.xms2013_sniper_shako",
		icon = "backpack/workshop/player/items/all_class/xms2013_sniper_shako/xms2013_sniper_shako",
		models = {
			["basename"] = "models/workshop/player/items/all_class/xms2013_sniper_shako/xms2013_sniper_shako_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "spy"},
		targets = {"shirt"},
		paintable = true,
		name = "The Frenchman's Formals",
		localized_name = "info.tf2pm.hat.fall2013_spy_fancycoat",
		icon = "backpack/workshop/player/items/all_class/fall2013_fancycoat/fall2013_fancycoat",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_fancycoat/fall2013_fancycoat_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_spy_fancycoat_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_fancycoat/fall2013_fancycoat_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_spy_fancycoat_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_fancycoat_s2/fall2013_fancycoat_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"soldier", "heavy", "engineer", "demo"},
		targets = {"hat"},
		paintable = true,
		name = "Sophisticated Smoker",
		localized_name = "info.tf2pm.hat.sum20_sophisticated_smoker",
		icon = "backpack/workshop/player/items/all_class/sum20_sophisticated_smoker/sum20_sophisticated_smoker",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/sum20_sophisticated_smoker/sum20_sophisticated_smoker_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/sum20_sophisticated_smoker/sum20_sophisticated_smoker_demo.mdl",
			["engineer"] = "models/workshop/player/items/all_class/sum20_sophisticated_smoker/sum20_sophisticated_smoker_engineer.mdl",
			["heavy"] = "models/workshop/player/items/all_class/sum20_sophisticated_smoker/sum20_sophisticated_smoker_heavy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"scout", "heavy", "demo"},
		targets = {"shirt"},
		paintable = true,
		name = "Weight Room Warmer",
		localized_name = "info.tf2pm.hat.fall2013_weight_room_warmer",
		icon = "backpack/workshop/player/items/all_class/fall2013_weight_room_warmer/fall2013_weight_room_warmer",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_weight_room_warmer/fall2013_weight_room_warmer_%s.mdl",
		},
		bodygroup_overrides = {
			["dogtags"] = 1,
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "The Candleer",
		localized_name = "info.tf2pm.hat.hw2013_kindlin_candles",
		icon = "backpack/workshop/player/items/all_class/hw2013_kindlin_candles/hw2013_kindlin_candles",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_kindlin_candles/hw2013_kindlin_candles_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"spy", "medic"},
		targets = {"necklace"},
		paintable = true,
		name = "Harry",
		localized_name = "info.tf2pm.hat.dec18_harry",
		icon = "backpack/workshop/player/items/all_class/dec18_harry/dec18_harry",
		models = {
			["medic"] = "models/workshop/player/items/all_class/dec18_harry/dec18_harry_medic.mdl",
			["spy"] = "models/workshop/player/items/all_class/dec18_harry/dec18_harry_spy.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec18_harry_style0",
				models = {
					["medic"] = "models/workshop/player/items/all_class/dec18_harry/dec18_harry_medic.mdl",
					["spy"] = "models/workshop/player/items/all_class/dec18_harry/dec18_harry_spy.mdl",
				}
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec18_harry_style1",
				models = {
					["medic"] = "models/workshop/player/items/all_class/dec18_harry_sleeping/dec18_harry_sleeping_medic.mdl",
					["spy"] = "models/workshop/player/items/all_class/dec18_harry_sleeping/dec18_harry_sleeping_spy.mdl",
				}
			},
			[2] = {
				localized_name = "info.tf2pm.hat.dec18_harry_style2",
				models = {
					["medic"] = "models/workshop/player/items/all_class/dec18_harry_villainous/dec18_harry_villainous_medic.mdl",
					["spy"] = "models/workshop/player/items/all_class/dec18_harry_villainous/dec18_harry_villainous_spy.mdl",
				}
			},
		},
	},
	{
		classes = {"demo", "pyro"},
		targets = {"shirt"},
		paintable = true,
		name = "The Sub Zero Suit",
		localized_name = "info.tf2pm.hat.xms2013_pyro_arctic_suit",
		icon = "backpack/workshop/player/items/all_class/xms2013_arctic_suit/xms2013_arctic_suit",
		models = {
			["demoman"] = "models/workshop/player/items/all_class/xms2013_arctic_suit/xms2013_arctic_suit_demo.mdl",
			["pyro"] = "models/workshop/player/items/all_class/xms2013_arctic_suit/xms2013_arctic_suit_pyro.mdl",
		},
	},
	{
		classes = {"soldier", "pyro"},
		targets = {"grenades"},
		paintable = true,
		name = "Pin Pals",
		localized_name = "info.tf2pm.hat.hw2013_volatile_voodoo",
		icon = "backpack/workshop/player/items/all_class/hw2013_volatile_voodoo/hw2013_volatile_voodoo",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_volatile_voodoo/hw2013_volatile_voodoo_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"spy", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Deep Cover Operator",
		localized_name = "info.tf2pm.hat.sbox2014_camo_headband",
		icon = "backpack/workshop/player/items/all_class/sbox2014_camo_headband/sbox2014_camo_headband",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_camo_headband/sbox2014_camo_headband_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo", "sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "The Toowoomba Tunic",
		localized_name = "info.tf2pm.hat.sbox2014_toowoomba_tunic",
		icon = "backpack/workshop/player/items/all_class/sbox2014_toowoomba_tunic/sbox2014_toowoomba_tunic",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_toowoomba_tunic/sbox2014_toowoomba_tunic_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_toowoomba_tunic_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_toowoomba_tunic/sbox2014_toowoomba_tunic_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_toowoomba_tunic_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/sbox2014_toowoomba_tunic_style1/sbox2014_toowoomba_tunic_style1_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "spy", "engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Computron 5000",
		localized_name = "info.tf2pm.hat.hwn2021_computron",
		icon = "backpack/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron_heavy.mdl",
			["medic"] = "models/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron_medic.mdl",
			["scout"] = "models/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron_scout.mdl",
			["spy"] = "models/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/hwn2021_computron/hwn2021_computron_engineer.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "Trickster's Treats",
		localized_name = "info.tf2pm.hat.hwn2021_tricksters_treats",
		icon = "backpack/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_soldier.mdl",
			["pyro"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_pyro.mdl",
			["demoman"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_demo.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_tricksters_treats_style0",
				models = {
					["soldier"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_soldier.mdl",
					["pyro"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_pyro.mdl",
					["demoman"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats/hwn2021_tricksters_treats_demo.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.hwn2021_tricksters_treats_style1",
				models = {
					["soldier"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats_style2/hwn2021_tricksters_treats_style2_soldier.mdl",
					["pyro"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats_style2/hwn2021_tricksters_treats_style2_pyro.mdl",
					["demoman"] = "models/workshop/player/items/all_class/hwn2021_tricksters_treats_style2/hwn2021_tricksters_treats_style2_demo.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"pyro", "engineer"},
		targets = {"pyro_head_replacement"},
		paintable = true,
		name = "The Cute Suit",
		localized_name = "info.tf2pm.hat.sbox2014_pyro_zipper_suit",
		icon = "backpack/workshop/player/items/all_class/sbox2014_zipper_suit/sbox2014_zipper_suit",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_zipper_suit/sbox2014_zipper_suit_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_pyro_zipper_suit_style0",
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.sbox2014_pyro_zipper_suit_style1",
				bodygroup_overrides = {
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"engineer", "pyro", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "Winter Wrap Up",
		localized_name = "info.tf2pm.hat.dec19_winter_wrap_up",
		icon = "backpack/workshop/player/items/all_class/dec19_winter_wrap_up/dec19_winter_wrap_up",
		models = {
			["pyro"] = "models/workshop/player/items/all_class/dec19_winter_wrap_up/dec19_winter_wrap_up_pyro.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec19_winter_wrap_up/dec19_winter_wrap_up_engineer.mdl",
			["sniper"] = "models/workshop/player/items/all_class/dec19_winter_wrap_up/dec19_winter_wrap_up_sniper.mdl",
		},
		bodygroup_overrides = {
			["head"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "pyro", "engineer"},
		targets = {"belt_misc"},
		paintable = true,
		name = "The Beep Boy",
		localized_name = "info.tf2pm.hat.fall2013_beep_boy",
		localized_description = "info.tf2pm.hat.fall2013_beep_boy_desc",
		icon = "backpack/workshop/player/items/all_class/fall2013_beep_boy/fall2013_beep_boy",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_beep_boy/fall2013_beep_boy_%s.mdl",
		},
	},
	{
		classes = {"engineer", "sniper"},
		targets = {"feet"},
		paintable = true,
		name = "Support Spurs",
		localized_name = "info.tf2pm.hat.cc_summer2015_support_spurs",
		icon = "backpack/workshop/player/items/all_class/cc_summer2015_support_spurs/cc_summer2015_support_spurs",
		models = {
			["basename"] = "models/workshop/player/items/all_class/cc_summer2015_support_spurs/cc_summer2015_support_spurs_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["dogtags"] = 1,
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"belt_misc"},
		paintable = true,
		name = "Socked and Loaded",
		localized_name = "info.tf2pm.hat.dec16_stocking",
		localized_description = "info.tf2pm.hat.dec16_stocking_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_stocking/dec16_stocking",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/dec16_stocking/dec16_stocking_soldier.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_stocking/dec16_stocking_pyro.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_stocking/dec16_stocking_demo.mdl",
		},
	},
	{
		classes = {"scout", "spy", "engineer"},
		targets = {"shirt"},
		paintable = false,
		name = "Aloha Apparel",
		localized_name = "info.tf2pm.hat.fall17_aloha_apparel",
		localized_description = "info.tf2pm.hat.fall17_aloha_apparel_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_aloha_apparel/fall17_aloha_apparel",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_aloha_apparel/fall17_aloha_apparel_%s.mdl",
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = false,
		name = "Handy Canes",
		localized_name = "info.tf2pm.hat.dec16_handy_canes",
		localized_description = "info.tf2pm.hat.dec16_handy_canes_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_handy_canes/dec16_handy_canes",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/dec16_handy_canes/dec16_handy_canes_soldier.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_handy_canes/dec16_handy_canes_pyro.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_handy_canes/dec16_handy_canes_demo.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier", "heavy"},
		targets = {"soldier_coat"},
		paintable = true,
		name = "The Gift Bringer",
		localized_name = "info.tf2pm.hat.dec15_gift_bringer",
		icon = "backpack/workshop/player/items/all_class/dec15_gift_bringer/dec15_gift_bringer",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec15_gift_bringer/dec15_gift_bringer_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"soldier", "heavy", "demo"},
		targets = {"hat"},
		paintable = false,
		name = "The War Eagle",
		localized_name = "info.tf2pm.hat.fall17_war_eagle",
		localized_description = "info.tf2pm.hat.fall17_war_eagle_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_war_eagle/fall17_war_eagle",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_war_eagle/fall17_war_eagle_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "pyro", "demo"},
		targets = {"grenades"},
		paintable = true,
		name = "Bananades",
		localized_name = "info.tf2pm.hat.fall17_bananades",
		localized_description = "info.tf2pm.hat.fall17_bananades_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_bananades/fall17_bananades",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_bananades/fall17_bananades_%s.mdl",
		},
		bodygroup_overrides = {
			["grenades"] = 1,
		},
	},
	{
		classes = {"engineer", "sniper"},
		targets = {"engineer_pocket", "sniper_pocket"},
		paintable = true,
		name = "Hillbilly Speed-Bump",
		localized_name = "info.tf2pm.hat.sept2014_hillbilly_speedbump",
		localized_description = "info.tf2pm.hat.sept2014_cosmetic_desc",
		icon = "backpack/workshop/player/items/all_class/sept2014_hillbilly_speedbump/sept2014_hillbilly_speedbump",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sept2014_hillbilly_speedbump/sept2014_hillbilly_speedbump_%s.mdl",
		},
	},
	{
		classes = {"scout", "heavy", "pyro", "soldier"},
		targets = {"hat"},
		paintable = true,
		name = "Pestering Jester",
		localized_name = "info.tf2pm.hat.hwn2016_pestering_jester",
		icon = "backpack/workshop/player/items/all_class/hwn2016_pestering_jester/hwn2016_pestering_jester",
		models = {
			["scout"] = "models/workshop/player/items/all_class/hwn2016_pestering_jester/hwn2016_pestering_jester_scout.mdl",
			["soldier"] = "models/workshop/player/items/all_class/hwn2016_pestering_jester/hwn2016_pestering_jester_soldier.mdl",
			["pyro"] = "models/workshop/player/items/all_class/hwn2016_pestering_jester/hwn2016_pestering_jester_pyro.mdl",
			["heavy"] = "models/workshop/player/items/all_class/hwn2016_pestering_jester/hwn2016_pestering_jester_heavy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"soldier", "heavy"},
		targets = {"hat"},
		paintable = true,
		name = "The Nuke",
		localized_name = "info.tf2pm.hat.fall17_nuke",
		localized_description = "info.tf2pm.hat.fall17_nuke_desc",
		icon = "backpack/workshop/player/items/all_class/fall17_nuke/fall17_nuke",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall17_nuke/fall17_nuke_%s.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "soldier", "demo", "medic", "scout", "spy", "engineer", "sniper"},
		targets = {"beard"},
		paintable = true,
		name = "Particulate Protector",
		localized_name = "info.tf2pm.hat.dec20_particulate_protector",
		icon = "backpack/workshop/player/items/all_class/dec20_particulate_protector/dec20_particulate_protector",
		models = {
			["basename"] = "models/workshop/player/items/all_class/dec20_particulate_protector/dec20_particulate_protector_%s.mdl",
		},
		styles = {
			[0] = {
				localized_name = "info.tf2pm.hat.dec20_particulate_protector_style0",
			},
			[1] = {
				localized_name = "info.tf2pm.hat.dec20_particulate_protector_style1",
				bodygroup_overrides = {
					["headphones"] = 1,
					["hat"] = 1,
				},
			},
		},
	},
	{
		classes = {"heavy", "soldier", "demo", "medic", "scout", "spy", "engineer", "sniper"},
		targets = {"hat"},
		paintable = false,
		name = "Teufort Knight",
		localized_name = "info.tf2pm.hat.bak_teufort_knight",
		icon = "backpack/workshop/player/items/all_class/bak_teufort_knight/bak_teufort_knight",
		models = {
			["basename"] = "models/workshop/player/items/all_class/bak_teufort_knight/bak_teufort_knight_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "heavy", "sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "Airborne Attire",
		localized_name = "info.tf2pm.hat.spr17_wingman",
		localized_description = "info.tf2pm.hat.spr17_wingman_desc",
		icon = "backpack/workshop/player/items/all_class/spr17_wingman/spr17_wingman",
		models = {
			["scout"] = "models/workshop/player/items/all_class/spr17_wingman/spr17_wingman_scout.mdl",
			["heavy"] = "models/workshop/player/items/all_class/spr17_wingman/spr17_wingman_heavy.mdl",
			["sniper"] = "models/workshop/player/items/all_class/spr17_wingman/spr17_wingman_sniper.mdl",
		},
	},
	{
		classes = {"demo", "heavy", "sniper"},
		targets = {"shirt"},
		paintable = true,
		name = "EOTL_hiphunter_jacket",
		localized_name = "info.tf2pm.hat.eotl_hiphunter_jacket",
		icon = "backpack/workshop/player/items/all_class/hiphunter_jacket/hiphunter_jacket",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hiphunter_jacket/hiphunter_jacket_%s.mdl",
		},
	},
	{
		classes = {"heavy", "soldier", "demo", "medic", "scout", "spy", "engineer", "sniper"},
		targets = {"glasses"},
		paintable = true,
		name = "The Macho Mann",
		localized_name = "info.tf2pm.hat.jul13_macho_mann_glasses",
		localized_description = "info.tf2pm.hat.jul13_macho_mann_glasses_desc",
		icon = "backpack/workshop/player/items/all_class/jul13_macho_mann_glasses/jul13_macho_mann_glasses",
		models = {
			["basename"] = "models/workshop/player/items/all_class/jul13_macho_mann_glasses/jul13_macho_mann_glasses_%s.mdl",
		},
	},
	{
		classes = {"soldier", "engineer", "heavy"},
		targets = {"beard"},
		paintable = true,
		name = "The Viking Braider",
		localized_name = "info.tf2pm.hat.fall2013_the_braided_pride",
		localized_description = "info.tf2pm.hat.fall2013_the_braided_pride_desc",
		icon = "backpack/workshop/player/items/all_class/fall2013_the_braided_pride/fall2013_the_braided_pride",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_the_braided_pride/fall2013_the_braided_pride_%s.mdl",
		},
	},
	{
		classes = {"soldier", "engineer", "heavy"},
		targets = {"beard"},
		paintable = true,
		name = "The Cuban Bristle Crisis",
		localized_name = "info.tf2pm.hat.fall2013_the_cuban_coverup",
		localized_description = "info.tf2pm.hat.fall2013_the_cuban_coverup_desc",
		icon = "backpack/workshop/player/items/all_class/fall2013_the_cuban_coverup/fall2013_the_cuban_coverup",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_the_cuban_coverup/fall2013_the_cuban_coverup_%s.mdl",
		},
	},
	{
		classes = {"scout", "engineer"},
		targets = {"hat"},
		paintable = true,
		name = "Trucker's Topper",
		localized_name = "info.tf2pm.hat.dec17_truckers_topper",
		icon = "backpack/workshop/player/items/all_class/dec17_truckers_topper/dec17_truckers_topper",
		models = {
			["scout"] = "models/workshop/player/items/all_class/dec17_truckers_topper/dec17_truckers_topper_scout.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec17_truckers_topper/dec17_truckers_topper_engineer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
			["headphones"] = 1,
		},
	},
	{
		classes = {"heavy", "pyro", "scout", "engineer", "demo"},
		targets = {"pants"},
		paintable = true,
		name = "The Boom Boxers",
		localized_name = "info.tf2pm.hat.hwn2020_boom_boxers",
		icon = "backpack/workshop/player/items/all_class/hwn2020_boom_boxers/hwn2020_boom_boxers",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hwn2020_boom_boxers/hwn2020_boom_boxers_%s.mdl",
		},
	},
	{
		classes = {"heavy", "pyro", "soldier", "scout", "spy", "engineer", "demo"},
		targets = {"glasses"},
		paintable = true,
		name = "Reader's Choice",
		localized_name = "info.tf2pm.hat.dec16_readers_choice",
		localized_description = "info.tf2pm.hat.dec16_readers_choice_desc",
		icon = "backpack/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice",
		models = {
			["heavy"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_heavy.mdl",
			["pyro"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_pyro.mdl",
			["demoman"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_demo.mdl",
			["scout"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_scout.mdl",
			["spy"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_spy.mdl",
			["engineer"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_engineer.mdl",
			["soldier"] = "models/workshop/player/items/all_class/dec16_readers_choice/dec16_readers_choice_soldier.mdl",
		},
	},
	{
		classes = {"pyro", "engineer"},
		targets = {},
		paintable = true,
		name = "The Trick Stabber",
		localized_name = "info.tf2pm.hat.hwn2019_trick_stabber",
		icon = "backpack/workshop/player/items/all_class/hwn2019_trick_stabber/hwn2019_trick_stabber",
		models = {
			["pyro"] = "models/workshop/player/items/all_class/hwn2019_trick_stabber/hwn2019_trick_stabber_pyro.mdl",
			["engineer"] = "models/workshop/player/items/all_class/hwn2019_trick_stabber/hwn2019_trick_stabber_engineer.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"soldier", "demo"},
		targets = {"hat"},
		paintable = true,
		name = "Bobby Bonnet",
		localized_name = "info.tf2pm.hat.sum19_bobby_bonnet",
		icon = "backpack/workshop/player/items/all_class/sum19_bobby_bonnet/sum19_bobby_bonnet",
		models = {
			["soldier"] = "models/workshop/player/items/all_class/sum19_bobby_bonnet/sum19_bobby_bonnet_soldier.mdl",
			["demoman"] = "models/workshop/player/items/all_class/sum19_bobby_bonnet/sum19_bobby_bonnet_demo.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"demo", "spy"},
		targets = {"hat"},
		paintable = true,
		name = "Ethereal Hood",
		localized_name = "info.tf2pm.hat.hw2013_ethereal_hood",
		icon = "backpack/workshop/player/items/all_class/hw2013_ethereal_hood/hw2013_ethereal_hood",
		models = {
			["demoman"] = "models/workshop/player/items/all_class/hw2013_ethereal_hood/hw2013_ethereal_hood_demo.mdl",
			["spy"] = "models/workshop/player/items/all_class/hw2013_ethereal_hood/hw2013_ethereal_hood_spy.mdl",
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"heavy", "pyro", "medic", "scout", "spy", "engineer", "soldier"},
		targets = {"belt_misc"},
		paintable = false,
		name = "Buttler",
		localized_name = "info.tf2pm.hat.bak_buttler",
		icon = "backpack/workshop/player/items/all_class/bak_buttler/bak_buttler",
		models = {
			["basename"] = "models/workshop/player/items/all_class/bak_buttler/bak_buttler_%s.mdl",
		},
	},
	{
		classes = {"soldier", "spy", "medic"},
		targets = {"hat"},
		paintable = true,
		name = "The Powdered Practitioner",
		localized_name = "info.tf2pm.hat.enlightened_mann",
		localized_description = "info.tf2pm.hat.enlightened_mann_desc",
		icon = "backpack/workshop/player/items/all_class/enlightened_mann/enlightened_mann",
		models = {
			["basename"] = "models/workshop/player/items/all_class/enlightened_mann/enlightened_mann_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.enlightened_mann_style0",
				models = {
					["basename"] = "models/workshop/player/items/all_class/enlightened_mann/enlightened_mann_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.enlightened_mann_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/enlightened_mann_s2/enlightened_mann_s2_%s.mdl",
				}
			},
		},
		bodygroup_overrides = {
			["hat"] = 1,
		},
	},
	{
		classes = {"scout", "demo"},
		targets = {"feet"},
		paintable = false,
		name = "The Baphomet Trotters",
		localized_name = "info.tf2pm.hat.hw2013_scout_demo_faunlegs",
		icon = "backpack/workshop/player/items/all_class/hw2013_scout_demo_faunlegs/hw2013_scout_demo_faunlegs",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_scout_demo_faunlegs/hw2013_scout_demo_faunlegs_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
			["shoes"] = 1,
		},
	},
	{
		classes = {"pyro", "engineer"},
		targets = {},
		paintable = true,
		name = "The Special Eyes",
		localized_name = "info.tf2pm.hat.fall2013_the_special_eyes",
		icon = "backpack/workshop/player/items/all_class/fall2013_the_special_eyes/fall2013_the_special_eyes",
		models = {
			["basename"] = "models/workshop/player/items/all_class/fall2013_the_special_eyes/fall2013_the_special_eyes_%s.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_the_special_eyes_style1",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_the_special_eyes/fall2013_the_special_eyes_%s.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.fall2013_the_special_eyes_style2",
				models = {
					["basename"] = "models/workshop/player/items/all_class/fall2013_the_special_eyes_style1/fall2013_the_special_eyes_style1_%s.mdl",
				}
			},
		},
	},
	{
		classes = {"engineer", "pyro", "sniper"},
		targets = {"hat"},
		paintable = true,
		name = "The Snaggletoothed Stetson",
		localized_name = "info.tf2pm.hat.hw2013_leather_face",
		icon = "backpack/workshop/player/items/all_class/hw2013_leather_face/hw2013_leather_face",
		models = {
			["basename"] = "models/workshop/player/items/all_class/hw2013_leather_face/hw2013_leather_face_%s.mdl",
		},
		bodygroup_overrides = {
			["headphones"] = 1,
			["hat"] = 1,
		},
	},
	{
		classes = {"engineer", "sniper"},
		targets = {"engineer_pocket", "sniper_pocket"},
		paintable = false,
		name = "The Dual-Core Devil Doll",
		localized_name = "info.tf2pm.hat.robo_all_spybot",
		localized_description = "info.tf2pm.hat.robo_all_spybot_desc",
		icon = "backpack/workshop/player/items/all_class/robo_all_spybot/robo_all_spybot",
		models = {
			["basename"] = "models/workshop/player/items/all_class/robo_all_spybot/robo_all_spybot_%s.mdl",
		},
	},
	{
		classes = {"scout", "demo"},
		targets = {"feet"},
		paintable = false,
		name = "The Sole Saviors",
		localized_name = "info.tf2pm.hat.sbox2014_armor_shoes",
		icon = "backpack/workshop/player/items/all_class/sbox2014_armor_shoes/sbox2014_armor_shoes",
		models = {
			["basename"] = "models/workshop/player/items/all_class/sbox2014_armor_shoes/sbox2014_armor_shoes_%s.mdl",
		},
		bodygroup_overrides = {
			["shoes_socks"] = 1,
			["shoes"] = 1,
		},
	},

}
