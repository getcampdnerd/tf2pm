
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

tf2_pm = tf2_pm or {}

if SERVER then
	AddCSLuaFile('tf2pm/sh_model.lua')
	AddCSLuaFile('tf2pm/sh_functions.lua')
	AddCSLuaFile('tf2pm/sh_worker.lua')
	AddCSLuaFile('tf2pm/cl_worker.lua')
	AddCSLuaFile('tf2pm/cl_network.lua')
	AddCSLuaFile('tf2pm/cl_gui.lua')
	AddCSLuaFile('tf2pm/cl_panels.lua')
	AddCSLuaFile('tf2pm/sh_playermodels.lua')

	resource.AddWorkshop('2225564383')
end

include('tf2pm/sh_model.lua')
include('tf2pm/sh_functions.lua')

include('tf2pm/sh_worker.lua')

if CLIENT then
	include('tf2pm/cl_worker.lua')
	include('tf2pm/cl_network.lua')
	include('tf2pm/cl_panels.lua')
	include('tf2pm/cl_gui.lua')
else
	include('tf2pm/sv_worker.lua')
	include('tf2pm/sv_network.lua')
end

include('tf2pm/sh_playermodels.lua')

do
	local auto = {
		'allclass', 'demoman', 'engineer', 'heavy',
		'medic', 'multiclass', 'pyro', 'scout',
		'sniper', 'soldier', 'spy'
	}

	for i = 1, 7 do
		table.insert(auto, 'medals' .. i)
	end

	for i, filename in ipairs(auto) do
		if SERVER then
			AddCSLuaFile('tf2pm/auto/' .. filename .. '.lua')
		end

		local data = include('tf2pm/auto/' .. filename .. '.lua')

		if istable(data) then
			for _, hatdef in ipairs(data) do
				local models = hatdef.models
				hatdef.models = nil

				if models.basename then
					local basename = models.basename
					models.basename = nil

					for _, classname in ipairs(hatdef.classes) do
						if not models[classname] then
							models[classname] = basename:format(classname, classname, classname, classname, classname)
						end
					end
				end

				for classname, model in pairs(models) do
					if model:trim():lower() == '' then
						print('[TF2PM] Empty auto model of ' .. hatdef.name .. ' of class ' .. classname)
					else
						local copy = table.Copy(hatdef)

						if istable(copy.styles) then
							for _, style in pairs(copy.styles) do
								if istable(style.models) then
									style.model = style.models[classname] or style.models.basename and style.models.basename:format(classname, classname, classname, classname, classname)
									style.models = nil
								end
							end
						end

						if istable(copy.targets) then
							if table.qhasValue(copy.targets, 'whole_head') then
								table.RemoveByValue(copy.targets, 'whole_head')
								table.insert(copy.targets, 'face')
								table.insert(copy.targets, 'glasses')
								table.insert(copy.targets, 'hat')
							end

							if table.qhasValue(copy.targets, 'glasses') then
								table.RemoveByValue(copy.targets, 'glasses')
								table.insert(copy.targets, 'eyes')
								table.insert(copy.targets, 'face')
							end
						end

						copy.applicable = classname
						copy.model = model
						copy.responsive_skins = {0, 1}

						if filename:startsWith('medals') then
							copy.gui_category = 'medals'
						end

						local findname = model:split('/')
						findname = findname[#findname]
						findname = findname:sub(1, -5)
						findname = findname:replace(classname .. '_', '')
						findname = findname:replace('_' .. classname, '')

						--[[if classname == 'demoman' then
							findname = findname:replace('demo_', '')
							findname = findname:replace('_demo', '')
						end]]

						tf2_pm.RegisterHat(classname .. '_' .. findname, copy)
					end
				end
			end
		end
	end
end

local function refresh_lang()
	local rebuild = {}

	for i = 1, 50 do
		AddCSLuaFile('tf2pm/lang/' .. i .. '.lua')
		table.append(rebuild, include('tf2pm/lang/' .. i .. '.lua') or ErrorNoHalt('[TF2PM] Can\'t load translation strings from tf2pm/lang/' .. i .. '.lua!\n') or {})
	end

	local json = util.Decompress(DLib.util.Base64Decode(table.concat(rebuild, '\n')))

	if not json then
		ErrorNoHalt('[TF2PM] Failed to decompress language files!\n')
	else
		local decoded = util.JSONToTable(json)

		if decoded then
			for finalcode, data in pairs(decoded) do
				if table.qhasValue(DLib.I18n.CURRENT_LANG, finalcode) then
					for langstring, phrase in pairs(data) do
						DLib.i18n.registerPhrase(finalcode, 'info.tf2pm.hat.' .. langstring, tostring(phrase))
					end
				end
			end
		else
			ErrorNoHalt('[TF2PM] Failed to decode language files!\n')
		end
	end
end

refresh_lang()
hook.Add('DLib.TranslationsReloaded', 'TF2PM', refresh_lang)
hook.Add('DLib.LanguageChanged', 'TF2PM', refresh_lang)

local files = file.Find('tf2pm/autorun/*.lua', 'LUA')

for i, _file in ipairs(files) do
	if SERVER then
		AddCSLuaFile('tf2pm/autorun/' .. _file)
	end

	include('tf2pm/autorun/' .. _file)
end
